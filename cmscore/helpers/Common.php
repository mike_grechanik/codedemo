<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\helpers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use mgrechanik\cmscore\models\Tformats;
use yii\web\UploadedFile;
use mgrechanik\cmscore\service\Transliterator;
use mgrechanik\cmscore\uploads\models\Files;
use mgrechanik\cmscore\assets\ShortCodes\TokenInsertAsset;
use yii\base\Module;

/**
 * Helper to do all the common work for cms.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Common
{
    /**
     * Building the url by innerUrl of the page.
     * 
     * "Get" parameters from innerUrl will be moved to the params.
     * It is a wrapper to yii\helpers\Url::toRoute, so:
     * - the $url is expected to have a leading slash (but it will otherwise be added automatically)
     *   We need this because innerUrl is the full address, and full address for toRoute
     *   has to have a leading slash
     * - Example: 
     *   Common::urlTo('/article/main/view?id=10', ['lang' => 'ru']) // ==> '/albatros'
     * 
     * @api
     * @param string $url page's innerUrl in this cmf
     * @param array $params
     * @param boolean $scheme
     * @return string The aliased url
     */
    public static function urlTo($url, $params = [], $scheme = false)
    {
        $parse = parse_url($url);
        $route = isset($parse['path']) ? $parse['path'] : '';            
        if (strncmp($route, '/', 1)) {
            $route = '/' . $route;
        }
        $res = [$route];
        if (isset($parse['query'])) {
            $args = [];
            parse_str($parse['query'], $args);
            $res = array_merge($res, $args);
        }  
        if ($params) {
            $res = array_merge($res, $params);
        }        
        if (isset($parse['fragment'])) {
            $res['#'] = $parse['fragment'];
        }
        return Url::toRoute($res, $scheme);
    }
    
    /**
     * Returns aliased url to a page by it's number
     * 
     * @api
     * @param integer $pid
     * @param array $params
     * @param boolean $scheme True for absolute
     * @return string|null
     */
    public static function urlToPage($pid, $params = [], $scheme = false)
    {
        if ($arr = Yii::$app->urlManager->getPageInfoByPid($pid)) {
            $params = array_merge($params, ['lang' => $arr['lang'], 'cross' => 'front']);
            return self::urlTo('/' . $arr['source'], $params, $scheme);
        }
        return null;
    }

    /**
     * Decorating a list of links to html for admin lists.
     * 
     * Each element of $links array lokks like this:
     * ['label' => 'Home', 'url' => ['site/index']],
     * 
     * @api
     * @param array $links
     * @param array $options
     * @param string $delimiter
     * @return string
     */
    public static function decorateLinks($links, $options = [], $delimiter = '&nbsp;')
    {
        $res = '';
        $amount = count($links);
        $i = 0;
        $j = 0;
        foreach ($links as $val) {
            $i++;
            $j++;
            $delm = ($i < $amount) ? $delimiter : '';
            $res .= Html::a($val['label'], $val['url'], $options) . $delm;
            if ($j == 2) {
                $res .= '<br>';
                $j = 0;
            }
        }
        return $res;
    }
    
    //////////////////////////////
    ////////// Files
    //////////////////////////////      
    
    /**
     * Returning from path to a mini image the information about original image and miniformat.
     * 
     * When there is the request for an image which does not exist we
     * are checking if it is the request after a miniature of some certain image
     * 
     * Example:
     * Request - /uploads/some1/mini100/sss.jpg
     * Result:
     *   Array
     *   (
     *       [filename] => sss.jpg
     *       [fileext] => jpg
     *       [miniformat] => mini100
     *       [imagepath] => some1/sss.jpg // path to the original image
     *   )
     * 
     * @param string $miniPath The path to the miniature image. Like 'mini100/sample.png'.
     * @param array $imageTypes The array with image extensions in it's values.
     * @return array|null An array of metadata about image or null if wrong path.
     */
    public static function getImageInfoFromMini($miniPath, $imageTypes)
    {
        $res = [];
        $miniPath = str_replace('../', '', $miniPath);
        $miniPath = ltrim($miniPath, '/');
        $parts = explode('/', $miniPath);
        if (count($parts) < 2) {
            return null;
        }
        $name = array_pop($parts);
        $res['filename'] = $name;
        $x2 = explode('.', $name);
        $res['fileext'] = array_pop($x2);
        // Pattern will be like this: '/.+\.((jpg)|(jpeg)|(png)|(bmp)|(gif))$/'
        $pattern = '/.+\.((' . implode(')|(', $imageTypes) . '))$/';
        if (!preg_match($pattern, $name)) {
            return null;
        }
        $res['miniformat'] = array_pop($parts);
        $parts[] = $name;
        $res['imagepath'] = implode('/', $parts);
        return $res;
    }
    
    /**
     * Inserts to the image path information about mini format.
     * 
     * Example:
     * ('some1/sss.jpg', 'mini100') => 'some1/mini100/sss.jpg'
     * 
     * @api
     * @param string $imagePath Path to the image
     * @param string $miniFormat Miniature format
     * @return string The path to a miniature of the image
     */
    public static function getMiniImagePath($imagePath, $miniFormat)
    {
        $parts = explode('/', $imagePath);
        $name = array_pop($parts);
        $parts[] = $miniFormat;
        $parts[] = $name;
        return implode('/', $parts);
    }
    
    /**
     * List of image formats ready for selects. Including 'original' for no format.
     * 
     * @api
     * @param boolean $withOriginal Whether to include original
     * @return array
     * @see \mgrechanik\cmscore\models\ImageInsertButton
     */
    public static function getImageFormats($withOriginal = true)
    {
        $res = [];
        if ($withOriginal) {
            $res['original'] = Yii::t('cmscore', 'Original');
        }
        $all = Yii::$app->getModule('uploads')->miniFormats;
        foreach ($all as $key => $mf) {
            $res[$key] = Yii::t('admintips', $mf['title']);
        }
        return $res;
    } 
    
    /**
     * Checks whether image format exists in the system.
     * 
     * @api
     * @param string $format Image format
     * @return boolean
     */
    public static function isImageFormatExists($format)
    {
       $formats = self::getImageFormats(false); 
       return isset($formats[$format]);
    }

    /**
     * Adjusts filename of the uploaded file to make sure it's name is free in the directory.
     * 
     * This function makes sure and adjust $fobj->name for being vacant in the $dir
     * 
     * See [[ArticleNode::saveFiles()]]
     * 
     * @param string $dir The real dir in which we are checking.
     * Dir here is with "/" in the end.
     * $dir = Yii::$app->getModule('uploads')->uploadsRootFull; // with finishing slash
     * @param UploadedFile $fobj File uploaded object
     * @param string $delimiter
     */
    public static function AdjustFilenameBeforeSave($dir, UploadedFile $fobj, $delimiter = '-')
    {
        $name = $fobj->baseName;
        $ext = $fobj->extension;
        $tr = new Transliterator();
        $name = $name0 = $tr->transliterate('ru', $name);
        $count = 0;
        while (file_exists($dir . $name . '.' .$ext)) {
            $count++;
            $name = $name0 . $delimiter . $count;
        }
        $fobj->name = $name . '.' . $ext;
    }
    
    /**
     * Getting the outter url for a file saved under $fid number in the Files table.
     * 
     * Look at it's use in [[ArticleNode::getAnnounceData()]]
     * 
     * @api
     * @param integer $fid The number of the file in the Files table.
     * @return string The path to the file saved in the Files table
     * @see \mgrechanik\cmscore\uploads\Module::uploadsWeb
     */
    public static function getFileUrl($fid)
    {
        if ($fid && ($file = Files::findOne($fid))) {
            $upload = Yii::$app->getModule('uploads');
            return $upload->uploadsWeb . $file->fullpath;
        } 
        return false;
    }   
    
    /**
     * Getting the outter url for a file from relative path.
     * 
     * @param string $path The path as it is saved in the fullpath column of the Files table
     * @return string
     */
    public static function getFileUrlByPath($path)
    {
        $upload = Yii::$app->getModule('uploads');
        return $upload->uploadsWeb . $path;        
    }


    //////////////////////////////
    ////////// End Files
    //////////////////////////////      

    
    //////////////////////////////
    ////////// Text formats
    //////////////////////////////    
    
    /**
     * This function is processing the text with needed format
     * 
     * It returns an array with two keys:
     * [
     *    'text' => 'Here is processed text'
     *    'data' => [
     *       // some additional data for this text cache. Like:
     *       'updated' => 1212121212   // it means the timestamp when this format has been changed
     *       'assets' => []
     *    ]
     * ]
     * 
     * @api
     * @param string $formatName The name of the text format
     * @param string $text The text being processed
     * @param boolean $process Whether to process associated data
     * like registering js assets
     * @return array See the format above
     * @see \mgrechanik\ctypes\page\controllers\MainController
     */
    public static function format($formatName, $text, $process = false)
    {
        $data = [];
        if ($format = Tformats::findOne($formatName)) {
            $format->process($text, $data);
            if ($process) {
                self::processFormatData($data);
            }            
        }
        return [
            'text' => $text,
            'data' => $data,
        ];
    }
    
    /**
     * Processing additional data associated with cached text
     * 
     * @api
     * @param array $data
     * @see \mgrechanik\ctypes\page\controllers\MainController
     */
    public static function processFormatData($data)
    {
        if (!is_array($data)) {
            return;
        }
        if (isset($data['assets'])) {
            foreach ($data['assets'] as $asset) {
                Yii::$app->view->registerAssetBundle($asset);
            }
        }
    }
    
    /**
     * Whether to rebuil the cache
     * 
     * It checks the emptyness of the cacheText and then whether
     * the time from $cache_data is different from the time this format 
     * was changed last time.
     * 
     * It is optimased for a mass operation also. Look at [[Tformats::findCachedFormat]].
     * 
     * @api
     * @param string $tformat
     * @param string $cacheText
     * @param string $cache_data Serialized array of data
     * @return boolean Whether rebuilding needed
     * @see \mgrechanik\ctypes\page\controllers\MainController
     */
    public static function whetherToRebuiltCache($tformat, $cacheText, $cache_data = '')
    {
        $process = false;
        if (!$cacheText) {
            $process = true;
        } else {
            // checking the version of the format
            $data = [];
            if ($cache_data) {
                $data = unserialize($cache_data);
            }
            $updated = isset($data['updated']) ? $data['updated'] : 0;
            if ($format = Tformats::findCachedFormat($tformat)) {
                if ($updated != $format->updated) {
                    $process = true;
                }
            }
        }  
        return $process;
    }
    
    //////////////////////////////
    ////////// End Text formats
    //////////////////////////////    
    

    
    //////////////////////////////
    ////////// Tokens
    //////////////////////////////
    
    /**
     * Returns a template of pathauto for a content type
     * 
     * This template is being managed at the Content type settings page
     * 
     * @api
     * @param integer $contentType
     * @return string
     * @see \mgrechanik\cmscore\behaviors\AliasFieldBehavior::beforeValidate
     */
    public static function getPathAutoTemplateForContent($contentType)
    {
        $pmanager = Yii::$app->get('pageManager');
        $ctypes = $pmanager->getContentTypes(); 
        if (!isset($ctypes[$contentType])) {
            return '';
        }
        $ctype = $ctypes[$contentType];
        if (!isset($ctype['contentSettingsModelClass'])) {
            return '';
        }        
        $class = $ctype['contentSettingsModelClass'];
        $model = new $class();
        $model->initFieldsModel();  
        if ($model->hasMethod('getPathAutoSettings')) {
            $data = $model->getPathAutoSettings();
            if (isset($data['template'])) {
                return ltrim($data['template'], '/');
            }
        }
        return '';
    }  
    
    /**
     * Processes the template by replacing all tokens of the choosen token group
     * with their values
     * 
     * @api
     * @param string $template The template with tokens inside it. like - "pages/[node:title]"
     * @param string $group The name of the token group from which we are taking tokens.
     * @param array $args Associative array of additional parameters. 
     * It will be passed to the callback functions  associated with tokens
     * @return string string with replacements done
     * @see \mgrechanik\cmscore\behaviors\AliasFieldBehavior::beforeValidate
     */
    public static function processTokens($template, $group, array $args = [])
    {
        $arr = self::getTokensForReplace($group, $args);
        return strtr($template, $arr);
    }

    /**
     * Returns an array of replacement pairs for a certain token group.
     * 
     * It's format (for a strtr)
     * [tokentemplate => value, ...]
     * 
     * @param string $group Token group
     * @param array $args
     * @return string[]
     */
    public static function getTokensForReplace($group, array $args = [])
    {
        $res = [];
        /* @var $pm \mgrechanik\cmscore\components\PageManager */
        $pm = Yii::$app->get('pageManager');
        $tokensdef = $pm->getTokens();
        $groups = $pm->getTokengroups();
        if (isset($groups[$group])) {
            $g = $groups[$group];
            $tokens = [];
            foreach ($g as $val) {
                $tokens = array_merge($tokens, $val['tokens']);
            }
            $tokens = array_unique($tokens);
            foreach ($tokens as $tok) {
                if (isset($tokensdef[$tok])) {
                    $t = $tokensdef[$tok];
                    $val = call_user_func_array($t['callback'], [$tok, $args]);
                    $res[$t['template']] = $val;
                }
            }
        }
        return $res;
    }
    
    /**
     * Returns a html(and registers js) of help hith for a certain token group
     * 
     * Just print it in the template.
     * 
     * @api
     * @param string $group
     * @return string
     */
    public static function getTokenHint($group)
    {
        $res = '<div class="tokens-hint"><span>' . 
                Yii::t('admintips', 'Allowed tokens to use:') . '</span><div class="tokens-body">';
        /* @var $pm \mgrechanik\cmscore\components\PageManager */
        $pm = Yii::$app->get('pageManager');
        $tokensdef = $pm->getTokens();
        $groups = $pm->getTokengroups();
        $tclass = ' class="table table-striped table-bordered table-hover table-condensed"';
        if (isset($groups[$group])) {
            TokenInsertAsset::register(Yii::$app->view);
            $g = $groups[$group];
            $res .= "<table$tclass>";
            foreach ($g as $val) {
                $res .= '<tr><td>' . Yii::t('admintips', $val['title']) . '</td><td>&nbsp;</td></tr>';
                $tokens = $val['tokens'];
                foreach ($tokens as $tok) {
                    if (isset($tokensdef[$tok])) {
                        $t = $tokensdef[$tok];
                        $res .= '<tr><td><a href="#" class="token-template" title="' . Yii::t('admintips', 'Click to insert this token into field') . '">' . $t['template'] . '</a></td><td>' . Yii::t('admintips', $t['title']) . '</td></tr>';
                    }
                }
            }
            $res .= '</table>';
        }
        $res .= '</div></div>';
        return $res;        
    }
    
    //////////////////////////////
    ////////// End Tokens
    //////////////////////////////
    
    
    /**
     * Returns a list of key=>value of timezones
     * 
     * @return array Timezones list.
     */
    public static function generate_timezone_list()
    {
        static $regions = array(
            \DateTimeZone::AFRICA,
            \DateTimeZone::AMERICA,
            \DateTimeZone::ANTARCTICA,
            \DateTimeZone::ASIA,
            \DateTimeZone::ATLANTIC,
            \DateTimeZone::AUSTRALIA,
            \DateTimeZone::EUROPE,
            \DateTimeZone::INDIAN,
            \DateTimeZone::PACIFIC,
        );

        $timezones = array();
        foreach( $regions as $region )
        {
            $timezones = array_merge( $timezones, \DateTimeZone::listIdentifiers( $region ) );
        }

        $timezone_offsets = array();
        foreach( $timezones as $timezone )
        {
            $tz = new \DateTimeZone($timezone);
            $timezone_offsets[$timezone] = $tz->getOffset(new \DateTime);
        }

        // sort timezone by timezone name
        ksort($timezone_offsets);

        $timezone_list = array();
        foreach( $timezone_offsets as $timezone => $offset )
        {
            $offset_prefix = $offset < 0 ? '-' : '+';
            $offset_formatted = gmdate( 'H:i', abs($offset) );

            $pretty_offset = "UTC${offset_prefix}${offset_formatted}";

            $t = new \DateTimeZone($timezone);
            $c = new \DateTime(null, $t);
            //$current_time = $c->format('g:i A');
            $current_time = $c->format('d-m-Y H:i');

            $timezone_list[$timezone] = "$timezone (${pretty_offset})  =>  $current_time";
        }

        return $timezone_list;
    }
    
    /**
     * informs all modules (except main application) about some event who has come.
     * 
     * In $someData pass a data if you need.
     * You will find it in $event object passed to a handler. 
     * (Look at User module for example)
     * 
     * @param string $eventname The name of the event fired.
     * @param mixed $someData Additional data about event.
     * In the event handlers you will find it in the $event->someData
     */
    public static function informAllModules($eventname, $someData = null)
    {
        $event = new \mgrechanik\cmscore\base\Event;
        if ($someData) {
            $event->someData = $someData;
        }
        self::informChildren(Yii::$app, $eventname, $event);
    }
    
    /**
     * Informs all children modules of a $parent module about global event.
     * 
     * @param \yii\base\Module $parent
     * @param string $eventname
     * @param \mgrechanik\cmscore\base\Event $event
     */
    protected static function informChildren(Module $parent, $eventname, $event)
    {
        foreach ($parent->getModules() as $id => $mod) {
            $module = $parent->getModule($id);
            $module->trigger($eventname, $event);
            self::informChildren($module, $eventname, $event);            
        }        
    }      
    
    /**
     * Returns user friendly title of the error page
     * 
     * @see mgrechanik/cmscore/admin/views/admin/page/error.php or other(frontend) error.php files
     * @param integer $status Error status code
     * @return boolean|string Error string or false if no association found
     */
    public static function getErrorTitle($status)
    {
        $titles = [
            403 => 'Access denied',
            404 => 'Page not found',
        ];
        if (isset($titles[$status])) {
            return Yii::t('cmscore', $titles[$status]);
        }
        return false;
    }
}

