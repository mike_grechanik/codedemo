<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\models\traits;

use yii\db\ActiveRecord;

/**
 * External API to work with Materialized Path functionality.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
trait MaterializedPath
{

    /**
     * Getting the query for getting all children of a certain node.
     * 
     * @param ActiveRecord|null $node The node we are getting tree query for. Null for a root node. 
     * Root node is also the new record AR object with it's name as '__ROOT__'
     * @param array $treeCondition The condition to indicate needed tree among others in the table
     */
    public static function getTreeQuery($node = null, $treeCondition = [])
    {
        if (! $node instanceof ActiveRecord) {
            $node = static::getRootNode($treeCondition);
        }
        return $node->getSubTreeQuery();
    }
    
    /**
     * Getting the root node of the tree.
     * 
     * @api
     * @param array $treeCondition The condition to indicate needed tree among others in the table
     */
    public static function getRootNode($treeCondition = [])
    {
        $class = get_called_class();
        $node = new $class($treeCondition);
        $names = $node->getMpNames();
        $node->{$names['name']} = '__ROOT__';
        return $node;
    }

    /**
     * Returns the tree of the [[TreeNode]] objects
     * 
     * @api
     * @param ActiveRecord|null $parnode The node we are getting it's children tree for. 
     * Null for a root node. 
     * Root node is also the new record AR object with it's name as '__ROOT__'
     * @param array $treeCondition The condition to indicate needed tree among others in the table
     * @return TreeNode[] The first level of (sub)tree
     * @see TreeNode
     */
    public static function getArrayTree($parnode = null, $treeCondition = [])
    {
        $class = get_called_class();
        $nodes = $class::getTreeQuery($parnode, $treeCondition)->asArray()->all();
        $tree = [];
        $pathPrefix = '';
        if ($parnode) {
            $pathPrefix = $parnode->getFullPath();
        } else {
            $parnode = $class::getRootNode($treeCondition);
        }
        $names = $parnode->getMpNames();
        $cache = [];
        foreach ($nodes as $node) {
            $path = $node[$names['path']];
            if ($pathPrefix) {
                $path = (string) substr($path, strlen($pathPrefix));
            }
            $parid = $class::getParentidFromPath($path);
            $object = new TreeNode();
            $object->value = $node;
            $cache[intval($node[$names['id']])] = $object;
            if ($parid === 0) {
                $tree[] = $object;
            } else {
                if (isset($cache[$parid])) {
                    $pobj = $cache[$parid];
                    $pobj->children[] = $object;
                    $object->parent = $pobj;
                }
            }
        }
        return $tree;
    }
    
    /**
     * Full rebuilding of positions of the (sub)nodes of the tree
     * 
     * @api
     * @param ActiveRecord|null $parnode The node we are getting tree for. Null for a root node. 
     * @param array $treeCondition The condition to indicate needed tree among others in the table
     * @param array $subnodes The new information about positions
     * @see \mgrechanik\menu\controllers\admin\menu\TaskController::actionView()
     */
    public static function saveSubTreeOrder($parnode = null, $treeCondition = [], $subnodes)
    {
        if (!$parnode) {
            $parnode = static::getRootNode($treeCondition);
        }
        $mpNames = $parnode->getMpNames();
        $pathPrefix = $parnode->getFullPath();
        $class = get_called_class();
        foreach ($subnodes as $pk => $val) {
            if (!is_string($val)) {
                continue;
            }
            $data = explode(':', $val);
            if (count($data) != 2) {
                continue;
            }
            list($path, $weight) = $data;
            if (preg_match('#[^\d/]#iu', $path)) {
                continue;
            }
            $weight = intval($weight);
            $path = $pathPrefix . $path;
            $level = substr_count($path, '/') + 1;
            $cond = [$mpNames['id'] => $pk];
            if ($treeCondition) {
                $cond = array_merge($cond, $treeCondition);
            }
            $class::updateAll([
                $mpNames['path'] => $path,
                $mpNames['weight'] => $weight,
                $mpNames['level'] => $level,
            ], $cond);
        }
    }

    /**
     * Returns the last id in the path. The id of the parent node.
     * 
     * @param string $path Path
     * @return integer Id of the parent node
     */
    public static function getParentidFromPath($path)
    {
        $path = rtrim($path, '/');
        $parts = explode('/', $path);
        return (int) array_pop($parts);
    }
}
