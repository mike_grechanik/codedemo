<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\models\traits;

/**
 * The node of the tree, built by Materialized Path functionality
 * 
 * @see \mgrechanik\cmscore\models\traits\MaterializedPath::getArrayTree()
 * @see \mgrechanik\cmscore\service\AdminPagesManager
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class TreeNode 
{
    /**
     * @var array Associative array of column values of the row this node is associated with
     */
     public $value;
     
     /**
      * @var self|null Link to the parent TreeNode object or null if the parent is root.
      */
     public $parent;
     
     /**
      * @var self[] Array of children of this node 
      */
     public $children = [];
     
     /**
      * Checks whether this node ia a leaf.
      * 
      * @return boolean Whether this node ia a leaf.
      */
     public function isLeaf()
     {
         return empty($this->children);
     }
     
     /**
      * Sets the active trail to the current node and all it's ancestors
      * 
      * @see \mgrechanik\menu\helpers\MenuHelper::preprocessNode()
      */
     public function setActiveTrail()
     {
         $this->value['expanded'] = 1;
         $this->value['active-trail'] = 1;
         if ($this->parent) {
             $this->parent->setActiveTrail();
         }
     }
     
     /**
      * Delete this node from parent or mark it as deleted for first level nodes.
      * 
      * @see \mgrechanik\cmscore\widgets\SettingsTree::preprocessTree()
      */
     public function deleteFromParent()
     {
         if ($this->parent) {
             foreach ($this->parent->children as $key => $val) {
                 if ($val === $this) {
                     unset($this->parent->children[$key]);
                     break;
                 }
             }
         } else {
             $this->value['deleted_node'] = 1;
         }
     }
}

