<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\models\variables;

use Yii;

/**
 * The base model for all variables
 * 
 * @property string $varlang The current language of the variable
 * @property string $varname The name of the variable
 * @property string $varparcat The parent category id of the variable
 * @property string $varsingleprops The singleproperties of the variable
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class BaseModel extends \yii\base\Model 
{

    /**
     * These four properties are being managed during editing process
     * @internal 
     */
    protected $varlang;
    protected $varname;
    protected $varparcat;
    protected $varsingleprops = []; 

    /**
     * @var Variable Variable object with this model inside
     */
    protected $varobj;
    
    /**
     * Returns a helpText for this variable.
     * 
     * Useful when Settings has only this one variable (Settings::type == 2)
     * 
     * @return string
     */
    public function getTeaser()
    {
       return ''; 
    }    
    
    /**
     * Implements property
     */
    public function getVarname()
    {
        return $this->varname;
    }
    
    /**
     * Implements property
     */    
    public function getVarlang()
    {
        return $this->varlang;
    }
    
    /**
     * Implements property
     */    
    public function getVarparcat()
    {
        return $this->varparcat;
    }
    
    /**
     * Implements property
     */    
    public function getVarsingleprops()
    {
        return $this->varsingleprops;
    }
    
    /**
     * Implements property
     */    
    public function setVarname($val)
    {
        $this->varname = $val;;
    }  
    
    /**
     * Implements property
     */    
    public function setVarlang($val)
    {
        $this->varlang = $val;;
    }  
    
    /**
     * Implements property
     */    
    public function setVarparcat($val)
    {
        $this->varparcat = $val;;
    }   
    
    /**
     * Implements property
     */    
    public function setVarobj($val)
    {
        $this->varobj = $val;;
    } 
    
    /**
     * Implements property
     */    
    public function setVarsingleprops($val)
    {
        $this->varsingleprops = (array) $val;;
    } 
    
    /**
     * @inheritdoc
     */    
    public function formName()
    {
        return $this->varname . '_' . $this->varlang;
    }
    
    /**
     * If a lang of the model is not the default one
     * then singleprops are not permitted to edit
     * 
     * @inheritdoc
     */    
    public function safeAttributes()
    {
        $safe = parent::safeAttributes();
        $isMain = ($this->varlang == Yii::$app->urlManager->getDefaultLangCode());
        if (!$isMain) {
            $safe = array_diff($safe, $this->varsingleprops);
        }
        return $safe;    
    }
    
    /**
     * An opportunity to adjust anything before saving
     * 
     * For overloading
     * 
     * @api
     * @param boolean $isMain whether it is a model for default language
     */
    protected function beforeSave($isMain)
    {
       
    }    

    /**
     * Saving the model into database (into associated [[Variable]]) 
     */
    public function save()
    {
        if (!$this->varobj) {
            return;
        }
        // whether the main model is changing
        $isMain = ($this->varlang == Yii::$app->urlManager->getDefaultLangCode());
        $this->beforeSave($isMain);
        // saving the current model
        $this->varobj->conf = $this->attributes;
        $this->varobj->save(false);
        // saving the related models
        if ($isMain) {
            $langs = array_diff(Yii::$app->urlManager->getAllLanguageCodes(), [$this->varlang]);
            if (!empty($langs)) {
                $meta = Yii::$app->get('pageManager')->getVariables();
                $conf = [];
                if ($meta[$this->varname]['ignoreLang'] == 1){
                    $conf = $this->attributes;
                } else {
                    foreach ($this->varsingleprops as $name) {
                        $conf[$name] = $this->$name;
                    }
                }
                if ($conf) {
                    foreach ($langs as $lang) {
                        if ($model = Variable::getModelObject($this->varname, $lang, true)) {
                            $model->setAttributes($conf, false);
                            $model->save();
                        }
                    }
                }
            }
        }
    }
}

