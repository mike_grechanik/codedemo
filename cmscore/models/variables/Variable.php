<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\models\variables;

use Yii;

/**
 * This is the model class for table "{{%variable}}".
 * 
 * This table storages variables(per langusges) and their values
 *
 * @property integer $vid
 * @property string $name
 * @property string $lang
 * @property string $conf
 * @property string $categoryname
 * @property integer $parcatid
 * @property string $singleprops
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Variable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%variable}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'lang'], 'required'],
            [['conf', 'singleprops'], 'safe'],
            [['parcatid'], 'integer'],
            [['name', 'categoryname'], 'string', 'max' => 125],
            [['lang'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_FIND, [$this, 'myafterFind']);        
    }
    
    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */
    public function myafterFind($event)
    {
        $this->conf = ($this->conf == '') ? [] : unserialize($this->conf);
    }
    
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->conf = (empty($this->conf)) ? '' : serialize($this->conf);
            return true;
        }
        return false;
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vid' => Yii::t('cmscore', 'Vid'),
            'name' => Yii::t('cmscore', 'Name'),
            'lang' => Yii::t('cmscore', 'Lang'),
            'conf' => Yii::t('cmscore', 'Conf'),
            'categoryname' => Yii::t('cmscore', 'Categoryname'),
            'parcatid' => Yii::t('cmscore', 'Parcatid'),
            'singleprops' => Yii::t('cmscore', 'singleprops'),
        ];
    }
    
    /**
     * Returns the model which resides in this Variable
     * 
     * It is only for inner use during saving process (BaseModel::save()
     * Do not use from API, use SettingManager::getSettingModel because the latter
     * has not varobj
     * $forsave - for ignoreLang whether to return main or real object
     * 
     * @internal
     * @param string $name
     * @param string $lang
     * @param boolean $forsave Whether we are during saving process
     * @return \mgrechanik\cmscore\models\variables\BaseModel|null
     */
    public static function getModelObject($name, $lang, $forsave = false)
    {
        $meta = Yii::$app->get('pageManager')->getVariables();
        if (isset($meta[$name])) {
            if (($meta[$name]['ignoreLang'] == 1) && (!$forsave)) {
                // for ignore Lang object we manipulate always with it's default language version
                $lang = Yii::$app->urlManager->getDefaultLangCode();
            }
            if ($obj = self::findOne(['name' => $name, 'lang' => $lang])) {
                $class = $meta[$name]['modelClass'];
                $conf = array_merge(
                    $obj->conf,
                    $meta[$name]['classConfig'], //  <== "varsingleprops" is here
                    [
                        'varname' => $name,
                        'varlang' => $lang,
                        'varobj' => $obj,
                        'varparcat' => $obj->parcatid,
                    ]
                );
                return new $class($conf);
            }            
        }
        return null;
    }
}
