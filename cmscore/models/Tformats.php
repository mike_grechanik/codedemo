<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\models;

use Yii;
use mgrechanik\user\models\rbac\AuthItem;

/**
 * This is the model class for table "{{%tformats}}".
 *
 * This table storages text formats and it's filters and configs
 * Explanation
 * field filters is serialized array like ['filter1', 'filter2']
 * field filters_config are array of config for filters. It's structure is like:
 *   array(
 *      'filter1' = > $config_array,
 *      'filter2' = > $config_array,
 *   )
 * 
 * @property string $name
 * @property string $fullname
 * @property string $filters
 * @property string $filters_config
 * @property integer $status
 * @property integer $weight
 * @property integer $updated
 * @property string $helptext
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Tformats extends \yii\db\ActiveRecord
{
    /**
     * @var array Unserialized array of filters for this format 
     */
    protected $_filters = [];
    
    /**
     * @var array Unserialized array of filter configs for this format 
     */    
    protected $_filters_config = [];
    
    /**
     * @var self[] 
     * @see findCachedFormat()
     */
    protected static $_cache_formats = [];

    /**
     * Caching Tformat objects during mass operation.
     * 
     * When for announces I check for all of them whether cache rebuilding needed.
     * 
     * @api
     * @param string $name
     * @return self
     * @see \mgrechanik\cmscore\helpers\Common::whetherToRebuiltCache
     */
    public static function findCachedFormat($name)
    {
        if (!isset(self::$_cache_formats[$name])) {
            self::$_cache_formats[$name] = self::findOne($name);
        } 
        return self::$_cache_formats[$name];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tformats}}';
    }
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'handlerAfterUpdate']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'handlerAfterInsert']);
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'handlerAfterDelete']);
        
    } 
    
    /**
     * Returns an array of filters which are associated with this format
     * 
     * @return array
     */
    public function getFilters()
    {
        if (!$this->_filters) {
            if ($this->filters) {
                $this->_filters = unserialize($this->filters);
            }
            if ($this->filters_config) {
                $this->_filters_config = unserialize($this->filters_config);
            }           
        }
        return $this->_filters;
    }
    
    /**
     * Returns only shortcodes
     * 
     * @return array
     */
    public function getScodes()
    {
        $res = [];
        $meta = self::getFiltersMetaData();
        $all = $this->getFilters();
        foreach ($all as $filtr) {
            if (isset($meta[$filtr]) && !isset($meta[$filtr]['isPositional']) && !isset($meta[$filtr]['isNotReal'])) {
                $res[$filtr] = [
                    'name' => $meta[$filtr]['name'],
                    'category' => $meta[$filtr]['category'],
                    'modalHelpText' => isset($meta[$filtr]['modalHelpText']) ? $meta[$filtr]['modalHelpText'] : '',
                ];    
            }
        }
        return $res;
    }
    
    /**
     * Returns an array of help texts for all filters and shortcodes of this text format
     * 
     * They have already been translated. ('filters' group)
     * 
     * @return array
     */
    public function getHelpTexts()
    {
        $res = [];
        $meta = self::getFiltersMetaData();
        $all = $this->getFilters();
        foreach ($all as $filtr) {
            if (isset($meta[$filtr]) && isset($meta[$filtr]['formatHelpText'])) {
                $res[$filtr] = Yii::t('filters', $meta[$filtr]['formatHelpText']);
            }
        }
        return $res;
    }    

    /**
     * Returns meta data about filters gathered by spaider
     * 
     * @return array
     */
    protected static function getFiltersMetaData()
    {
        return Yii::$app->get('pageManager')->getFilters();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname'], 'required'],
            [['status', 'weight'], 'integer'],
            [['status'], 'in', 'range' => [0,1]],
            [['fullname'], 'string', 'max' => 120],
            [['fullname'], 'checkFullname'],
            [['helptext'], 'safe'],
        ];
    }
    
    /**
     * Implementation of inline validator
     * 
     * Checks the name of text format for being unique.
     * 
     * @param string $attribute
     * @param array $params
     * @return void
     */
    public function checkFullname($attribute, $params)
    {
        if ($attribute != 'fullname') {
            return;
        }
        $fullname = trim($this->$attribute);
        $name = preg_replace('/[^a-zа-я0-9ё\s]/ui', '', $fullname);
        $name = str_replace(' ', '_', $name);
        $name = mb_strtolower($name);
        $error = false;
        if ($this->isNewRecord) {
            $this->name = $name;
        }
        if ($format = self::findOne($name)) {
            $error = true;
            if (!$this->isNewRecord && ($this->name == $format->name)) {
                $error = false;
            }
        }
        if ($error) {
            $this->addError($attribute, Yii::t('cmscore', 'Text format with this name already exists'));
        }
    }    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('cmscore', 'Name'),
            'fullname' => Yii::t('cmscore', 'Text format full name'),
            'filters' => Yii::t('cmscore', 'Filters'),
            'filters_config' => Yii::t('cmscore', 'Filters Config'),
            'status' => Yii::t('cmscore', 'Status'),
            'weight' => Yii::t('cmscore', 'Weight'),
            'helptext' => Yii::t('cmscore', 'Help text for this format'),
        ];
    }
    
    /**
     * @inheritdoc
     */    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->updated = time();
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */
    public function handlerAfterUpdate($event)
    {
        $auth = Yii::$app->authManager;
        $key = 'text_formats_use_next_' . $this->name;
        if ($ai = AuthItem::findOne($key)) {
            $ai->permweight = $this->weight;
            $ai->valsfortranslate = serialize([
                'formatname' => $this->fullname,
            ]);
            $ai->save(false);
        }                
    }  
    
    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */    
    public function handlerAfterInsert($event)
    {
        // permissions for this menu               
        $auth = Yii::$app->authManager;
        $key = 'text_formats_use_next_' . $this->name;
        $perm = $auth->createPermission($key);
        $perm->description = 'Use the next text format - {formatname}';
        $auth->add($perm);
        if ($ai = AuthItem::findOne($key)) {
            $ai->isfromspider = 0;
            $ai->isshowatadminpage = 1;
            $ai->permcategory = 'Text formats';
            $ai->permweight = $this->weight;
            $ai->valsfortranslate = serialize([
                'formatname' => $this->fullname,
            ]);
            $ai->save(false);
        }        
    }  
    
    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */    
    public function handlerAfterDelete($event)
    {
        $auth = Yii::$app->authManager;
        $key = 'text_formats_use_next_' . $this->name;
        if ($perm = $auth->getPermission($key)) {
            $auth->remove($perm);
        }
    }
    
    /**
     * Processing a text with choosen($this) text format
     * 
     * Flag $flags['includeAssets'] = 1; needs to be set inside callback if
     * the shortcode really exists in the text and I need to register assets.
     * Because if there is no representation of the shortcode then tere is no
     * need for it's assets eighter
     * 
     * @param string $text Text being processed according current text format
     * @param array $data The additional data which is filled inside this function
     * @see \mgrechanik\cmscore\helpers\Common::format()
     */
    public function process(&$text, &$data)
    {
        $meta = self::getFiltersMetaData();
        $data['updated'] = $this->updated;
        if ($filters = $this->getFilters()) {
            foreach ($filters as $filter) {
                if (isset($meta[$filter])) {
                    $fullfilter = $meta[$filter];
                    if (isset($fullfilter['isNotReal']) || !isset($fullfilter['callback'])) {
                        continue;
                    }                    
                    $config = (isset($this->_filters_config[$filter])) ? $this->_filters_config[$filter] : [];
                    $callable = $fullfilter['callback'];
                    if (is_callable($callable)) {
                        $flags = [];
                        $text = call_user_func_array($callable, [$text, $config, &$flags]);
                        if (isset($fullfilter['data']['assets']) && isset($flags['includeAssets'])) {
                            if (!isset($data['assets'])) {
                                $data['assets'] = [];
                            }
                            $assets = array_merge($data['assets'], $fullfilter['data']['assets']);
                            $data['assets'] = $assets;
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Figures out which text format has an image button
     * 
     * @return string Json encoded string (['formatHasIt' => 1,...])
     * @see /mgrechanik/ctypes/page/views/admin/tasks/_form.php
     */
    public static function getJsonFormatsWithImageButton()
    {
        $res = [];
        $all = self::findAll(['status' => 1]);
        foreach ($all as $obj) {
            $filters = $obj->getFilters();
            if (in_array('imageinsertbutton', $filters)) {
                $res[$obj->name] = 1;
            }
        }
        return json_encode($res);
    }
    
}
