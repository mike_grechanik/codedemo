<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\services;

use Yii;
use yii\data\Pagination;
use mgrechanik\cmscore\models\Pagetracker;
use mgrechanik\cmscore\models\ArticleInterface;
use mgrechanik\cmscore\helpers\Common;

/**
 * Service to display a list of announces of nodes
 * 
 * Example of use:
 * ```code
 *       $tracker = new NodeTracker([
 *           'types' => [2],
 *           'templateData' => [
 *               2 => [
 *                   'templatePath' => '@mgrechanik/ctypes/article/views/announces/simple.php',
 *                   'params' => [],
 *               ],
 *           ],
 *           'lang' => Yii::$app->getUrlManager()->getLanguageCode(),
 *           'andWhere' => ['status' => 1],
 *           'defaultPageSize' => (int) SettingsManager::getSettingValue('mainpage_articles_number', 'val', 10),
 *       ]);
 *       $tracker->prepare();
 *       
 *       return $this->render('pages', ['res' => $tracker->res, 'paginator' => $tracker->paginator]);
 * ```
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class NodeTracker extends \yii\base\Object
{
    /**
     * @var array List of content types of nodes to display
     */
    public $types = [];
    
    /**
     * @var array Data about templates to display announces.
     * It's format:
     * [
     *     nodetype => [
     *        'templatePath' => '',
     *        'params' => [] //optional
     *     ],
     *     ...
     * ]
     */
    public $templateData = [];
    
    /**
     * @var string Short code of the language of the nodes. Null for all languages.
     */
    public $lang;
    
    /**
     * @var mixed Additional restriction for choosing the nodes 
     */
    public $andWhere;

    /**
     * @var string 
     */
    public $orderBy = 'created desc';
    
    /**
     * @var string The format of created date time value. It is for date() function.
     */
    public $createdFormat = 'd-m-Y H:i';
    
    /**
     * @var string The text of the "Read more" link. It's translate group is [[translateGroup]].
     */
    public $readMoreText = 'Read more';
    
    /**
     * @var string The translate group for [[readMoreText]]
     */
    public $translateGroup = 'cmscore';

    /**
     * @var integer Amount of announces per page
     */
    public $defaultPageSize = 10;

    /**
     * @var array List of announces ready to be shown 
     */
    public $res = [];
    
    /**
     * @var Pagination  Paginator 
     */
    public $paginator;
    
    /**
     * @var array Classes checked of being of ArticleInterface
     */
    protected $_checkedClasses = [];

    /**
     * Building the result data
     */
    public function prepare()
    {
        if (empty($this->types)) {
            return;
        }
        $where = ['pagetype' => $this->types];
        if ($this->lang) {
            $where['lang'] = $this->lang;
        }
        $query = Pagetracker::find()->where($where);
        if ($this->andWhere) {
            $query->andWhere($this->andWhere);
        }
        $countQuery = clone $query;
        $this->paginator = $paginator = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize' => $this->defaultPageSize, 
            'forcePageParam' => false,
        ]);        
        $res = $query
                ->offset($paginator->offset)
                ->limit($paginator->limit)
                ->orderBy($this->orderBy)
                ->asArray()
                ->all();
        $ctypes = Yii::$app->pageManager->getContentTypes();
        foreach ($res as $key => $val) {
            if (!$val['tformat']) {
                continue;
            }
            $modelClass =  null;
            if (isset($ctypes[$val['pagetype']]['contentModelClass'])) {
                $modelClass = $ctypes[$val['pagetype']]['contentModelClass'];
            } else {
                continue;
            }
            if (!$this->isModelClassOfArticleInterface($modelClass)) {
                continue;
            }
            // --- CACHING
            $process = Common::whetherToRebuiltCache($val['tformat'], $val['announce'], $val['cache_data']);
            if ($process) {
                if (!($model = $modelClass::findOne($val['nid']))) {
                    continue;
                }
                $ctype = (int) $val['pagetype'];
                if (!isset($this->templateData[$ctype]['templatePath'])) {
                    continue;
                }
                $res2 = $model->getAnnounceData(
                    $this->templateData[$ctype]['templatePath'],
                    (isset($this->templateData[$ctype]['params'])) ? $this->templateData[$ctype]['params'] : []
                );
                $res[$key]['announce'] = $res2['text'];
                // saving the cache
                $data = serialize($res2['data']);
                Pagetracker::updateAll(['announce' => $res2['text'], 'cache_data' => $data],
                        'nid=:nid and pagetype=:pagetype', ['nid' => $val['nid'], 'pagetype' => $val['pagetype']]);
                // end saving the cache
            } else {
                $data = [];
                if ($val['cache_data']) {
                    $data = unserialize($val['cache_data']);
                }
                Common::processFormatData($data);
            }
            // --- END CACHING    
            $res[$key]['announce'] = strtr($res[$key]['announce'], [
                '%created%' => date($this->createdFormat, (int) $res[$key]['created']),
                '%readmore%' => Yii::t($this->translateGroup, $this->readMoreText),
            ]);
        } 
        $this->res = $res;
    }

    /**
     * Checks whether the class is one of article interface.
     * 
     * @param string $class
     * @return boolean
     */
    protected function isModelClassOfArticleInterface($class)
    {
        if (!isset($this->_checkedClasses[$class])) {
            $this->_checkedClasses[$class] = (new $class) instanceof ArticleInterface;
        }
        return $this->_checkedClasses[$class];
    }
}
