<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\behaviors;

use mgrechanik\cmscore\models\Pagealias;
use mgrechanik\cmscore\models\Page;
use yii\db\ActiveRecord;
use mgrechanik\cmscore\helpers\Common;

/**
 * Field behavior to manage aliases per node/page.
 * 
 * @see \mgrechanik\cmscore\models\ProgramPage
 * @see \mgrechanik\ctypes\page\models\PageNode
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class AliasFieldBehavior extends FieldsBehavior
{
    /**
     * @var string The path to a template with this behavior model
     */    
    public $template = '@mgrechanik/cmscore/models/fieldsviews/aliasfieldedit.php';

    /**
     * @var boolean Whether to suggest auto value. 
     */
    public $isSuggestAuto = true;
    
    /**
     * @var string The name of the token group tokens from which
     * will be used to generate auto value. 
     */
    public $tokengroup = '';

    /**
     * @inheritdoc
     */      
    public function initModel()
    {
        $mainModel = $this->owner;
        if ($mainModel->isNewRecord || (!$mainModel->pageid)) {
            $this->model = new Pagealias();
        } else {
            $fm2 = Pagealias::findOne($mainModel->pageid);
            $this->model = $fm2;
            if (!$fm2) {
                $fm2 = new Pagealias();
                $this->model = $fm2;
                $this->adjustModels();
            }
        }
        $this->model->setLang($mainModel->lang);
    }  
    
    /**
     * @inheritdoc
     */       
    public function saveModel()
    {
        if (trim($this->model->alias) == '') {
            $this->model->delete();
        } else {
            $this->model->save(false);
        }
    }            

    /**
     * @inheritdoc
     */       
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    } 
    
    /**
     * Event handler.
     * 
     * @param \yii\base\Event $event
     */
    public function beforeDelete($event)
    {
        if (!$this->model) {
            $this->initModel();
        }
        if ($this->model) {
            if ($page = $this->model->page) {
                $page->delete();
            }
        }        
    }
    
    /**
     * Event handler.
     * 
     * @param \yii\base\Event $event
     */    
    public function beforeValidate($event)
    {
        if ((trim($this->model->alias) == '') && (isset($_POST['aliasautomatic']))) {
            if ($this->isSuggestAuto) {
                $template = Common::getPathAutoTemplateForContent($this->owner->contentType);
                $this->model->alias = Common::processTokens($template, $this->tokengroup, ['nodeobject' => $this->owner]);
            }            
        }
    }

    /**
     * Event handler.
     * 
     * @param \yii\base\Event $event
     */    
    public function afterInsert($event)
    {
        $ow = $this->owner;
        $innerUrl = str_replace('<nid>', $ow->nid, $ow->innerUrlTemplate);
        $page = new Page([
            'lang' => $ow->lang,
            'source' => $innerUrl,
        ]);
        if ($page->save()) {
            $ow->pageid = $page->pid;
        }        
    }    
}

