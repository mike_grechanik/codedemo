<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\behaviors;

use yii\db\ActiveRecord;
use mgrechanik\cmscore\models\MaterializedPathInterface;

/**
 * This behavior turns AR object into the node of Materialized Path tree
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class MaterializedPath extends \yii\base\Behavior
{
    /**
     * @var boolean Whether to move children and not delete.
     * When the node is being deleted it's children will be:
     * - moved to element's parent if true
     * - deleted if false 
     */
    public $moveChildrenAtDelete = true;

    /**
     * @var array Names of the mp fields
     * @see attach() 
     */
    protected $fieldnames = [];

    /**
     * @inheritdoc
     */  
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'handlerBeforeDelete',
        ];
    }
    
    /**
     * @inheritdoc
     */      
    public function attach($owner)
    {
        parent::attach($owner);
        if (!($this->owner instanceof MaterializedPathInterface)) {
            throw new \Exception(get_class($this->owner) . ' has to implement MaterializedPathInterface');
        }
        $this->fieldnames = $this->owner->getMpNames();
    }

    /**
     * Getting query object specified with tree condition of the current node
     * 
     * @return \yii\db\Query Query object with treeCondition set
     */
    public function qfind()
    {
        $cond = $this->owner->getTreeCondition();
        $class = get_class($this->owner);
        $query = $class::find();
        if (!empty($cond)) {
            $query->where($cond);
        }
        return $query;
    }
    
    /**
     * Getting the query object specified to get all sub tree nodes of the owner node
     * 
     * @return \yii\db\Query 
     */
    public function getSubTreeQuery()
    {
        $ow = $this->owner;
        $path = $ow->getFullPath();
        if ($path === '') {
            $query = $this->qfind();
        } else {
            $query = $this->qfind()->andWhere(['like', $this->fieldnames['path'], "$path%", false]);
        }
        return $query->orderBy($this->fieldnames['level'] . ',' . $this->fieldnames['weight']);
    }
    
    /**
     * Getting the query object specified to get children nodes of the owner node
     * 
     * @return \yii\db\Query
     */
    public function getChildrenQuery()
    {
        $ow = $this->owner;
        $path = $ow->getFullPath();
        return $this->qfind()
                ->andWhere([$this->fieldnames['path'] => $path])
                ->orderBy($this->fieldnames['level'] . ',' . $this->fieldnames['weight']);
    }

    /**
     * The full path of the node.
     * 
     * Empty string for a root node.
     * And $ow->path . $ow->id . '/'; for all other nodes
     * 
     * @return string
     */
    public function getFullPath()
    {
        $ow = $this->owner;
        if ($ow->isNewRecord && ($ow->{$this->fieldnames['name']} == '__ROOT__')) {
            return '';
        }
        return $ow->{$this->fieldnames['path']} . $ow->{$this->fieldnames['id']} . '/';
    }
    
    /**
     * Getting the level of the node
     * 
     * @return integer
     */
    public function getFullLevel() 
    {
        $ow = $this->owner;
        if ($ow->isNewRecord && ($ow->{$this->fieldnames['name']} == '__ROOT__')) {
            return 0;
        }
        return $ow->{$this->fieldnames['level']};        
    }

    /**
     * Helper to get nodes also by their pk identifier from the same tree
     * 
     * @param ActiveRecord|integer)null $nid The identifier of the node needed. Null for a root node.
     * @return ActiveRecord|null 
     */
    public function getNode($nid = null)
    {
        if ($nid instanceof ActiveRecord) {
            return $nid;
        }
        $node = null;
        if (is_numeric($nid)) {
            $node = $this->qfind()->andWhere([$this->fieldnames['id'] => $nid])->one();
        } elseif ($nid === null) {
            // ROOT node for this tree
            $ow = $this->owner;
            $class = get_class($ow);
            $node = $class::getRootNode($ow->getTreeCondition());
        }
        return $node;
    }
    
    /**
     * Adds the current node to the other one
     * 
     * The new record has to have tree condition (if there is) set before calling this method.
     * 
     * ```php
     * $snew = new Sometree(['treeid' => 1, 'name' => 'new record']);
     * $snew->addToNode(...)
     * ```
     * 
     * @param ActiveRecord|integer|null $nid The pk of existed node to which we add current one
     * @param boolean $save Whether to save the the model
     */
    public function addToNode($nid = null, $save = true)
    {
        if ($node = $this->getNode($nid)) {
            $ow = $this->owner;
            $idname = $this->fieldnames['id'];
            if (!is_null($node->{$idname}) && ($ow->{$idname} == $node->{$idname})) {
                // first check was for a root node
                return;
            }            
            $ow->{$this->fieldnames['path']} = $node->getFullPath();
            $ow->{$this->fieldnames['weight']} = $node->getChildrenQuery()->max($this->fieldnames['weight']) + 1;
            $ow->{$this->fieldnames['level']} = $node->getFullLevel() + 1;
            if ($save) {
                $ow->save(false);
            }
        }
    }
    
    /**
     * Returns the identifier of the parent node.
     * 
     * If parent node is a root it will return null.
     * 
     * @return string|null
     */
    public function getParentId()
    {
        $ow = $this->owner;
        $path = $ow->{$this->fieldnames['path']};
        $path = rtrim($path, '/');
        $parts = explode('/', $path);
        $id =(int) array_pop($parts);
        return ($id) ? $id : null;
    } 
    
    /**
     * Returns the identifiers of the parent nodes.
     * 
     * @return array
     */
    public function getParentIds()
    {
        $ow = $this->owner;
        $path = $ow->{$this->fieldnames['path']};
        $path = rtrim($path, '/');
        if ($path) {
            return explode('/', $path);
        }
        return [];
    }     

    /**
     * Return the parent node to the current one.
     * 
     * @return ActiveRecord
     */
    public function getParent()
    {
        return $this->getNode($this->getParentId());
    }

    /**
     * Moves current node to be the child of another node
     * 
     * @param ActiveRecord|integer|null $nid The pk of existed node to which we are moving current one
     * @param boolean $save Whether to save the the model
     */
    public function moveToNode($nid = null, $save = true)
    {
        if ($node = $this->getNode($nid)) {
            $ow = $this->owner;
            $idname = $this->fieldnames['id'];
            //not a root node
            if (!is_null($node->{$idname})) {
                // to the same node
                if ($ow->{$idname} == $node->{$idname}) {
                    return;
                }
                // moving to one of descendants
                if (in_array($ow->{$idname}, $node->getParentIds())) {
                    return;
                }
            }
            $oldfullpath = $ow->getFullPath();
            $oldfulllevel = $ow->getFullLevel();
            $descendants = $ow->getSubTreeQuery()->all();
            // changing the node
            $ow->{$this->fieldnames['path']} = $node->getFullPath();
            $ow->{$this->fieldnames['weight']} = $node->getChildrenQuery()->max($this->fieldnames['weight']) + 1;
            $ow->{$this->fieldnames['level']} = $node->getFullLevel() + 1;
            if ($save) {
                $ow->save(false);
            }
            // changing the descendants of node
            $newfullpath = $ow->getFullPath();
            $newfulllevel = $ow->getFullLevel();
            $leveldiff = $newfulllevel - $oldfulllevel;
            foreach ($descendants as $child) {
                $child->adjust($oldfullpath, $newfullpath, $leveldiff);
                $child->save(false);
            }
        }
    }
    
    /**
     * Helper function to a moving process
     * 
     * @param string $oldpath
     * @param string $newpath
     * @param integer $leveldiff
     */
    public function adjust($oldpath, $newpath, $leveldiff)
    {
        $ow = $this->owner;
        $path = $ow->{$this->fieldnames['path']};
        $path = $newpath . str_replace($oldpath, '', $path);
        $ow->{$this->fieldnames['path']} = $path;
        $ow->{$this->fieldnames['level']} = $ow->{$this->fieldnames['level']} + $leveldiff;
    }

    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */
    public function handlerBeforeDelete($event)
    {
        $ow = $this->owner;
        if ($move = $this->moveChildrenAtDelete){
            $parid = $ow->getParentId();
        }
        foreach ($ow->getChildrenQuery()->all() as $child) {
            if ($move) {
                $child->moveToNode($parid);
            } else {
                $child->delete();
            }
        }
    }
    
}
