<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div>
    <h2><em><?= Yii::t('filters' , 'Permanent link')?></em></h2>
    <?php $form = ActiveForm::begin([
    'id' => 'model-js-form',
    'validationUrl' => ['/admin/admin/page/scodevalidate', 'code' => 'permlink'],
]) ?>
    <?= $form->field($model, 'pid', ['enableAjaxValidation' => true])->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'text')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'isabsolute')->dropDownList([
        0 => Yii::t('cmscore', 'No'),
        1 => Yii::t('cmscore', 'Yes'),
    ]) ?>
    <?php
    print Html::submitButton(Yii::t('cmscore', 'Insert'), ['id' => 'inserttotext', 'class' => 'btn btn-primary']) 
    ?>
    <?php ActiveForm::end()  ?>
</div>
