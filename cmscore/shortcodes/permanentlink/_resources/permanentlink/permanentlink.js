var scodeInitFunc = function(){
    $('#model-js-form').on('ajaxComplete', function (event, jqXHR, textStatus){
        if ($('div.field-plinkmodel-pid').hasClass('has-error')) {
            return;
        }
        var val = $('#plinkmodel-pid').val();
        scodewaitAjax = true;
        $.ajax({
            url: '/admin/admin/page/gettitleforpageid?pid=' + val,
            type: "GET",
            data: {},
            success: function (data) {
                scodewaitAjax = false;
                if (data) {
                    $('#plinkmodel-title').val(data.title);
                }
            }
        });                        
    });    
};

var scodeResultFunc = function($modal, $textarea){
    var htmlEscape = function(str) {
        return String(str)
                .replace(/&/g, '&amp;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&#39;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;');
    };    
    var abs = '';
    var title = '';
    if ($('#plinkmodel-isabsolute').val() == 1) {
        abs = '|a';
    }
    if ($('#plinkmodel-title').val()) {
        title = ' title="' + htmlEscape($('#plinkmodel-title').val()) + '"';
    }    
    var text = '<a href="[urlfrompageid:' + $('#plinkmodel-pid').val() 
    + abs + ']"' + title + '>' + $('#plinkmodel-text').val() + '</a>';
    window.editorinsert($textarea, text);
    $modal.modal('hide');
    
    // mandatory to do for every type of result function !!!
    scodewaitAjax = false;
    // end mandatory
}


