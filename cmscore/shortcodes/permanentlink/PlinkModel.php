<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\shortcodes\permanentlink;

use Yii;
use mgrechanik\cmscore\models\Page;
use mgrechanik\cmscore\helpers\Common;

/**
 * Model to represent a form with permament link inserting shortcode functionality.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class PlinkModel extends \yii\base\Model 
{
    /**
     * @var integer The page number 
     */
    public $pid;
    
    /**
     * @var string Text of the link
     */
    public $text = '';
    
    /**
     * @var string Attribute title of the link.
     */
    public $title = '';
    
    /**
     * @var integer Whether the link has an absolute address 
     */
    public $isabsolute = 0;

    
    //del??  protected $pageid = null;

    /**
     * @inheritdoc
     */       
    public function attributeLabels()
    {
        return [
            'pid' => Yii::t('filters', 'Page number'),
            'text' => Yii::t('filters', 'The text of the link'),
            'title' => Yii::t('filters', '"title" attribute of this link'),
            'isabsolute' => Yii::t('filters', 'Is this link absolute?'),
        ];
    }    

    /**
     * @inheritdoc
     */       
    public function rules()
    {
        return [
            [['pid', 'isabsolute', 'text'], 'required'],
            [['pid'], 'exist', 'targetClass' => Page::className(), 'targetAttribute' => 'pid'],
            [['isabsolute'], 'integer'],
            [['isabsolute'], 'in', 'range' => [0, 1]],
            [['title', 'text'], 'string', 'max' => 255],

        ];        
    }
        
    /**
     * Processing permanent link shortcodes into urls
     * 
     * It is an implementation of filter callback for a Spider.
     * 
     * @param string $text
     * @param array $config
     * @param array $flags
     * @return text
     */
    public static function process($text, $config, &$flags)
    {
        $res = preg_replace_callback('/\[urlfrompageid:(\d+)(?:\|(a))?\]/', function($matches){
            $pid = (int) $matches[1];
            $scheme = (int) isset($matches[2]);
            $url = Common::urlToPage($pid, [], $scheme);
            if (!$url) {
                $url = 'deleteemptyurl';
            }
            return $url;
        }, $text);
        return str_replace('href="deleteemptyurl"', '', $res);
    }

}

