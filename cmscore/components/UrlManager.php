<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\components;

use Yii;
use yii\base\InvalidConfigException;
use mgrechanik\cmscore\components\url\LanguageMode;
use mgrechanik\cmscore\models\Page;

/**
 * UrlManager of the cmf.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class UrlManager extends \yii\web\UrlManager
{
    /**
     * @var string Language code.
     * We set this in [[createUrl()]] for AliasUrlRule to know 
     * for what language we are creating an url;  
     */
    public $currentCreateLangCode = '';
    
    /**
     * @var string|array The innerurl of the front page
     * For one route:
     *       'frontPage' =>  'site/pages',
     * If we have different url's per language
     *       'frontPage' =>  [
     *          'ru' => 'page/main/view?id=19',
     *          'en' => 'page/main/view?id=20',
     *       ],   
     */
    public $frontPage = '';

    /**
     * @var \mgrechanik\cmscore\components\url\OneLanguageMode The language object.
     * It handles language operations and mode.
     */
    protected $language;

    /**
     * @var array Cross domain settings set via config 
     *  [] - if admin pages are at the same domain
     *  And for a different admin
     *  [
     *    'http://www.site.com' => 'front',
     *    'http://www.adminsite.com' => 'back',
     *  ]
     */
    public $crossDomainSettings = [];

    /**
     * @var string Current cross admin state 
     * It might be 'front', 'back' or 'both'
     * 'both' is used when admin pages are at the same domain with front pages.
     */
    protected $crossDomainAdmin = 'both';

    /**
     * @var array Cross admin adresses in the format [mode=>domain,..] 
     */
    protected $crossAdminAdresses = [];

    /**
     * @var \mgrechanik\cmscore\components\url\AliasUrlRule Alias url rule object
     */
    protected $_aliasRule;
    
    /**
     * @inheritdoc
     */       
    public function init()
    {
        parent::init();
        if ((null === $this->language) ||
           (!($this->language instanceof LanguageMode))) {
            throw new InvalidConfigException('This implementation of UrlManager has to have a "language" property set.');
        }
        $this->checkcrossdomain();
    }
    
    /**
     * Checking cross domain settings.
     *  
     * @throws InvalidConfigException
     */
    protected function checkcrossdomain()
    {
        $domain = Yii::$app->getRequest()->getHostInfo();
        $settings = $this->crossDomainSettings;
        if (!empty($settings)) {
           if (isset($settings[$domain])) {
               $this->crossDomainAdmin = $settings[$domain];
           } else {
               throw new InvalidConfigException('This implementation of UrlManager has a wrong implementation of $crossDomainSettings property');
           }
        }
        // it is a redirect from address like 'http://admin123.delsite.ru'
        // redirect from empty address to the address with a cobtroller path
        if ($this->crossDomainAdmin == 'back' && ($domain == rtrim(Yii::$app->getRequest()->getAbsoluteUrl(), '/'))) {
            header('Location: ' . $domain . '/admin/admin/page/main');
            exit();
        }
        if ($this->crossDomainAdmin != 'both') {
            Yii::$app->on(\yii\base\Application::EVENT_AFTER_REQUEST, ['mgrechanik\cmscore\components\UrlManager', 'checkCrossFromAdmin']);
        }
    }
    
    /**
     * Implementation of event handler for \yii\base\Application::EVENT_AFTER_REQUEST event.
     * 
     * @param ]yii\base\event $event
     */
    public static function checkCrossFromAdmin($event)
    {
        if (Yii::$app->urlManager->crossDomainAdmin == 'back' && 
            !Yii::$app->get('pageManager')->isthisadminpage) {
            // redirect to front
            $domain = Yii::$app->urlManager->getCrossAdresses()['front'];
            header('Location: ' . $domain . Yii::$app->request->getUrl());
            exit();
        }
        if (Yii::$app->urlManager->crossDomainAdmin == 'front' && 
            Yii::$app->get('pageManager')->isthisadminpage) {
            // redirect to back
            $domain = Yii::$app->urlManager->getCrossAdresses()['back'];
            header('Location: ' . $domain . Yii::$app->request->getUrl());
            exit();
        }        
    }

    /**
     * Getter for a [[crossDomainAdmin]]
     * 
     * @api
     * @return array Current cross admin state 
     */
    public function getCrossDomainAdmin()
    {
        return $this->crossDomainAdmin;
    }
    
    /**
     * Getter for a [[crossAdminAdresses]]
     * 
     * @api
     * @return array Cross admin addresses per mode (['back' => domain,...])
     */    
    public function getCrossAdresses()
    {
        if (empty($this->crossAdminAdresses)) {
            foreach ($this->crossDomainSettings as $k => $v) {
                $this->crossAdminAdresses[$v] = $k;
            }
        }
        return $this->crossAdminAdresses;
    }
    
    /**
     * Returns domain url address of the site's front .
     * 
     * Returns domain address to a front of this site.
     * getHostOfFront() will give:
     * 'http://site.com/'
     * 
     * @api
     * @param string $suffix Suffix to add to address.
     * @return string
     */
    public function getHostOfFront($suffix = '/')
    {
        if ($this->crossDomainAdmin == 'both') {
            $host = Yii::$app->getRequest()->getHostInfo();
        } else {
            $host = $this->getCrossAdresses()['front'];
        }
        return $host . $suffix;
    }

    /**
     * @inheritdoc
     */        
    public function parseRequest($request)
    {
        $this->language->parseRequest($request);
        // We need this for $controller->goHome() functionality
        Yii::$app->homeUrl = \yii\helpers\Url::to(['/']);
        return parent::parseRequest($request);
    }
    
    /**
     * Creating the url.
     * 
     * You might use the next additional keys:
     * [
     *     'lang' => Short language code
     *     'cross' => 'back'/'front'
     * ]
     * For a front page use '' or '<front>' for params[0]
     * 
     * @inheritdoc
     * @param array $params
     * @return string
     */
    public function createUrl($params)
    {
        $params = (array) $params;
        if (isset($params['lang'])) {
            $code = $params['lang'];
            unset($params['lang']);
        } else {
            $code = $this->getLanguageCode();
        }
        // checking for FrontPage
        $this->checkFrontPage($params, $code);
        $cross = false;
        if (isset($params['cross'])) {
            $cross = $params['cross'];
            unset($params['cross']);
        }
        $this->currentCreateLangCode = $code;
        $url = parent::createUrl($params);
        $url = $this->language->changeUrl($url, $code);
        if ($cross) {
            $this->crossDomainCreateAdjust($url, $cross);
        }
        return $url;
    }
    
    /**
     * Modifying url if it should lead to another domain.
     * 
     * @param string $url
     * @param string $cross
     */
    protected function crossDomainCreateAdjust(&$url, $cross)
    {
        if (($this->crossDomainAdmin == 'both') || ($this->crossDomainAdmin == $cross)) {
            return;
        }
        $adresses = $this->getCrossAdresses();
        if (isset($adresses[$cross])) {
            $url = $adresses[$cross] . $url;
        }
    }
    
    /**
     * Checking for a front page.
     * 
     * @see \mgrechanik\cmscore\components\url\AliasUrlRule::getIsToFront()
     * @param array $params
     * @param string $code
     */
    protected function checkFrontPage(&$params, $code)
    {
        if (isset($params[0])){
            $route = trim($params[0], '/');
            if ( ($route == '<front>') 
                 ||
                 (!is_array($this->frontPage) && ($route == $this->frontPage))    
               ) {
                $params[0] = '';
            } 
        }

        // Plus an array check is done in [[AliasUrlRule]] because 
        // there we determine full innerUrl
    }
    
    /**
     * Checks if this lang and innerUrl leads to front page.
     * 
     * @param string $lang Short code of the lang
     * @param string $innerUrl
     * @return boolean
     */
    public function isFrontPage($lang, $innerUrl)
    {
        $fr = $this->frontPage;
        if (is_array($fr)) {
            if (isset($fr[$lang])) {
                return ($fr[$lang] == $innerUrl);
            }
        } else {
            return ($fr == $innerUrl);
        }
        return false;
    }    

    /**
     * Setter for a language mode object property.
     * 
     * @param array $language
     */
    public function setLanguage($language)
    {
        $this->language = Yii::createObject($language);
    }
    
    /**
     * Getter for a language mode object property.
     * 
     * @return \mgrechanik\cmscore\components\url\OneLanguageMode
     */
    public function getLanguage()
    {
        return $this->language;
    } 
    
    /**
     * Returns all language codes. The code of the default language is the first.
     * 
     * Returns an array of all language codes with the code for the 
     * default language at the beginning or the array.
     * like ['ru','en']
     * 
     * @api
     * @return array
     */
    public function getAllLanguageCodes()
    {
        $def = $this->getDefaultLangCode();
        $res = [$def];
        foreach ($this->language->langCodes as $k => $val) {
            if ($k != $def) {
                $res[] = $k;
            }
        }
        return $res;
    }
    
    /**
     * Returns key=>value array of all languages. Default language is the first.
     * 
     * Used to create admin interface, items for input etc.
     * Example of result:
     *   array (size=2)
     *     'ru' => string 'Русский'
     *     'en' => string 'English'
     * 
     * @api
     * @return array
     */
    public function getOptionsForAllLanguages()
    {
        $defs = $this->language->langCodes;
        $codes = $this->getAllLanguageCodes();
        $res = [];
        foreach ($codes as $code) {
            $res[$code] = $defs[$code]['label'];
        }
        return $res;
    }


    /**
     * Returns the short code of the default language. 
     * 
     * @api
     * @return string The language code
     */
    public function getDefaultLangCode()
    {
        return $this->getLanguageCode($this->language->defaultLanguage);
    }

    /**
     * Returns the short code of the language. Like 'ru'. 
     * 
     * @api
     * @param null|string $langfull The full name of the language
     * @return language code descriptor
     */    
    public function getLanguageCode($langfull = null)
    {
        if (empty($langfull)) {
            $langfull = Yii::$app->language;
        }    
        if (isset($this->language->languages[$langfull])) {
            return $this->language->languages[$langfull]['code'];
        }
        return null;
    }
    

    
    /**
     * Returns whether short language code exists.
     * 
     * @api
     * @param string $code The short language code
     * @return boolean
     */
    public function isLangCodeExists($code) 
    {
        if (isset($this->language->langCodes[$code])) {
            return true;
        }
        return false;
    }
    
    /**
     * Returns whether there is only one language in the system.
     * 
     * @api
     * @return boolean
     */
    public function isOneLanguage()
    {
        return (count($this->language->languages) == 1);
    }
    

    
    /**
     * Returns alias url rule object.
     * 
     * Look at [[getPageInfoByPid()]] 
     * @see \mgrechanik\cmscore\maintenance\behaviors\MaintenanceAllowedurlsAdjustBehavior
     * @return \mgrechanik\cmscore\components\url\AliasUrlRule
     */
    public function getAliasRule()
    {
        if ($this->_aliasRule === null) {
            $this->_aliasRule = false;
            foreach ($this->rules as $rule) {
                if ($rule instanceof \mgrechanik\cmscore\components\url\AliasUrlRule) {
                    $this->_aliasRule = $rule;
                    break;
                }
            }
        }
        return $this->_aliasRule;
    }
    
    /**
     * Returns information about page by it's number ($pid).
     * 
     * Result's format is: ['source'=>, 'lang'=>] or false if page has not been found.
     * 
     * @see \mgrechanik\cmscore\helpers\Common::urlToPage()
     * @param type $pid
     * @return array|boolean
     */
    public function getPageInfoByPid($pid)
    {
         if ($rule = $this->getAliasRule()) {
             return $rule->getPageInfoByPid($pid);
         }
         return false;
    }
    
}
