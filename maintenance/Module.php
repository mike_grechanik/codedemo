<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\maintenance;

use Yii;
use yii\base\Application;
use yii\helpers\VarDumper;

/**
 * Module to put site into a maintenance mode.
 * 
 * You have to put the name of this module into a \yii\base\Application::bootstrap
 * (in the main config file - web.php) 
 * 
 * For all pages (without exeptions) will be shown message managed by
 * 'message_maintenance_mode' variable (look index.php view associated with DefaultController::actionIndex()).
 * 
 * This page is cached, so for anonymous users we will have no database queries at all.
 * TagDependency with a tag name: 'maintenance_mode_message'
 * 
 * @property boolean $enabled Whether the maintenance mode is enabled for current user
 * @property boolean $globalenabled Whether the site is in the maintenance mode (file flag exists)
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Module extends \yii\base\Module
{
    /**
     * Status code for answer 200
     */
    const STATUS_CODE_OK = 200;
    
    /**
     * @var string Controller namespace for this module
     */
    public $controllerNamespace = 'mgrechanik\cmscore\maintenance\controllers';
    
    /**
     * @var string Aliased path to a dir where flag file of maintenance mode is kept
     */
    public $flagPath = '@runtime/maintenance';
    
    /**
     * @var string The name of the flag file 
     * If file with this name exists at [[flagPath]] then
     * there is a maintenance mode turned on
     */
    public $flagFilename = 'flag.txt';
    
    /**
     * @var string  The address to the page where we are managing texts for maintenance messages
     * Like '/admin/admin/page/settings-edit?id=12#message_maintenance_mode'
     * @see \mgrechanik\cmscore\maintenance\controllers\AdminController::actionWork()
     */
    public $settingPageAddress = '';

    /**
     * @var string Real path to a [[flagPath]] dir
     */
    protected $innerFlagPath;
    
    /**
     * @var array list of "outter" urls  which are accessible for guests during maintenance mode
     * Like the ones from debug module. Without leading slash.
     * Use not this but MaintenanceAllowedurlsAdjustBehavior in most cases
     * @see innerAllowedUrlsFilename
     */
    public $allowedUrls = [];
    
    /**
     * Status code the pages with maintenance mode will return (503 - 'Service Unavailable')
     * 
     * @var integer 
     */
    public $statusCode = 503;

    /**
     * @var string Filename from [[flagPath]] where we keep addresses
     * which are accessible during Maintenance mode
     * 
     * The format of that array is [pid => url, ...] where url is without leading slash.
     * Example: [71 => 'en/login']
     * 
     * @see \mgrechanik\cmscore\maintenance\behaviors\MaintenanceAllowedurlsAdjustBehavior
     * @see \mgrechanik\cmscore\models\ProgramPage::behaviors()
     */
    protected $innerAllowedUrlsFilename = 'allowedurls.php';
    
    /**
     * @var boolean Whether the site is under Maintenance mode
     * for curent request (for admin(false) or not admin(true))
     */
    protected $enabled = false;
    
    /**
     * @var boolean Whether the site is under Maintenance mode
     * (when flag file exists)
     */    
    protected $globalenabled = false;
    
    /**
     * Implementation of property
     * 
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    
    /**
     * Implementation of property
     * 
     * @return boolean
     */
    public function getGlobalenabled()
    {
        return $this->globalenabled;
    } 
    
    /**
     * Full real path to the flag file
     * 
     * @return string
     */
    public function getFlagFilePath()
    {
        return $this->innerFlagPath . '/' . $this->flagFilename;
    }
    
    /**
     * Full real path to the [[innerAllowedUrlsFilename]]
     * 
     * @return string
     */
    public function getAllowedUrlsFilePath()
    {
        return $this->innerFlagPath . '/' . $this->innerAllowedUrlsFilename;
    }    

    /**
     * @inheritdoc
     */    
    public function init()
    {
        parent::init();
        $this->innerFlagPath = $pathname = Yii::getAlias($this->flagPath);
        if (!is_dir($pathname)) {
            mkdir($pathname);
        }
        
        if ($flag = file_exists($this->getFlagFilePath())) {
            $this->globalenabled = true;
            $user = Yii::$app->user;
            $this->enabled = true;
            if (!$user->isGuest && $user->can('core_see_basic_admin_list_pages')) {
                // The current user has admin access so he skips maintenance mode
                $this->enabled = false;
                Yii::$app->on(Application::EVENT_BEFORE_ACTION, [$this, 'adjustMessage']);
                return;
            } 
            $currentUrl = Yii::$app->request->getPathInfo();
            if (in_array($currentUrl, array_merge($this->allowedUrls, $this->getInnerAllowedUrls()))) {
                // No real maintenance mode for permitted urls
                return;
            }
            if (Yii::$app->urlManager->crossDomainAdmin == 'back') {
                // No real maintenance mode for admin pages when they are separated
                return;
            }
            
            // Real Maintenance mode.
            // determining the language
            Yii::$app->urlManager->language->parseRequest(Yii::$app->request);
            Yii::$app->catchAll = ['maintenance/default/index'];
            if(Yii::$app->getRequest()->isAjax) {
                Yii::$app->getResponse()->setStatusCode(self::STATUS_CODE_OK);
            } else {
                Yii::$app->getResponse()->setStatusCode($this->statusCode);
            }            
        }
    }
    
    /**
     * Event handler
     * 
     * It shows help info to admin user about web site being in the maintenance mode.
     */
    public function adjustMessage()
    {
        $online = '';
        if (Yii::$app->user->can('maintenance_change_mode')) {
            $online = ' ' . \yii\helpers\Html::a(Yii::t('cmscore', 'Make it online'), ['/maintenance/admin/work', 'cross' => 'back']);
        }
        Yii::$app->session->setFlash('form-successful-message-maintenance', 
            Yii::t('cmscore', 'Web site is under maintenance mode.') . $online
        );        
    }  
    
    /**
     * Saves into file [[innerAllowedUrlsFilename]]  new info about allowed urls
     * 
     * The key of the array is the page number ([pid=>url]).
     * 
     * @api
     * @param array $newarr Array of new pairs of allowed urls
     * @see \mgrechanik\cmscore\maintenance\behaviors\MaintenanceAllowedurlsAdjustBehavior
     */
    public function adjustInnerAllowedUrls($newarr)
    {
        // Invalidating cache
        \yii\caching\TagDependency::invalidate(\Yii::$app->cache, 'maintenance_mode_message');
        $file = $this->getAllowedUrlsFilePath();
        $allowed = $this->getInnerAllowedUrls();
        foreach ($newarr as $pid => $url) {
            $allowed[$pid] = $url;
        }
        file_put_contents($file, "<?php\nreturn " . VarDumper::export($allowed) . ";\n", LOCK_EX);
    }
    
    /**
     * Getting the array of allowed urls from [[innerAllowedUrlsFilename]]
     * 
     * From file it will return the next information - [pid=>url, ...]
     * @return string[]
     */
    protected function getInnerAllowedUrls()
    {
        $file = $this->getAllowedUrlsFilePath();
        if (file_exists($file)) {
            return require($file);
        }
        return [];
    }
}
