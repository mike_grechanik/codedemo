<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\maintenance\behaviors;

use mgrechanik\cmscore\models\Pagetracker;
use yii\db\ActiveRecord;
use mgrechanik\cmscore\behaviors\FieldsBehavior;
use mgrechanik\cmscore\helpers\Common;
use Yii;

/**
 * Maintenance module uses this field behavior to collect allowed urls.
 * 
 * It connects to [[ProgramPage]] (see lower) and reacts to changing
 * of outter urls for certain inner urls.
 * 
 * It implements a method for extracting freshest urls from system.
 * 
 * @see \mgrechanik\cmscore\models\ProgramPage
 * @see \mgrechanik\cmscore\maintenance\Module::adjustInnerAllowedUrls()
 * @see \mgrechanik\cmscore\components\url\AliasUrlRule::$flagGetFreshData
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class MaintenanceAllowedurlsAdjustBehavior extends FieldsBehavior
{
    /**
     * @var string[] Allowed inner urls (example ['users/main/login'])
     */
    public $innerUrls = [];
    
    /**
     * @inheritdoc
     */     
    public function events()
    {
        return [
            self::EVENT_AFTER_SAVE_FIELDS => 'handlerAfterUpdate',
        ];
    } 
    
    /**
     * Event handler
     */     
    public function handlerAfterUpdate()
    {
        $ow = $this->owner;
        if (Yii::$app->hasModule('maintenance')
                && (!empty($this->innerUrls))
                && (in_array($ow->page->source, $this->innerUrls))
                ) {
            
            // looking for a fresh alias
            if ($rule = Yii::$app->urlManager->getAliasRule()) {
                $mode = $rule->mode;
                $rule->mode = 0;
                $rule->flagGetFreshData = true;
            }
            $url = Common::urlTo($ow->page->source, ['lang' => $ow->lang]);
            // Adjusting
            Yii::$app->getModule('maintenance')->adjustInnerAllowedUrls([
                $ow->pageid => ltrim($url, '/')
            ]);
            // End adjusting
            if ($rule) {
                $rule->mode = $mode;
                $rule->flagGetFreshData = false;
            }
            // end looking for a fresh alias
        }
    }
}

