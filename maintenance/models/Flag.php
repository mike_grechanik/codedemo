<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\maintenance\models;

use Yii;

/**
 * Model to represet a status of current maintenance mode.
 * 
 * It also manages existence of the 'flag' file.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Flag extends \yii\base\Model 
{
    /**
     * @var integer Whether we are in the m.mode(1) or not(0) 
     */
    public $val;
    
    /**
     * @var string Real file path to a 'flag' file
     * @see \mgrechanik\cmscore\maintenance\Module::getFlagFilePath()
     */
    protected $file;

    /**
     * @inheritdoc
     */ 
    public function rules()
    {
        return [
            ['val', 'in', 'range' => [0,1]]
        ];
    }
    
    /**
     * @inheritdoc
     */     
    public function attributeLabels()
    {
        return [
            'val' => Yii::t('cmscore', 'Whether web site is in the maintenance mode'),
        ];
    }
    
    /**
     * @inheritdoc
     */     
    public function init()
    {
        parent::init();
        if ($this->file) {
            $this->val = file_exists($this->file);
        }
    }
    
    /**
     * Creating or deleting the 'flag' file depending on [[val]]
     * 
     * @api
     */
    public function save()
    {
        if ($this->file) {
            if (($this->val == 1) && !file_exists($this->file)) {
                file_put_contents($this->file, '1');
            } else {
                if (file_exists($this->file)) {
                    unlink($this->file);
                }
            }
        }        
    }
    
    /**
     * Setter for a [[file]]
     * 
     * @param string $val
     */
    public function setFile($val)
    {
        $this->file = $val;
    }
}

