<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\maintenance\controllers;

use Yii;
use mgrechanik\cmscore\base\Controller;
use yii\base\InvalidRouteException;

/**
 * This is the controller which cathes all requests during Maintenance mode on.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */      
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\PageCache',
                'varyByRoute' => false,
                'enabled' => $this->module->enabled && Yii::$app->user->isGuest,
                'only' => ['index'],
                'duration' => 0,
                'variations' => [
                    \Yii::$app->language,
                ],
                'dependency' => [
                    'class' => 'yii\caching\TagDependency',
                    'tags' => 'maintenance_mode_message',
                ],

            ],
        ];
    }    

    /**
     * This action manages maintenance page
     * 
     * @return mixed
     * @throws InvalidRouteException
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isAjax) {
            return false;
        }        
        if (!($this->module->globalenabled && $this->module->enabled)) {
            throw new InvalidRouteException('Page not found');
        }
        $this->layout = '/maintenancemain';
        return $this->render('index');
    }
    
    /**
     * @inheritdoc
     */      
    public function getMetaData()
    {
        return [
            'index' => [
                'nopage' => true,
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */     
    public function adjustBeforeAction($event)
    {
        // we are doing this to eleminate addresses like 'maintenance/default/index'
        // for a langswitcher
        $pmanager = Yii::$app->get('pageManager');
        $pmanager->innerUrl = '/'; // it was '' but did not work
        $pmanager->langCode = Yii::$app->urlManager->getLanguageCode();
    }
}
