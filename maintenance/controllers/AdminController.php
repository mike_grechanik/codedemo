<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmscore\maintenance\controllers;

use Yii;
use mgrechanik\cmscore\base\AdminController as AdminC;
use mgrechanik\user\models\basic\PermissionInterface;
use yii\filters\AccessControl;
use mgrechanik\cmscore\maintenance\models\Flag;

/**
 * Admin controller of maintenance module
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class AdminController extends AdminC implements PermissionInterface
{
    /**
     * @inheritdoc
     */      
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['work'],
                        'roles' => ['maintenance_change_mode'],
                    ],
                ],
            ],
        ];
    }        

    /**
     * Admin page to changing Maintenance mode.
     * 
     * @return mixed
     */
    public function actionWork()
    {
        $this->initializeAdminPage('service/maintenance-work');
        $model = new Flag([
            'file' => $this->module->getFlagFilePath()
        ]);
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            if ($model->val) {
                $message = 'Maintenance mode has been turned on';
            } else {
                $message = 'Maintenance mode has been turned off';
            }
            Yii::$app->session->setFlash('form-successful-message', Yii::t('cmscore', $message));
            return $this->refresh();
        }
        return $this->render('work', ['model' => $model, 'settingpageaddress' => $this->module->settingPageAddress]);
    }
    
    /**
     * @inheritdoc
     */      
    public function getAdminPages()
    {
        return [
            'service/maintenance-work' => [
                'text' => 'Maintenance mode',
                'url' => ['/maintenance/admin/work'],
                'permissions' => ['maintenance_change_mode'],
            ],                        
        ];
    }

    /**
     * @inheritdoc
     */  
    public function getPermissions()
    {
        return [
            'maintenance_change_mode' => [
                'description' => 'Setting site to the maintenance mode',
                'category' => 'Service',
            ],
        ];        
    }
}
