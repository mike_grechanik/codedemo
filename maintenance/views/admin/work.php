<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

\yii\gii\GiiAsset::register($this);
$this->params['breadcrumbs'][] = $this->title = Yii::t('cmscore', 'Maintenance mode');
?>

<div class="default-view">
    <h1><?= $this->title ?></h1>
    
    <?php $form = ActiveForm::begin(); ?>

    <div class="maintetance-text-help">
        <?= Yii::t('cmscore', 'You can adjust the maintenance message texts')
            . ' ' .Html::a(Yii::t('cmscore', 'here'), $settingpageaddress) ?>
    </div>
    <?= $form->field($model, 'val')->dropDownList([
        0 => Yii::t('cmscore', 'No'),
        1 => Yii::t('cmscore', 'Yes'),
    ])->hint(Yii::t('admintips', 'By choosing "Yes" you will put this web site to a maintenance mode, so it will be not available for users without admin rights. Keep in mind that login page will still be available.<br>Choose "No" to put site to a normal mode')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('cmscore', 'Change mode'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
