<?php
use yii\helpers\Html;
$this->title = Yii::t('cmscore', 'Maintenance mode');
?>
<div class="maintenance-default-index">
    <h1><?= $this->title ?></h1>
    <p>
        <?php
        print Html::encode(\mgrechanik\cmscore\service\SettingsManager::getSettingValue('message_maintenance_mode', 'val'));
        ?>
    </p>
</div>
