<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\controllers\admin\menu;

use Yii;
use mgrechanik\menu\models\Menus;
use mgrechanik\menu\models\SearchMenus;
use mgrechanik\cmscore\base\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use mgrechanik\menu\models\Menuitem;
use mgrechanik\blocks\models\Block;
use mgrechanik\user\models\basic\PermissionInterface;
use yii\filters\AccessControl;

/**
 * Implements the CRUD actions for Menus model.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class TaskController extends AdminController implements PermissionInterface
{
    /**
     * @inheritdoc
     */       
    public function behaviors()
    {
        $req = Yii::$app->request;
        $id = (int)$req->get('id');        
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['core_see_basic_admin_list_pages'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['menu_manage_all_menus'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => ['menu_manage_menu_by_id_' . $id],
                    ],                    
                ],
            ],
        ];
    }    


    /**
     * Lists all Menus models.
     * 
     * @return mixed
     */
    public function actionIndex()
    {
        $this->initializeAdminPage('menus/all');
        $menus = Menus::find()->all();
        return $this->render('index', [
            'menus' => $menus,
        ]);
    }

    /**
     * Displays a single Menus model.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $this->initializeAdminPage('menus/all/{id}/view', ['menuid' => $id, 'menuname' => $model->mname]);
        $user = Yii::$app->user;
        $can = $user->can('menu_manage_all_menus') || $user->can('menu_manage_menu_by_id_' . (int) $id);
        if ($can && isset($_POST['tree'])) {
            Menuitem::saveSubTreeOrder(null, ['mid' => $id], $_POST['tree']);
            Block::updateAll(['cacheexpire' => 0], ['btype' => 'menublock', 'bname' => $id]);            
            Yii::$app->session->setFlash('form-successful-message', Yii::t('menu', 'The position of the menu items has been saved'));
            return $this->redirect(['view', 'id' => $id]);
        }        
        $tree = Menuitem::getArrayTree(null, ['mid' => $id]);
        return $this->render('view', [
            'model' => $model,
            'tree' => $tree,
            'can' => $can,
        ]);
    }

    /**
     * Getting the languages of the system and setting menu with default language if there is only one.
     * 
     * @param Menus $model
     * @return null|array
     */
    public static function adjustLangs($model)
    {
        $um = Yii::$app->get('urlManager');
        if ($um->isOneLanguage()) {
            $model->lang = $um->getLanguageCode();
            return null;
        } else {
            $model->scenario = 'withlang';
            $langs = $um->language->langCodes;
            $l = [];
            foreach ($langs as $k => $val) {
                $l[$k] = $val['label'];
            }
            $langs = $l;
            return $langs;
        }                
    }

    /**
     * Creates a new Menus model.
     * 
     * If creation is successful, the browser will be redirected to the 'view' page.
     * 
     * @return mixed
     */
    public function actionCreate()
    {
        $this->initializeAdminPage('menus/create');
        $model = new Menus;
        $langs = self::adjustLangs($model); 
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('form-successful-message', Yii::t('menu', 'The new menu has been created'));
            return $this->redirect(['view', 'id' => $model->mid]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'langs' => $langs,
            ]);
        }
    }

    /**
     * Updates an existing Menus model.
     * 
     * If update is successful, the browser will be redirected to the 'view' page.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $this->initializeAdminPage('menus/all/{id}/edit', ['menuid' => $id, 'menuname' => $model->mname]);
        $langs = self::adjustLangs($model); 
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->mid]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'langs' => $langs,
            ]);
        }
    }

    /**
     * Deletes an existing Menus model.
     * 
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->initializeAdminPage('menus/all/{id}/delete', ['menuid' => $id, 'menuname' => $model->mname]);
        // whether the node is not for deleting
        $permanent = false;
        if (in_array(intval($id), $this->module->permanentMenus)) {
            $permanent = true;
            Yii::$app->session->setFlash('form-successful-message', Yii::t('menu', 'You cannot delete this menu because it serves as the permanent one'));
        }        
        if (isset($_POST['Deletemenu'])  && !$permanent) {
            $model->delete();
            Yii::$app->session->setFlash('form-successful-message', Yii::t('menu', 'The Menu has been deleted'));
            return $this->redirect(['index']);
            
        }
        
        return $this->render('delete', ['model' => $model]);        

    }

    /**
     * Finds the Menus model based on its primary key value.
     * 
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @param integer $id
     * @return Menus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * @inheritdoc
     */        
    public function getPermissions()
    {
        return [
            'menu_manage_all_menus' => [
                'description' => 'Manage all menus',
            ],
            'menu_manage_menu_item_categories' => [
                'description' => 'Manage categories among menu items',
            ],            
            'menu_manage_menu_item_adminlinks' => [
                'description' => 'Manage links to admin pages among menu items',
            ],                        
            'category' => 'Menu',
        ];
    } 
    
    /**
     * @inheritdoc
     */        
    public function getAdminPages()
    {
        return [
            'menus' => [
                'text' => 'Menus',
                'isCategory' => 1,
            ],
            'menus/all' => [
                'text' => 'All menus',
                'url' => ['/menu/admin/menu/task/index'],
                'title' => 'All menus',
                'permissions' => ['core_see_basic_admin_list_pages'],
            ],
            'menus/create' => [
                'text' => 'Create',
                'title' => 'Create a new menu',
                'url' => ['/menu/admin/menu/task/create'],
                'permissions' => ['menu_manage_all_menus'],
            ],
            'menus/all/{id}' => [
                'text' => 'Menu "{menuname}"',
                'isCategory' => 1,
                'is_show_dynamic_link_callback' => 'mgrechanik\menu\helpers\MenuHelper::isMenuPage',
            ],            
            'menus/all/{id}/view' => [
                'text' => 'View',
                'title' => 'View menu "{menuname}"',
                'isTab' => 1,
                'url' => ['/menu/admin/menu/task/view', 'id' => '{menuid}'],
                'is_show_dynamic_link_callback' => 'mgrechanik\menu\helpers\MenuHelper::isMenuPage',
            ],                        
            'menus/all/{id}/edit' => [
                'text' => 'Edit menu name',
                'title' => 'Edit name of menu {menuname}',
                'url' => ['/menu/admin/menu/task/update', 'id' => '{menuid}'],
                'isTab' => 1,
                'is_show_dynamic_link_callback' => 'mgrechanik\menu\helpers\MenuHelper::isMenuPage',
                'permissions' => ['menu_manage_all_menus'],
                'permission_callback' => 'mgrechanik\menu\helpers\MenuHelper::isCanEditThisMenu',
            ],            
            'menus/all/{id}/delete' => [
                'text' => 'Delete menu',
                'title' => 'Delete menu {menuname}',
                'isTab' => 1,
                'url' => ['/menu/admin/menu/task/delete', 'id' => '{menuid}'],
                'is_show_dynamic_link_callback' => 'mgrechanik\menu\helpers\MenuHelper::isMenuPage',
                'permissions' => ['menu_manage_all_menus'],
            ], 
            'menus/all/{id}/itsblock' => [
                'text' => 'Block with this menu',
                'title' => 'Block with this menu',
                'isTab' => 1,
                'is_show_dynamic_link_callback' => 'mgrechanik\menu\helpers\MenuHelper::isMenuPage',
                'url' => ['/blocks/admin/tasks/edit', 'id' => '?'],
                'dynamic_change_callback' => 'mgrechanik\menu\helpers\MenuHelper::setLinkToBlock',
                'permissions' => ['block_manage_all_blocks'],
            ],                        
            'blocks/all/{id}/menuviewpage' => [
                'text' => 'Menu of this block',
                'title' => 'Menu of this block',
                'isTab' => 1,
                'is_show_dynamic_link_callback' => 'mgrechanik\menu\helpers\MenuHelper::isBlockForMenuPage',
                'url' => ['/menu/admin/menu/task/view', 'id' => '{bname}'],
                'permissions' => ['core_see_basic_admin_list_pages'],
            ],                                    
        ];
    }    
}
