<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\controllers\admin\menuitem;

use Yii;
use mgrechanik\menu\models\Menuitem;
use mgrechanik\menu\models\SearchMenuitem;
use mgrechanik\cmscore\base\AdminController;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use mgrechanik\menu\models\Menus;
use mgrechanik\blocks\models\Block;
use yii\filters\AccessControl;

/**
 * Implements the CRUD actions for Menuitem model.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class TaskController extends AdminController
{
    /**
     * @inheritdoc
     */     
    public function behaviors()
    {
        $req = Yii::$app->request;
        $mid = (int)$req->get('mid');
        $id = (int)$req->get('id'); 
        if ($id) {
            if ($item = $this->findModel($id)) {
                $mid = $item->mid;
            }
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['menu_manage_all_menus'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['menu_manage_menu_by_id_' . $mid],
                    ],                    
                ],
            ],
        ];
    }        

    /**
     * Creates a new Menuitem model.
     * 
     * If creation is successful, the browser will be redirected to the 'view' page.
     * 
     * @param integer $mid The menu's mid
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate($mid)
    {
        if (!($menu = Menus::findOne($mid))) {
            throw new NotFoundHttpException('The requested menu does not exist.');
        }
        $this->initializeAdminPage('menus/all/{id}/itemcreate', ['menuitemcreate' => true, 'menuid' => $mid, 'menuname' => $menu->mname]);
        $model = new Menuitem;
        $model->mid = $mid;
        if (isset($_POST['Menuitem'])) {
            $model->load(Yii::$app->request->post());
            $parent = $model->weight;
            if (empty($parent)) {
                $parent = null;
            }
            $model->addToNode($parent, false);
            if ($model->save()) {
                Block::updateAll(['cacheexpire' => 0, 'cache' => null], ['btype' => 'menublock', 'bname' => $mid]);            
                Yii::$app->session->setFlash('form-successful-message', Yii::t('menu', 'The Menu item has been created'));
                return $this->redirect(['admin/menu/task/view', 'id' => $mid]);
            } 
        }
        return $this->render('create', [
            'model' => $model,
            'menu' => $menu,
        ]);
    }

    /**
     * Updates an existing Menuitem model.
     * 
     * If update is successful, the browser will be redirected to the 'view' page.
     * 
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = Yii::$app->user;
        if (  (((int) $model->iscategory) && (!$user->can('menu_manage_menu_item_categories'))) 
            ||
              (((int) $model->isdynamic) && (!$user->can('programmingwork')))  ) {
            throw new ForbiddenHttpException(Yii::t('menu', 'You are not allowed to manage this type of menu item'));
        }
        $menu = Menus::findOne($model->mid);
        $this->initializeAdminPage('menus/all/{id}/itemedit', ['menuitemid' => $id, 'menuid' => $model->mid, 'menuname' => $menu->mname]);
        // getting the position in the tree
        $oldweight = $model->weight;
        $parid = $model->getParentId();
        if (!$parid) {
            $parid = 0;
        }
        $model->weight = $parid;

        if (isset($_POST['Menuitem'])) {
            $model->load(Yii::$app->request->post());
            $parent = $model->weight;
            if ($parent == $parid) {
                // position has not been changed
                $model->weight = $oldweight;
            } else {
                // position has been changed
                if (empty($parent)) {
                    $parent = null;
                } 
                $model->moveToNode($parent, false);
            }
            if ($model->save()) {
                Block::updateAll(['cacheexpire' => 0, 'cache' => null], ['btype' => 'menublock', 'bname' => $model->mid]);            
                Yii::$app->session->setFlash('form-successful-message', Yii::t('menu', 'The Menu item has been updated'));
                return $this->redirect(['admin/menu/task/view', 'id' => $menu->mid]);
            } 
        }        

        return $this->render('update', [
            'model' => $model,
            'menu' => $menu,
        ]);
        
    }

    /**
     * Deletes an existing Menuitem model.
     * 
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $user = Yii::$app->user;
        if (  (((int) $model->iscategory) && (!$user->can('menu_manage_menu_item_categories'))) 
            ||
              (((int) $model->isdynamic) && (!$user->can('programmingwork')))  ) {
            throw new ForbiddenHttpException(Yii::t('menu', 'You are not allowed to manage this type of menu item'));
        }
        $menu = Menus::findOne($model->mid);
        $this->initializeAdminPage('menus/all/{id}/itemdelete', ['menuitemid' => $id, 'menuid' => $model->mid, 'menuname' => $menu->mname]);
        if (isset($_POST['Deletemenu'])) {
            $model->delete();
            Yii::$app->session->setFlash('form-successful-message', Yii::t('menu', 'The Menu item has been deleted'));
            Block::updateAll(['cacheexpire' => 0, 'cache' => null], ['btype' => 'menublock', 'bname' => $model->mid]);            
            return $this->redirect(['admin/menu/task/view', 'id' => $menu->mid]);
        }
        return $this->render('delete', ['model' => $model, 'menu' => $menu]);        
    }

    /**
     * Finds the Menuitem model based on its primary key value.
     * 
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @param integer $id
     * @return Menuitem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menuitem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * @inheritdoc
     */     
    public function getAdminPages()
    {
        return [
            'menus/all/{id}/editcontents' => [
                'text' => 'Editing contents',
                'isTab' => 1,
                'isCategory' => 1,
                'isActive' => 1,
                'is_show_dynamic_link_callback' => 'mgrechanik\menu\helpers\MenuHelper::isEditContentsPage',
                'permissions' => ['menu_manage_all_menus'],                
            ],                   
            'menus/all/{id}/itemcreate' => [
                'text' => 'Adding menu item',
                'isTab' => 1,
                'tabLevel' => 2,
                'is_show_dynamic_link_callback' => 'mgrechanik\menu\helpers\MenuHelper::isAddMenuItem',
                'permissions' => ['menu_manage_all_menus'],                
            ],       
            'menus/all/{id}/itemedit' => [
                'text' => 'Edit menu item',
                'title' => 'Edit menu item',
                'url' => ['/menu/admin/menuitem/task/update', 'id' => '{menuitemid}'],
                'isTab' => 1,
                'tabLevel' => 2,
                'is_show_dynamic_link_callback' => 'mgrechanik\menu\helpers\MenuHelper::isMenuItemPage',
                'permissions' => ['menu_manage_all_menus'],                
            ],            
            'menus/all/{id}/itemdelete' => [
                'text' => 'Delete menu item',
                'title' => 'Delete menu item',
                'url' => ['/menu/admin/menuitem/task/delete', 'id' => '{menuitemid}'],
                'isTab' => 1,
                'tabLevel' => 2,
                'is_show_dynamic_link_callback' => 'mgrechanik\menu\helpers\MenuHelper::isMenuItemPage',
                'permissions' => ['menu_manage_all_menus'],                                
            ],          
        ];
    }        
}
