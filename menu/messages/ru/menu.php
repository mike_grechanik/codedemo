<?php

return [
    'Do you really want to delete the menu' => 'Вы точно хотите удалить меню',
    'The Menu has been deleted' => 'Меню было удалено',
    'The new menu has been created' => 'Было создано новое меню',
    'The position of the menu items has been saved' => 'Позиции элементов меню сохранены',
    'Save positions of menu items' => 'Сохранить расположение пунктов меню',
    'Delete menu item' => 'Удаление пункта меню',
    'Create menu' => 'Создать меню',
    'Site menus' => 'Все меню сайта',
    'Site menu' => 'Меню сайта',
    'Mid' => 'Номер',
    'Mname' => 'Название',
    'Lang' => 'Язык',
    'Isadmin' => 'Админское меню?',
    'Is this admin menu?' => 'Это админское меню?',
    'Menu' => 'Меню',
    'root' => 'корень',
    'Link text' => 'Текст ссылки',
    'Link path' => 'Путь',
    'Enabled?' => 'Активен?',
    'Expanded?' => 'Развернутый?',
    'Is it admin link?' => 'Это ссылка на админскую страницу?',
    'Is it dynamic link?' => 'Это динамический пункт меню?',
    'Is it a category and not a link?' => 'Это категория а не ссылка?',
    'Position in the tree (choose a parent)' => 'Позиция в дереве (выберите родителя)',
    'External?' => 'Абсолютный путь у ссылки(с доменом)?',
    'Attribute "title" for this link' => 'Значение атрибута "title" ссылки',
    'Class for tag <a> (or for <span> if category)' => 'Значение атрибута "class" для тега <a> (или для <span> если категория)',
    'Target for tag <a>' => 'Значение атрибута "target" для тега <a>',
    'Rel for tag <a>' => 'Значение атрибута "rel" для тега <a>',
    'Attribute "style" for tag <a>' => 'Значение атрибута "style" для тега <a>',
    'Class for tag <li>' => 'Значение атрибута "class" для тега <li>',
    'Attribute "style" for tag <li>' => 'Значение атрибута "style" для тега <li>',
    'Create a new menu item' => 'Создать новый пункт меню',
    'Creating a menu item' => 'Создание нового пункта меню',
    'for next menu: {menuname}' => 'для следующего меню: {menuname}',
    'Updating a menu item' => 'Редактирование пункта меню',
    'The Menu item has been deleted' => 'Пункт меню удален',
    'The Menu item has been created' => 'Новый пункт меню создан',
    'The Menu item has been updated' => 'Пункт меню отредактирован',
    'Do you really want to delete the menu item' => 'Вы точно хотите удалить пункт меню',
    'You are not allowed to manage this type of menu item' => 'Вам не разрешено управлять данным типом пункта меню',
    'Menu item is not in a range' => 'Недопустимый пункт меню',
    'You cannot delete this menu because it serves as the permanent one' => 'Вы не можете удалить данное меню, потому что оно является одним из постоянных',
];

