<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\helpers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use mgrechanik\cmscore\helpers\Common;
use mgrechanik\menu\models\Menuitem;
use mgrechanik\menu\models\Menus;
use mgrechanik\cmscore\service\AdminPagesManager;
use mgrechanik\cmscore\models\traits\TreeNode;
use mgrechanik\blocks\models\Block;

/**
 * Helper to do all the service work for managing menuses
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class MenuHelper 
{
    /**
     * Preprocessing a tree before displaying.
     * 
     * Setting additional keys to ->value for nodes
     * 'active-trail'
     * 'active-trail-last'  - for real current item
     * 'first'
     * 'last'
     * 'url'
     * 
     * 
     * Look at example: \mgrechanik\menu\models\block\views\block.php
     * 
     * @param type $tree
     * @see \mgrechanik\cmscore\models\traits\MaterializedPath::getArrayTree() 
     */
    public static  function preprocessTree(&$tree)
    {
        $ob1 = null;
        $ob2 = null;
        foreach ($tree as $k => $node) {
            self::preprocessNode($node);
            if (! (int)$node->value['enabled']) {
                continue;
            }
            if (!$ob1) {
                $node->value['first'] = 1;
                $ob1 = $node;
            }
            $node->value['last'] = 1;
            if ($ob2) {
                unset($ob2->value['last']);
            } 
            $ob2 = $node;
            if (!$node->isLeaf()) {
                self::preprocessTree($node->children);
            }
        }
    }
    
    /**
     * Preprocessing the node of the tree.
     * 
     * @param TreeNode $node
     */
    protected static  function preprocessNode(TreeNode $node)
    {
        // current page url
        $curUrl = parse_url(ltrim(Yii::$app->request->getUrl(), '/'));
        $curUrl = (isset($curUrl['path'])) ? $curUrl['path'] : '';
        // menu item url
        $isactive = null;
        $url = false;
        $menuitem = $node->value;
        if ((int)$menuitem['iscategory']) {
            return;
        }
        if ((int)$menuitem['isdynamic']) {
            // callback is like the next : mgrechanik\menu\helpers\DynLinkExample::someLink1
            if (!is_callable($menuitem['lpath'])) {
                $res2 = false;
            } else {
                $res2 = call_user_func($menuitem['lpath']);
            }
            if ($res2) {
                if (isset($res2['permission'])) {
                    if (!Yii::$app->user->can($res2['permission'])) {
                        $res2 = false;
                    }
                }
            } 
            if ($res2) {
                if (isset($res2['text'])) {
                    $node->value['ltext'] = $res2['text'];
                }
                if (isset($res2['url'])) {
                    $url = $res2['url'];
                }                
            } else {
                $node->value['enabled'] = 0;
            }
        } else {
            if ((int)$menuitem['isadmin']) {
                $user = Yii::$app->user;
                $lang = Yii::$app->urlManager->getLanguageCode();
                if (!$user->isGuest) {
                    // is it optimal ?
                    $lang = $user->getIdentity()->adminlang;
                }
                $node->value['url'] = Common::urlTo($menuitem['lpath'], ['lang' => $lang, 'cross' => 'back']);
            } else {
                /*
                if (is_numeric($menuitem['lpath'])) {
                    if ($page = Yii::$app->get('pageManager')->page) {
                        if ($page->pid == $menuitem['lpath']) {
                            $isactive = true;
                        } else {
                            $isactive = false;
                        }
                    }
                    if ($url2 = Common::urlToPage(intval($menuitem['lpath']), [], intval($menuitem['external']))) {
                        $node->value['url'] = $url2;
                    }        
                } 
                */
                $matches = [];
                if (preg_match('/^(\d+)(#(.+))?$/', $menuitem['lpath'], $matches)) {
                    $menuitem['lpath'] = $matches[1];
                    $params = empty($matches[3]) ? [] : ['#' => $matches[3]];
                    if ($page = Yii::$app->get('pageManager')->page) {
                        if ($page->pid == $menuitem['lpath']) {
                            $isactive = true;
                        } else {
                            $isactive = false;
                        }
                    }
                    if ($url2 = Common::urlToPage(intval($menuitem['lpath']), $params, intval($menuitem['external']))) {
                        $node->value['url'] = $url2;
                    }        
                }                
                else {
                    $url = $menuitem['lpath'];
                    if (intval($menuitem['external'])) {
                        $node->value['url'] = Url::to($url, true);
                    }
                }
            }
        }

        if (!isset($node->value['url'])) {
            if ($url === false) {
                $node->value['url'] = '#';
                return;
            }            
            $node->value['url'] = $url;
        }
        
        $active = false;
        if ($isactive !== null) {
            $active = $isactive;
        } else {
            // checking url
            if ($url !== false) {
                $url = ltrim($url, '/');
                $active = ($url == $curUrl);                 
            }
        }
        if ($active) {
            $node->value['active-trail-last'] = 1;
            $node->setActiveTrail();
        }
    }    
    
    /**
     * Getting classes for <a> and <li> of the element of BasicTree
     * 
     * returns an array
     * [
     *   'liclass' =>'...',
     *   'aclass' =>'...'
     * ]
     * 
     * @param TreeNode $node
     * @return array
     */
    public static function getClassesFromNode(TreeNode $node)
    {
        $val = $node->value;
        $lis = [];
        $as = [];
        if (!empty($val['licssclass'])) {
            $lis[] = $val['licssclass'];
        }
        if (!empty($val['acssclass'])) {
            $as[] = $val['acssclass'];
        }        
        if ($val['iscategory']) {
            $as[] = 'category';
        }                
        if ($node->isLeaf()) {
            $lis[] = 'leaf';
        } else {
            if ((bool)$val['expanded']) {
                $lis[] = 'expanded';
            } else {
                $lis[] = 'collapsed';
            }
        }
        if (isset($val['first'])) {
            $lis[] = 'first';
        }
        if (isset($val['last'])) {
            $lis[] = 'last';
        } 
        if (isset($val['active-trail'])) {
            $lis[] = 'active-trail';
            $as[] = 'active-trail';
            if (isset($val['active-trail-last'])) {
                $as[] = 'active';
            }
        } 
        return [
            'liclass' => implode(' ', $lis),
            'aclass' => implode(' ', $as),
        ];
    }

    /**
     * Returns html for a link for menuitem
     * 
     * @param array $menuitem Associative array
     * @return string
     * @see [[SimpleTree]]
     */
    static public function getAtagFromItem($menuitem)
    {
        $res = [
           'url' => '#',
           'text' => '',
           'options' => [], 
        ];
        // finding url, text
        if ((int)$menuitem['isdynamic']) {
            // callback is like the next : mgrechanik\menu\helpers\DynLinkExample::someLink1
            $res2 = call_user_func($menuitem['lpath']);
            if ($res2) {
                if (isset($res2['permission'])) {
                    if (!Yii::$app->user->can($res2['permission'])) {
                        return null;
                    }
                    unset($res2['permission']);
                }
                $res = array_merge($res, $res2);
            } else {
                return null;
            }
        } elseif ((int)$menuitem['iscategory']) {
            $options = [];
            if (!empty($menuitem['acssclass'])) {
                $options['class'] = $menuitem['acssclass'];
            }                    
            return Html::tag('span', Html::encode($menuitem['ltext']), $options);
        }
        else {
            if ((int)$menuitem['isadmin']) {
                // 
                $user = Yii::$app->user;
                $lang = Yii::$app->urlManager->getLanguageCode();
                if (!$user->isGuest) {
                    $lang = $user->getIdentity()->adminlang;
                }
                $res['url'] = Common::urlTo($menuitem['lpath'], ['lang' => $lang, 'cross' => 'back']);
            } else {
                /*
                if (is_numeric($menuitem['lpath'])) {
                    if ($url = Common::urlToPage(intval($menuitem['lpath']), [], intval($menuitem['external']))) {
                        $res['url'] = $url;
                    }        
                } 
                */
                $matches = [];
                if (preg_match('/^(\d+)(#(.+))?$/', $menuitem['lpath'], $matches)) {
                    $params = empty($matches[3]) ? [] : ['#' => $matches[3]];
                    if ($url = Common::urlToPage(intval($matches[1]), $params, intval($menuitem['external']))) {
                        $res['url'] = $url;
                    }        
                }                 
                else {
                    $res['url'] = $menuitem['lpath'];
                    if (intval($menuitem['external'])) {
                        $res['url'] = Url::to($res['url'], true);
                    }
                }
            }
            $res['text'] = $menuitem['ltext'];
        }
        if (!empty($menuitem['ltitle'])) {
            $res['options']['title'] = $menuitem['ltitle'];
        }
        if (!empty($menuitem['atarget'])) {
            $res['options']['target'] = $menuitem['atarget'];
        }        
        if (!empty($menuitem['arel'])) {
            $res['options']['rel'] = $menuitem['arel'];
        }        
        if (!empty($menuitem['acssclass'])) {
            $res['options']['class'] = $menuitem['acssclass'];
        }        
        if (!empty($menuitem['astyle'])) {
            $res['options']['style'] = $menuitem['astyle'];
        }  
        return Html::a(Html::encode($res['text']), $res['url'], $res['options']);
    }
    
    /**
     * Helper to create the list of items for the menu choise model.
     * 
     * @api
     * @param array $mids Allowed menus ids
     * @param null|integer $menuexcept The bid of menuitem we do not want to display in the list.
     * When editing menu item we do not want the list to have it or any of it's children. 
     * Otherwise a dangerous recursion might occure.
     * Look at \mgrechanik\menu\views\admin\menuitem\task\update.php (for menuexept )
     * @param integer $indent The indent among items of adjacent levels
     * @param string  $delimiter Waht we use as a delimiter.
     * @return array The list of ids for menu position choise
     * @see \mgrechanik\menu\models\fields\ContentMenuModel
     */
    static public function getSelectTrees($mids, $menuexcept = null, $indent = 2, $delimiter = '--')
    {
        $res = [0 => Yii::t('cmscore', 'not in menu')];
        foreach ($mids as $mid) {
            $tree = self::getSelectTree($mid, $menuexcept, $indent, $delimiter, false);
            if ($tree) {
                foreach ($tree as $k=>$v) {
                    $res[$k] = $v;
                }
            }
        }
        return $res;
    }

    /**
     * Helper to create the list of items for one menu choise.
     * 
     * @api
     * @param integer $mid
     * @param null|integer $menuexcept
     * @param integer $indent
     * @param string $delimiter
     * @param boolean $isrootkeyzero Whether it is a list of menus(false) or a list with one menu items(true).
     * The former is to create list of menus at the node manage page.
     * The latter is to create the list of items of one menu for a menu item manage process.
     * Look at \mgrechanik\menu\views\admin\menuitem\task\_form.php (for $isrootkeyzero)
     * @return array The list of ids for menu position choise
     */
    static public function getSelectTree($mid, $menuexcept = null, $indent = 2, $delimiter = '--', $isrootkeyzero = true)
    {
        $tree = Menuitem::getArrayTree(null, ['mid' => $mid]);
        if ($isrootkeyzero) {
            $res = [0 => Yii::t('cmscore', 'root')];
        } else {
            if ($menu = Menus::findOne($mid)) {
                $res = [-$mid => $menu->mname];
            }
        }
        self::moveTree($tree, $res, $indent, $delimiter, $menuexcept);
        return $res;
    }
    
    /**
     * Returns a list of menu items with only menu item of category type to choose from.
     * 
     * @api
     * @param integer $mid
     * @param null|integer $menuexcept
     * @param integer $indent
     * @param string $delimiter
     * @return array The list of ids for menu position choise
     */
    static public function getSelectCategoryTree($mid, $menuexcept = null, $indent = 2, $delimiter = '--')
    {
        $tree = Menuitem::getArrayTree(null, ['mid' => $mid]);
        $res = [];
        self::moveTree($tree, $res, $indent, $delimiter, $menuexcept, true);
        return $res;
    }    
    
    /**
     * Decorates the list of menuitems.
     * 
     * @param array $children
     * @param array $res The array with result
     * @param integer $indent
     * @param string $delimiter
     * @param null|integer $menuexcept Memu item id which not to show
     * @param boolean $categoryOnly Whether to show the menu items which are only of category type.
     */
    static protected function moveTree($children, &$res, $indent, $delimiter, $menuexcept = null, $categoryOnly = false)
    {
        foreach ($children as $node) {
            if ($menuexcept && ($node->value['id'] == $menuexcept)) {
                continue;
            } else {
                if ($skip = ($categoryOnly) && (!$node->value['iscategory'])) {
                    continue;
                }
                $res[$node->value['id']] = str_repeat($delimiter, $indent * $node->value['level']) .  $node->value['ltext'];
                if (!empty($node->children)) {
                    self::moveTree($node->children, $res, $indent, $delimiter, $menuexcept, $categoryOnly);
                }
            }
        }
    }
    
    /**
     * Whether we are at the page of managing menu.
     * 
     * Implementation of 'is_show_dynamic_link_callback' callback for
     * AdminPagesInterface.
     * 
     * @return boolean
     */
    public static function isMenuPage()
    {
        if (isset(AdminPagesManager::$tokens['menuid'])) {
            return true;
        } else {
            return false;
        }
    }  
    
    /**
     * Whether we are at the page of creating new menu item.
     * 
     * Implementation of 'is_show_dynamic_link_callback' callback for
     * AdminPagesInterface.
     * 
     * @return boolean
     */
    public static function isAddMenuItem()
    {
        if (isset(AdminPagesManager::$tokens['menuitemcreate'])) {
            return true;
        } else {
            return false;
        }
    }  
    
    /**
     * Whether we are at the page of creating new menu item.
     * 
     * Implementation of 'is_show_dynamic_link_callback' callback for
     * AdminPagesInterface.
     * 
     * @return boolean
     */
    public static function isMenuItemPage()
    {
        if (isset(AdminPagesManager::$tokens['menuitemid'])) {
            return true;
        } else {
            return false;
        }
    }   
    
    /**
     * Whether we are at one of the menu contents editing pages.
     * 
     * Implementation of 'is_show_dynamic_link_callback' callback for
     * AdminPagesInterface.
     * 
     * @return boolean
     */
    public static function isEditContentsPage()
    {
        return self::isAddMenuItem() || self::isMenuItemPage();
    }     
    
    /**
     * Page of the managing a block and the block is the menu type one.
     * 
     * Implementation of 'is_show_dynamic_link_callback' callback for
     * AdminPagesInterface.
     * 
     * @return boolean
     */
    public static function isBlockForMenuPage()
    {
        if (isset(AdminPagesManager::$objects['blockobj'])) {
            $obj = AdminPagesManager::$objects['blockobj'];
            if ($obj->btype == 'menublock') {
                return true;
            }
        } 
        return false;
    }      
    
    /**
     * Whether the user has the rights to edit current menu at menu manage process pages.
     * 
     * menu_manage_all_menus has already been checked
     * 
     * Implementation of 'permission_callback' callback for
     * AdminPagesInterface.
     * 
     * @return boolean
     */
    public static function isCanEditThisMenu()
    {
        if (isset(AdminPagesManager::$tokens['menuid'])) {
            $user = Yii::$app->user;
            $key = 'menu_manage_menu_by_id_' . (int) AdminPagesManager::$tokens['menuid'];
            return ($user->can($key)) ? true : false;                                                 
        } else {
            return false;
        }
    }    
    
    /**
     * Creating a link to the block of menu at the menu manage pages.
     * 
     * Implementation of 'dynamic_change_callback' callback for
     * AdminPagesInterface.
     * 
     * @param array $node
     * @return array
     */
    public static function setLinkToBlock($node)
    {
        if (isset(AdminPagesManager::$tokens['menuid'])) {
            //var_dump(AdminPagesManager::$tokens['menuid']);
            $mid = AdminPagesManager::$tokens['menuid'];
            if ($block = Block::find()->where(['iscopy' => 0, 'btype' => 'menublock', 'bname' => $mid])->one()) {
                $node['url']['id'] = $block->bid;
            }
        }
        return $node;
    }    
    
}

