<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\helpers;

/**
 * Class with the example of dнnamic menuitem callback
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class DynLinkExample
{
    /**
     * Returns associative array with information to build a link
     * 
     * @return array
     */
    public static function someLink1()
    {
        return [
            'text' => 'time:' . date('H:i:s'),
            'url' => '/time/' . time(),
            'permission' => 'menu_manage_menu_item_categories',
        ];
    }
}

