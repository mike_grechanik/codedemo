<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\behaviors;

use mgrechanik\menu\models\fields\WhichMenuModel;
use yii\db\ActiveRecord;
use mgrechanik\cmscore\behaviors\FieldsBehavior;
use mgrechanik\cmscore\models\ContentSettings;

/**
 * This is the field content type behavior. 
 * 
 * It represents the choise of the menus to which we can add links to the nodes
 * at the node managing workflow.
 * 
 * @see \mgrechanik\ctypes\page\models\PageSettings
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class WhichMenuBehavior extends FieldsBehavior
{
    /**
     * @inheritdoc
     */       
    public $template = '@mgrechanik/menu/models/fields/fieldviews/whichmenuedit.php';

    /**
     * @var ContentSettings The model to save our settings
     */     
    protected $saveModel;

    /**
     * @inheritdoc
     */     
    public function initModel()
    {
        $mainModel = $this->owner;
        $conf0 = [
            'pagetype' => $mainModel->contentType,
            'nid' => $mainModel->nid,
            'what' => 'whichmenu',
        ];
        $this->saveModel = ContentSettings::findOne($conf0);
        $conf = [];
        if ($this->saveModel) {
            if ($this->saveModel->configarray) {
                $conf = $this->saveModel->configarray; 
            }
        } else {
           $this->saveModel = new ContentSettings($conf0);
        }
        $this->model = new WhichMenuModel($conf);
    }  
    
    /**
     * @inheritdoc
     */     
    public function saveModel()
    {
        if ($this->model) {  
            $this->saveModel->configarray = ['menus' => $this->model->menus];
            $this->saveModel->save(false);
        }
    }            

    /**
     * Returns an array of allowed id of menus.
     * 
     * Result resides in the values of this array
     *   $ps = new \mgrechanik\ctypes\page\models\PageSettings();
     *   $ps->initFieldsModel();
     *   var_dump($ps->getAllowedMenuIds());
     *   array (size=2)
     *     0 => string '1' (length=1)
     *     1 => string '3' (length=1)
     * 
     * @api
     * @return array Ids of the menus choosen by this behavior
     * @see \mgrechanik\menu\behaviors\ContentMenuFieldBehavior::getAllowedMenuIds()
     */
    public function getAllowedMenuIds()
    {
        if (!$this->saveModel->isNewRecord) {
            $conf = $this->saveModel->configarray;
            if (!empty($conf['menus'])) {
                return $conf['menus'];
            }
        }
        return [];
    }
    
}

