<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\behaviors;

use mgrechanik\cmscore\behaviors\FieldsBehavior;
use mgrechanik\menu\models\fields\ContentMenuModel;
use mgrechanik\menu\models\Menuitem;
use mgrechanik\menu\models\ContentMenu;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the node level field behavior of choosing existence of the menu link to this node
 *  in one of the menus of the system.
 * 
 * @see \mgrechanik\ctypes\page\models\Pagenode
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class ContentMenuFieldBehavior extends FieldsBehavior
{
    /**
     * @inheritdoc
     */          
    public $template = '@mgrechanik/menu/models/fields/fieldviews/contentmenuedit.php';

    /**
     * @var ContentMenu The table to keep "node-menu" connection
     */       
    protected $connectmodel;

    /**
     * @inheritdoc
     */        
    public function initModel()
    {
        $mainModel = $this->owner;
        if (!$mainModel->isNewRecord) {
            if ($cm = ContentMenu::findOne([
                'pagetype' => $mainModel->contentType,
                'nid' => $mainModel->nid,
            ])) {
               $this->connectmodel = $cm; 
            }
        }  
        if ($this->connectmodel === null) {
            $this->connectmodel = new Contentmenu();
            $this->adjustModels();            
        }
        $name = '';
        $parid = 0;
        if ($itemid = $this->connectmodel->itemid) {
            if ($menuItem = Menuitem::findOne($itemid)) {
                $name = $menuItem->ltext;
                $parid = (int) $menuItem->getParentId(); 
                if (!$parid) {
                    $parid = - $menuItem->mid;
                }
            }
            
        }
        $this->model =  new ContentMenuModel([
            'itemid' => $itemid,
            'parentid' => $parid,
            'name' => $name,
            'menus' => $this->getAllowedMenuIds(),
        ]);
        
    } 
    
    /**
     * @inheritdoc
     */
    public function adjustModels()
    {
        $mainModel = $this->owner;
        $this->connectmodel->pagetype = $mainModel->contentType;
        $this->connectmodel->nid = $mainModel->nid;
    }

    /**
     * @inheritdoc
     */      
    public function saveModel()
    {
        if ($this->model) {  
            // defining old state
            $connectModel = $this->connectmodel;
            $oldItemId = (int) $connectModel->itemid;
            $oldTreeId = 0;
            $oldParentId = null;
            $oldItemObj = null;
            if ($oldItemId && ($oldItemObj = Menuitem::findOne($oldItemId))) {
                $oldTreeId = $oldItemObj->mid;
                $oldParentId = $oldItemObj->getParentId();
            }
            // defining a new state
            $newParentId = $this->model->parentid;
            if ($newParentId <= 0) {
                $newTreeId = abs($newParentId);
                $newParentId = null;
            } else {
                if ($parObj = Menuitem::findOne($newParentId)) {
                    $newTreeId = $parObj->mid;
                }
            }
            // defining the name
            $name = $this->model->name;
            if (!$name) {
                $name = ($oldItemObj) ? $oldItemObj->ltext : $this->owner->title;
            }
            // SAVING
            // 1) nothing has changed
            if (($oldTreeId == $newTreeId) && ($oldParentId == $newParentId)) {
                if ($oldItemObj) {
                    $oldItemObj->ltext = $name;
                    $oldItemObj->save(false);
                }
                return;
            }
            // 2) The tree has been changed
            if ($oldTreeId != $newTreeId) {
                // deleting from old tree
                if ($oldItemId) {
                   $oldItemObj->delete();
                }
                if ($newTreeId) {
                    $mitem = new Menuitem([
                        'mid' => $newTreeId,
                        'ltext' => $name,
                        'lpath' => $this->owner->pageid,
                    ]);
                    $mitem->addToNode($newParentId);
                    $connectModel->itemid = $mitem->id;
                    $connectModel->save(false);
                } else {
                    if ($oldItemId) {
                        $connectModel->delete(); 
                    }
                }
            } elseif ($oldItemId) {
                // moving in the same tree
                $oldItemObj->ltext = $name;
                $oldItemObj->moveToNode($newParentId);
            }
        }
    }            
    
    /**
     * @inheritdoc
     */        
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'handlerBeforeDelete',
        ];
    } 
    
    /**
     * Event handler.
     * 
     * @param \yii\base\Event $event
     */
    public function handlerBeforeDelete($event)
    {
        if (!$this->model) {
            $this->initModel();
        }
        if ($this->model) {
            $connectModel = $this->connectmodel;
            if (!$connectModel->isNewRecord) {
                $mitem = $connectModel->itemid;
                $connectModel->delete();
                if ($mobj = Menuitem::findOne($mitem)) {
                    $mobj->delete();
                }
            }
        }        
    }
    
    /**
     * Getting from content type of the current node the information
     * about ids of allowed menus.
     * 
     * @return array
     * @see \mgrechanik\menu\behaviors\WhichMenuBehavior::getAllowedMenuIds()
     */
    public function getAllowedMenuIds()
    {
        $id = $this->owner->contentType;
        $pmanager = Yii::$app->get('pageManager');
        $ctypes = $pmanager->getContentTypes(); 
        $ctype = $ctypes[$id];
        $class = $ctype['contentSettingsModelClass'];
        $model = new $class();
        $model->initFieldsModel();  
        return $model->getAllowedMenuIds();
    }
}

