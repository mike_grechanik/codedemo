<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu;

use Yii;

/**
 * Module to manage site menus
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Module extends \mgrechanik\cmscore\base\Module
{
    /**
     * @inheritdoc
     */        
    public $controllerNamespace = 'mgrechanik\menu\controllers';

    /**
     * @var integer[] The list of menu id's we do not want for admin to be able to delete.
     * For example when we do not want the Sitemap menu to be deleted
     * by admin because of mistake.
     */
    public $permanentMenus = [];
    
    /**
     * @inheritdoc
     */        
    public function init()
    {
        parent::init();

    }
    
    /**
     * @inheritdoc
     */       
    public function getMetaData()
    {
        return [
            'blockTypes' => [
                'menublock' => [
                    'name' => 'Menu Block',
                    'mainmodel' => [
                        'class' => 'mgrechanik\menu\models\Menus',
                    ],                    
                    'settingsmodel' => [
                        'class' => 'mgrechanik\menu\models\block\Settings',
                    ],
                    'isnotcustomcreate' => 1,
                ]                
            ],
           'translatesources' => [
               'menu' => [
                   'for' => 'menu',
                   'basePath' => '@mgrechanik/menu/messages',
               ],               
           ],             
        ];
    }       
}
