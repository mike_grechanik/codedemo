<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var mgrechanik\menu\models\Menus $model
 * @var yii\widgets\ActiveForm $form
 */

     \yii\gii\GiiAsset::register($this);
?>

<div class="menus-form default-view">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mname')->textInput(['maxlength' => 255])->hint(Yii::t('admintips', 'The name of the menu in the menu list')) ?>

    <?php 
    if (!empty($langs)) { 
        print $form->field($model, 'lang')->dropDownList($langs)->hint(Yii::t('admintips', 'The block with this menu could be shown only at pages of the site with choosen language'));
    } 
    ?>

    <?= $form->field($model, 'isadmin')->dropDownList([
        0 => Yii::t('cmscore', 'No'),
        1 => Yii::t('cmscore', 'Yes'),
    ])->hint(Yii::t('admintips', 'Choose this if you are creating menu which block will be shown only at admin pages')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('cmscore', 'Create') : Yii::t('cmscore', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
