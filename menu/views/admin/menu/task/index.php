<?php

use yii\helpers\Html;
use yii\grid\GridView;
use mgrechanik\cmscore\helpers\Common;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var mgrechanik\menu\models\SearchMenus $searchModel
 */

$this->title = Yii::t('menu', 'Site menus');
$this->params['breadcrumbs'][] = $this->title;

$islang = !Yii::$app->urlManager->isOneLanguage();
$langtitle = ($islang) ? '<th>' . Yii::t('cmscore', 'Language') . '</th>' : '';

?>
<div class="menus-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php  if (Yii::$app->user->can('menu_manage_all_menus')) { ?>
        <p>
            <?= Html::a(Yii::t('menu', 'Create menu'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>
    <div class="table-scroll">
        <table class="table table-striped table-bordered table-hover table-condensed">
            <tr><th><?= Yii::t('cmscore', 'Name') ?></th><?= $langtitle ?><th><?= Yii::t('menu', 'Is this admin menu?') ?></th><th><?= Yii::t('cmscore', 'Operations') ?></th></tr>
            <?php
              foreach ($menus as $key => $val) {
                  print 
                  '<tr>'
                  .  '<td>' . Html::encode($val->mname) . '</td>'
                  .  (($islang) ? '<td>' . $val->lang . '</td>' : '')
                  .  '<td>' . (($val->isadmin) ? Yii::t('cmscore', 'Yes') : Yii::t('cmscore', 'No')) . '</td>'
                  .  '<td class="cell-operations">' . Common::decorateLinks($val->getAdminLinks()) . '</td>'                  
                  . '</tr>';

              }
            ?>
        </table>
    </div>    
</div>
