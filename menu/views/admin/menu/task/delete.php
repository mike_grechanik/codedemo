<?php

use yii\helpers\Html;
$this->title = Yii::t('admintips', 'Deleting of') . ' ' . Yii::t('menu', 'Site menu') . ' : ' . $model->mname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('menu', 'Site menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= Html::encode($this->title) ?></h1>
<?= Html::beginForm(); ?>
<?php 
print Yii::t('menu', 'Do you really want to delete the menu') . ' "<b>' . Html::encode($model->mname) . '</b>"?' ;
?>
<br><br>
<?= Html::submitButton(Yii::t('cmscore', 'Delete'), ['name' => 'Deletemenu', 'class' => 'btn btn-primary']);?>
<?= Html::endForm(); ?>


