<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mgrechanik\cmscore\widgets\SimpleTreeEdit;
use mgrechanik\cmscore\assets\TreeEdit\TEAsset;

TEAsset::register($this);

/**
 * @var yii\web\View $this
 * @var mgrechanik\menu\models\Menus $model
 */

$this->title = Yii::t('menu', 'Site menu') . ' : ' . $model->mname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('menu', 'Site menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menus-view">

    <h1><?= Html::encode($this->title) ?></h1>
<?php 
$allow = [];
if (Yii::$app->user->can('menu_manage_all_menus')){
    $allow = ['u','d'];
} else if (Yii::$app->user->can('menu_manage_menu_by_id_' . $model->mid)) {
    $allow = ['u'];
}
?>
    <table class="table table-striped table-bordered table-hover table-condensed">
        <?php 
            if (!Yii::$app->urlManager->isOneLanguage()) {
                print '<tr><td>' . Yii::t('cmscore', 'Language') . '</td><td>' . $model->lang . '</td></tr>';
                print '<tr><td>' . Yii::t('menu', 'Is this admin menu?') . '</td><td>' . (($model->isadmin) ? Yii::t('cmscore', 'Yes') : Yii::t('cmscore', 'No')) . '</td></tr>';
            }
        ?>
    </table>
</div>
<?php if (in_array('u', $allow)){
    print Html::a(Yii::t('menu', 'Create a new menu item'), ['admin/menuitem/task/create', 'mid' => $model->mid], ['class' => 'btn btn-success']) . '<br><br>';
} ?>    
<div class="wrapper connectedSortable">
    <div class="treeDragDrop">
        <ul class="tdd-tree treeroot">
            <?php
            print SimpleTreeEdit::widget([
                'tree' => $tree,
                'canedit' => $can,
                'createLabelFunc' => function ($node, $field = '', $can) {
                    $id = (int) $node->value['id'];
                    //$res = $id . ' --- ' . Html::encode($node->value['ltext']);
                    $res = '&nbsp;' . Html::encode($node->value['ltext']);
                    if ($can) {
                        $res .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                            . Html::a(Yii::t('cmscore', 'Update'), ['admin/menuitem/task/update', 'id' => $id]) 
                            . '&nbsp;&nbsp;&nbsp;&nbsp;'
                            . Html::a(Yii::t('cmscore', 'Delete'), ['admin/menuitem/task/delete', 'id' => $id]) 
                            ;
                    }    
                    return $res;
            },
                ]);
            ?>
        </ul>
    </div>
</div>    
<?= Html::beginForm(); ?>
<?php if (in_array('u', $allow)){
    print '<br>' . Html::submitButton(Yii::t('menu', 'Save positions of menu items'), ['id' => 'beforesave', 'class' => 'btn btn-primary']);
} ?>
<?= Html::endForm(); ?>
