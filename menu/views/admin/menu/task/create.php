<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var mgrechanik\menu\models\Menus $model
 */

$this->title = Yii::t('menu', 'Create menu');
$this->params['breadcrumbs'][] = ['label' => Yii::t('menu', 'Site menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'langs' => $langs,
    ]) ?>

</div>
