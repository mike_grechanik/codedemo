<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var mgrechanik\menu\models\SearchMenuitem $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="menuitem-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'mid') ?>

    <?= $form->field($model, 'path') ?>

    <?= $form->field($model, 'level') ?>

    <?= $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'ltext') ?>

    <?php // echo $form->field($model, 'lpath') ?>

    <?php // echo $form->field($model, 'hidden') ?>

    <?php // echo $form->field($model, 'expanded') ?>

    <?php // echo $form->field($model, 'isadmin') ?>

    <?php // echo $form->field($model, 'isdynamic') ?>

    <?php // echo $form->field($model, 'ltitle') ?>

    <?php // echo $form->field($model, 'external') ?>

    <?php // echo $form->field($model, 'acssclass') ?>

    <?php // echo $form->field($model, 'atarget') ?>

    <?php // echo $form->field($model, 'arel') ?>

    <?php // echo $form->field($model, 'astyle') ?>

    <?php // echo $form->field($model, 'licssclass') ?>

    <?php // echo $form->field($model, 'listyle') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('menu', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('menu', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
