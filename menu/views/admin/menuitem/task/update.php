<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var mgrechanik\menu\models\Menuitem $model
 */

$this->title = Yii::t('menu', 'Updating a menu item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('menu', 'Site menus'), 'url' => ['admin/menu/task/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('menu', 'Site menu') . ' : ' . $menu->mname, 'url' => ['admin/menu/task/view', 'id' => $menu->mid]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="menuitem-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'menuexcept' => $model->id,
    ]) ?>

</div>
