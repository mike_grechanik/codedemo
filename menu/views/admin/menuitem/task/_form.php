<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mgrechanik\menu\helpers\MenuHelper;

/**
 * @var yii\web\View $this
 * @var mgrechanik\menu\models\Menuitem $model
 * @var yii\widgets\ActiveForm $form
 */

\yii\gii\GiiAsset::register($this);
$domain = \yii\helpers\Url::to(['/', 'cross' => ($model->isadmin) ? 'back' : 'front'], true);
$domain2 = rtrim($domain, '/');
$hintpath = Yii::t('admintips', 'The path can be one of the following:
<ul>    
    <li><code>some number</code> - it will mean page number</li>
    <li><code>some number#anchor</code> - it will mean the link to some anchor at the page with this number</li>
    <li><code>some absolute url</code> - it will have to start with <code>http</code></li>
    <li><code>url template</code> - You have to enter the url address of the page relevant to the root of the site (including a leading slash :<code>"{domain}<b style="color:blue;">/this-part-of-url</b>"</code>).</li>
</ul>  
<code>Example:</code>
<pre>
45
45#some-part-of-the-page
http::/some-site.com/page-about-me
/users/jack
/  
</pre>', ['domain' => $domain2]);

if ($model->hasErrors()) { ?>
<div class="alert alert-danger" role="alert">
    <?= Html::errorSummary($model) ?>
</div>    
<?php } ?>

<div class="menuitem-form default-view">

    <?php $form = ActiveForm::begin(); ?>
<div class="box box-bordered admin-block-1">
    <div class="box-title"><h3><i class="fa fa-th-list"></i><?= Yii::t('admintips', 'Basic') ?></h3></div>
    <div class="box-content nopadding"><div class="form-vertical form-bordered form-striped">
    <?= $form->field($model, 'ltext')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'lpath')->textInput(['maxlength' => 255])->hint($hintpath) ?>

    <?= $form->field($model, 'ltitle')->textInput(['maxlength' => 255]) ?>

    <?=
    $form->field($model, 'enabled')->dropDownList([
        1 => Yii::t('cmscore', 'Yes'),
        0 => Yii::t('cmscore', 'No'),
    ])->hint(Yii::t('admintips', 'Whether to show this menu item or not'))
    ?>

    <?=
    $form->field($model, 'weight')->dropDownList(
            MenuHelper::getSelectTree($model->mid, $menuexcept)
    )
    ?>

    <?=
    $form->field($model, 'expanded')->dropDownList([
        1 => Yii::t('cmscore', 'Yes'),
        0 => Yii::t('cmscore', 'No'),
    ])->hint(Yii::t('admintips', 'Whether to show this menu item\'s children'))
    ?>
    </div></div>
</div>  
<div class="box box-bordered admin-block-1">
    <div class="box-title"><h3><i class="fa fa-th-list"></i><?= Yii::t('admintips', 'Settings') ?></h3></div>
    <div class="box-content nopadding"><div class="form-vertical form-bordered form-striped">    
    <?php if ($model->isAttributeSafe('isadmin')) { ?>
        <?=
        $form->field($model, 'isadmin')->dropDownList([
            0 => Yii::t('cmscore', 'No'),
            1 => Yii::t('cmscore', 'Yes'),
        ])->hint(Yii::t('admintips', 'Whether this link leads into admin pages. If yes you do not have to enter any language prefixes into "Path" field'))
        ?>
    <?php } ?>      
    <?php if ($model->isAttributeSafe('isdynamic')) { ?>
        <?=
        $form->field($model, 'isdynamic')->dropDownList([
            0 => Yii::t('cmscore', 'No'),
            1 => Yii::t('cmscore', 'Yes'),
        ])
        ?>
    <?php } ?>  
    <?php if ($model->isAttributeSafe('iscategory')) { ?>    
        <?=
        $form->field($model, 'iscategory')->dropDownList([
            0 => Yii::t('cmscore', 'No'),
            1 => Yii::t('cmscore', 'Yes'),
        ])->hint(Yii::t('admintips', 'If this is not a link but a category you do not have to fill a "Path" field'))
        ?>    
    <?php } ?>  
    <?=
    $form->field($model, 'external')->dropDownList([
        0 => Yii::t('cmscore', 'No'),
        1 => Yii::t('cmscore', 'Yes'),
    ])->hint(Yii::t('admintips', 'Whether this link are shown with the absolute url (with domain)'))
    ?>
    </div></div>
</div> 
<div class="box box-bordered admin-block-1">
    <div class="box-title"><h3><i class="fa fa-th-list"></i><?= Yii::t('admintips', 'Presentation') ?></h3></div>
    <div class="box-content nopadding"><div class="form-vertical form-bordered form-striped">        
    <?= $form->field($model, 'acssclass')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'astyle')->textInput(['maxlength' => 255]) ?>

<?= $form->field($model, 'atarget')->dropDownList([
            '' => '',
            '_self' => '_self',
            '_blank' => '_blank',
            '_parent' => '_parent',
            '_top' => '_top',
        ]) ?>

<?= $form->field($model, 'arel')->textInput(['maxlength' => 20]) ?>

<?= $form->field($model, 'licssclass')->textInput(['maxlength' => 20]) ?>

<?= $form->field($model, 'listyle')->textInput(['maxlength' => 255]) ?>
    </div></div>
</div> 
    <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? Yii::t('cmscore', 'Create') : Yii::t('cmscore', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
