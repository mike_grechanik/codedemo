<?php

use yii\helpers\Html;
$this->title = Yii::t('menu', 'Delete menu item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('menu', 'Site menus'), 'url' => ['admin/menu/task/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('menu', 'Site menu') . ' : ' . $menu->mname, 'url' => ['admin/menu/task/view', 'id' => $menu->mid]];
$this->params['breadcrumbs'][] = $this->title;

?>
 <h1><?= Html::encode($this->title) ?></h1>
<?= Html::beginForm(); ?>
<?php 
print Yii::t('menu', 'Do you really want to delete the menu item') . ' "<b>' . Html::encode($model->ltext) . '</b>"?' ;
?>
<br><br>
<?= Html::submitButton(Yii::t('cmscore', 'Delete'), ['name' => 'Deletemenu', 'class' => 'btn btn-primary']);?>
<?= Html::endForm(); ?>


