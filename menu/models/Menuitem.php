<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\models;

use Yii;
use mgrechanik\cmscore\models\traits\MaterializedPath;
use mgrechanik\cmscore\models\MaterializedPathInterface;

/**
 * This is the model class for table "{{%menuitem}}".
 * 
 * It holds one position of the menu
 *
 * @property integer $id
 * @property integer $mid The id of the menu from [[Menus]]
 * @property string $path
 * @property integer $level
 * @property integer $weight
 * @property string $ltext
 * @property string $lpath
 * @property integer $enabled
 * @property integer $expanded
 * @property integer $isadmin
 * @property integer $isdynamic
 * @property integer $iscategory
 * @property string $ltitle
 * @property integer $external
 * @property string $acssclass
 * @property string $atarget
 * @property string $arel
 * @property string $astyle
 * @property string $licssclass
 * @property string $listyle
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Menuitem extends \yii\db\ActiveRecord implements MaterializedPathInterface
{
    /**
     * Mandatory to use together with MP functionality
     */
    use MaterializedPath;
    
    /**
     * @inheritdoc
     */    
    public function behaviors()
    {
        return [
            'materializedpath' => [
                'class' => 'mgrechanik\cmscore\behaviors\MaterializedPath',
            ],
        ];
    }  
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menuitem}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $res = [
            [['mid', 'ltext'], 'required'],
            [['lpath'], 'pathCheck', 'skipOnEmpty' => false],
            [['mid', 'enabled', 'expanded', 'external', 'weight'], 'integer'],
            [['enabled', 'expanded', 'external'], 'in', 'range' => [0, 1]],
            [['ltext', 'lpath', 'ltitle', 'astyle', 'listyle'], 'string', 'max' => 255],
            [['acssclass', 'atarget', 'arel', 'licssclass'], 'string', 'max' => 20]
        ];
        $user = Yii::$app->user;
        if ($user->can('programmingwork')) {
            $res[] = [['isdynamic'], 'integer'];
            $res[] = [['isdynamic'], 'in', 'range' => [0, 1]];
        }
        if ($user->can('menu_manage_menu_item_categories')) {
            $res[] = [['iscategory'], 'integer'];
            $res[] = [['iscategory'], 'in', 'range' => [0, 1]];
        }   
        if ($user->can('menu_manage_menu_item_adminlinks')) {
            $res[] = [['isadmin'], 'integer'];
            $res[] = [['isadmin'], 'in', 'range' => [0, 1]];
        }           
        return $res;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('menu', 'ID'),
            'mid' => Yii::t('menu', 'Mid'),
            'path' => Yii::t('menu', 'Path'),
            'level' => Yii::t('menu', 'Level'),
            'weight' => Yii::t('menu', 'Position in the tree (choose a parent)'),
            'ltext' => Yii::t('menu', 'Link text'),
            'lpath' => Yii::t('menu', 'Link path'),
            'enabled' => Yii::t('menu', 'Enabled?'),
            'expanded' => Yii::t('menu', 'Expanded?'),
            'isadmin' => Yii::t('menu', 'Is it admin link?'),
            'isdynamic' => Yii::t('menu', 'Is it dynamic link?'),
            'iscategory' => Yii::t('menu', 'Is it a category and not a link?'),
            'ltitle' => Yii::t('menu', 'Attribute "title" for this link'),
            'external' => Yii::t('menu', 'External?'),
            'acssclass' => Yii::t('menu', 'Class for tag <a> (or for <span> if category)'),
            'atarget' => Yii::t('menu', 'Target for tag <a>'),
            'arel' => Yii::t('menu', 'Rel for tag <a>'),
            'astyle' => Yii::t('menu', 'Attribute "style" for tag <a>'),
            'licssclass' => Yii::t('menu', 'Class for tag <li>'),
            'listyle' => Yii::t('menu', 'Attribute "style" for tag <li>'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function getTreeCondition()
    {
        return [
            'mid' => $this->mid,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getMpNames()
    {
        return [
            'id' => 'id',
            'path' => 'path',
            'weight' => 'weight',
            'level' => 'level',
            'name' => 'ltext',
        ];
    }
    
    /**
     * Inline validator
     * 
     * @param string $attribute
     * @param array $params
     */
	public function pathCheck($attribute, $params){
		if (($attribute == 'lpath')) {
            if ($this->iscategory) {
                return;
            }
            if ((trim($this->lpath) == '')) {
                $this->addError($attribute, Yii::t('yii', '{attribute} cannot be blank.', ['attribute' => Yii::t('menu', 'Link path')]));
                return;
            }
            $lpath = $this->lpath;
            /*
            if (is_numeric($lpath) || (strpos($lpath, 'http') === 0)) {
                return;
            }
            */
            if (preg_match('/^(\d+)(#(.+))?$/', $lpath)
                || (strpos($lpath, 'http') === 0)) {
                return;
            }            
            if (strpos($lpath, '/') !== 0) {
                $this->addError($attribute, Yii::t('admintips', 'The url address has to start with a leading slash - "/".'));
            }
            
		}
	}        
        
}
