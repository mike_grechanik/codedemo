<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\models\block;

use Yii;

/**
 * Settings model for menu blocks functionality
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Settings extends \mgrechanik\blocks\base\SettingsModel
{
    /**
     * @inheritdoc
     */      
    public $viewTemplate = '@mgrechanik/menu/models/block/views/block.php';

    /**
     * @inheritdoc
     */      
    protected $editTemplate = '@mgrechanik/blocks/base/view/settingsedit.php';
    
    /**
     * @inheritdoc
     */  
    public function attributeLabels()
    {
        return [
            'viewTemplate' => Yii::t('cmsblock', 'ViewTemplate'),
        ];
    }    
}
