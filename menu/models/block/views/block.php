<?php
use mgrechanik\menu\models\Menuitem;
use mgrechanik\menu\helpers\MenuHelper;
use mgrechanik\menu\widgets\BasicTree;
?>
<section class="block block-<?=$id  . $cssclass ?>">
  <?php if ($title) { ?>  
    <h3><?=$title?></h3>
  <?php } ?>  
    <div class="content">
        <?php 
            $tree = Menuitem::getArrayTree(null, ['mid' => $mainmodel->mid]);
            MenuHelper::preprocessTree($tree);
            print BasicTree::widget(['tree' => $tree]);
        ?>
    </div>
</section>
