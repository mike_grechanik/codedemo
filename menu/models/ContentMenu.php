<?php

namespace mgrechanik\menu\models;

use Yii;

/**
 * This is the model class for table "{{%contentmenu}}".
 * 
 * The connection between nodes and automatically created menu items
 *
 * @property integer $pagetype
 * @property integer $nid
 * @property integer $itemid
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class ContentMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contentmenu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pagetype', 'nid', 'itemid'], 'required'],
            [['pagetype', 'nid', 'itemid'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pagetype' => 'Pagetype',
            'nid' => 'Nid',
            'itemid' => 'Itemid',
        ];
    }
}
