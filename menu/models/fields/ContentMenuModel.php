<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\models\fields;

use Yii;
use mgrechanik\menu\models\Menus;
use mgrechanik\menu\helpers\MenuHelper;

/**
 * Model to represent a list of menus and their items.
 * 
 * This is the model for field.
 * 
 * @see \mgrechanik\menu\behaviors\ContentMenuFieldBehavior
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class ContentMenuModel extends \yii\base\Model 
{
    /**
     * @var array An array of allowed menu ids
     */
    public $menus = [];
    
    /**
     * @var integer the bid of the menu item 
     */
    public $itemid;
    
    /**
     * @var integer the bid of the parent menuitem or -mid if the parent is menu
     */
    public $parentid;

    /**
     * @var string The name in the menu 
     */
    public $name = '';
    
    /**
     * @var array An array of allowed menu item ids. The ones to choose from.
     */
    protected $allowedIds = [];
    
    /**
     * @inheritdoc
     */       
    public function init()
    {
        parent::init();
        $this->allowedIds = MenuHelper::getSelectTrees($this->menus, $this->itemid);;
    }

    /**
     * @inheritdoc
     */   
    public function rules()
    {
        return [
            ['parentid', 'menuItemExist'],
            ['name', 'string', 'max' => 255],
        ];
    }
    
    /**
     * @inheritdoc
     */   
    public function attributeLabels()
    {
        return [
            'parentid' => \Yii::t('cmscore', 'Choose the position in the menu'),
            'name' => \Yii::t('cmscore', 'Choose the label in the menu'),
        ];
    }    
    
    /**
     * Inline validator.
     * 
     * @param string $attribute
     * @param array $params
     */
    public function menuItemExist($attribute, $params)
    {
        if ($attribute == 'parentid') {
            if (!isset($this->allowedIds[$this->parentid])) {
                $this->addError($attribute, Yii::t('menu', 'Menu item is not in a range'));
            }
        }
    }
    
}
