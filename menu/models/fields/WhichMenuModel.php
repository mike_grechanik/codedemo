<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\models\fields;

use Yii;
use mgrechanik\menu\models\Menus;

/**
 * Model to represent a choise elements of checkboxes of menus available
 * 
 * This is the model for field.
 * 
 * @see \mgrechanik\menu\behaviors\WhichMenuBehavior
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class WhichMenuModel extends \yii\base\Model 
{
    /**
     * @var array This property represents the menus choosen 
     */
    public $menus;
    
    /**
     * @inheritdoc
     */      
    public function rules()
    {
        return [
            ['menus', 'menusExist'],
        ];
    }

    /**
     * @inheritdoc
     */      
    public function attributeLabels()
    {
        return [
            'menus' => \Yii::t('cmscore', 'Choose menus'),
        ];
    }    
    
    /**
     * Inline validator.
     * 
     * @param string $attribute
     * @param array $params
     */
    public function menusExist($attribute, $params)
    {
        if ($attribute == 'menus') {
            $menus = $this->menus;
            $right = self::getOptions();
            foreach ($menus as $key=>$val) {
                if (!isset($right[$val])) {
                    $this->addError($attribute, 'menus not in range');
                }
            }
        }
    }
    
    /**
     * Returns the associated array with all menus available to put links in.
     * 
     * This options is used to form the checkbox choise list.
     * 
     * @return array
     */
    public static function getOptions()
    {
        $sitemapid = null;
        $defs = Yii::$container->getDefinitions();
        if (isset($defs['mgrechanik\cmscore\behaviors\sitemap\SitemapNodeSettingsBehavior']['htmlSitemapMenuId'])) {
            $sitemapid = (int) $defs['mgrechanik\cmscore\behaviors\sitemap\SitemapNodeSettingsBehavior']['htmlSitemapMenuId'];
        }
        $res = [];
        $rows = Menus::find()->where(['isadmin' => 0])->asArray()->all();
        foreach ($rows as $val) {
            if ($sitemapid && ($sitemapid == $val['mid'])) {
                continue;
            }
            $res[$val['mid']] = $val['mname'];
        }
        return $res;
    }
}
