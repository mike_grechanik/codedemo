<?php
use mgrechanik\menu\models\fields\WhichMenuModel;
?>
<?= $form->field($model, 'menus')
        ->hint(Yii::t('admintips', 'Choose menus in which the pages of this content type can be added'))
        ->checkboxList(WhichMenuModel::getOptions())
         ?>

