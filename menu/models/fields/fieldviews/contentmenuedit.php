<?php
use mgrechanik\menu\helpers\MenuHelper;
?>
<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'parentid')->dropDownList(MenuHelper::getSelectTrees($model->menus, $model->itemid)) ?>

