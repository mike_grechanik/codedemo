<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\models;

use Yii;
use mgrechanik\blocks\base\BlockModelTrait;
use mgrechanik\blocks\base\MainModelInterface;
use mgrechanik\blocks\models\Block;
use mgrechanik\menu\models\Menuitem;
use mgrechanik\user\models\rbac\AuthItem;


/**
 * This is the model class for table "{{%menus}}".
 * 
 * This table holds menus of the system.
 *
 * @property integer $mid
 * @property string $mname
 * @property string $lang
 * @property integer $isadmin Whether this menu is for admin pages.
 * It corresponds wuth it's block isadmin property.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Menus extends \yii\db\ActiveRecord implements MainModelInterface
{
    /**
     * Mandatory for a block main model
     */
    use BlockModelTrait;
    
    /**
     * @var string The path to empty template because we do not edit menu model at block managing page
     * Mandatory for a block main model
     */
    protected $editTemplate = '@mgrechanik/menu/models/block/views/mainmodeledit.php';    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menus}}';
    }
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $um = Yii::$app->get('urlManager');
        $langRange = array_keys($um->language->langCodes);
        return [
            [['mname'], 'required'],
            [['lang', ], 'required', 'on' => 'withlang'],
            [['lang', ], 'in', 'range' => $langRange , 'on' => 'withlang'],
            [['isadmin'], 'in', 'range' => [0,1]],
            [['mname'], 'string', 'max' => 255],
            [['lang'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mid' => Yii::t('menu', 'Mid'),
            'mname' => Yii::t('menu', 'Mname'),
            'lang' => Yii::t('menu', 'Lang'),
            'isadmin' => Yii::t('menu', 'Isadmin'),
        ];
    }
    
    /**
     * @inheritdoc
     */    
    public function getBName()
    {
        return $this->mid;
    }   
    
    /**
     * @inheritdoc
     */    
    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'handlerAfterUpdate']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'handlerAfterInsert']);
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'handlerAfterDelete']);
    }     

    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */
    public function handlerAfterUpdate($event)
    {
        Block::updateAll([
               'lang' => $this->lang,
               'isadmin' => $this->isadmin,
            ], [
               'btype' => 'menublock',
               'bname' => $this->mid,
            ]                
         );
        $auth = Yii::$app->authManager;
        $key = 'menu_manage_menu_by_id_' . $this->mid;
        if ($ai = AuthItem::findOne($key)) {
            $ai->valsfortranslate = serialize([
                'menuname' => $this->mname,
            ]);
            $ai->save(false);
        }                
    }    
    
    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */    
    public function handlerAfterInsert($event)
    {
        $meta = Yii::$app->getModule('blocks')->get('blockManager')->getMeta();
        $meta = $meta['menublock'];
        $settings = Yii::createObject($meta['settingsmodel']);
        $block = new Block([
            'btype' => 'menublock',
            'bname' => $this->mid,
            'iscopy' => 0,
            'lang' => $this->lang,
            'item' => $this->mid,
            'region' => 'none',
            'weight' => 1,
            'title' => $this->mname,
            'iscacheable' => 0,
            'admintitle' => Yii::t('menu', 'Menu') . ':' . $this->mname,
            'settings' => $settings,
            'isadmin' => $this->isadmin,
        ]);
        $block->save(false);
        // permissions for this menu               
        $auth = Yii::$app->authManager;
        $key = 'menu_manage_menu_by_id_' . $this->mid;
        $perm = $auth->createPermission($key);
        $perm->description = 'Manage menu - {menuname}';
        $auth->add($perm);
        if ($ai = AuthItem::findOne($key)) {
            $ai->isfromspider = 0;
            $ai->isshowatadminpage = 1;
            $ai->permcategory = 'Menu';
            $ai->permweight = $this->mid;
            $ai->valsfortranslate = serialize([
                'menuname' => $this->mname,
            ]);
            $ai->save(false);
        }        
    }    
    
    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */    
    public function handlerAfterDelete($event)
    {
        Block::deleteAll([
               'btype' => 'menublock',
               'bname' => $this->mid,
            ]                
         );        
        Menuitem::deleteAll([
               'mid' => $this->mid,
            ]                
         ); 
        $auth = Yii::$app->authManager;
        $key = 'menu_manage_menu_by_id_' . $this->mid;
        if ($perm = $auth->getPermission($key)) {
            $auth->remove($perm);
        }
    }
    
    /**
     * Getting menu manage links.
     * 
     * Format see - mgrechanik\cmscore\helpers\Common::decorateLinks
     * 
     * @return array
     */
    public function getAdminLinks()
    {
        $id = $this->mid;
        $user = Yii::$app->user;
        $links = [];
        if ($user->can('core_see_basic_admin_list_pages')) {
            $links[] = [
                'label' => Yii::t('cmscore', 'View'),
                'url' => ['/menu/admin/menu/task/view', 'id' => $id],
            ];
        }
        $perm = 'menu_manage_menu_by_id_' . $id;
        if ($user->can('menu_manage_all_menus') || $user->can($perm)) {
            $links[] = [
                'label' => Yii::t('adminnames', 'Edit menu name'),
                'url' => ['/menu/admin/menu/task/update', 'id' => $id],
            ];
        }
        if ($user->can('menu_manage_all_menus')) {
            $links[] = [
                'label' => Yii::t('cmscore', 'Delete'),
                'url' => ['/menu/admin/menu/task/delete', 'id' => $id],
            ];
        } 
        return $links;
    }
}
