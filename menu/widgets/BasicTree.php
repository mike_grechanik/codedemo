<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use mgrechanik\menu\helpers\MenuHelper;

/**
 * Widget to print menus
 * 
 * Look at example: \mgrechanik\menu\models\block\views\block.php
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class BasicTree extends Widget
{
    /**
     * @var array of top nodes which buil a tree
     * @see \mgrechanik\cmscore\models\traits\MaterializedPath::getArrayTree() 
     */
    public $tree = [];

    /**
     * @var boolean Whether to add additional div wrapper for submenus
     */
    public $addInnerUlWrapper = false;

    /**
     * @inheritdoc
     */    
    public function run()
    {
        print '<ul class="menu clearfix">';
        foreach ($this->tree as $node) {
            $liclass = '';
            $options = [];
            if ($node->value['enabled'] == 0) {
                $content = '';
            } else {
                $data = MenuHelper::getClassesFromNode($node);
                if ($aclass = $data['aclass']) {
                    $options['class'] = $aclass;
                }
                $menuitem = $node->value;
                if (!empty($menuitem['astyle'])) {
                    $options['style'] = $menuitem['astyle'];
                }  
                if ($node->value['iscategory']) {
                    $content = Html::tag('span', Html::encode($node->value['ltext']), $options);
                } else {
                    if (!empty($menuitem['ltitle'])) {
                        $options['title'] = $menuitem['ltitle'];
                    }
                    if (!empty($menuitem['atarget'])) {
                        $options['target'] = $menuitem['atarget'];
                    }        
                    if (!empty($menuitem['arel'])) {
                        $options['rel'] = $menuitem['arel'];
                    }                      
                    $content = Html::a(Html::encode($node->value['ltext']), $node->value['url'], $options);
                }
            }
            if ($content) {
                $options = [];
                if ($liclass = $data['liclass']) {
                    $options['class'] = $liclass;
                }                
                if (!empty($node->value['listyle'])) {
                    $options['style'] = $node->value['listyle'];
                }   
                if (!$node->isLeaf()//empty($node->children) 
                     && ($node->value['expanded'] == 1)
                    ) {
                    $add = self::widget(['tree' => $node->children, 'addInnerUlWrapper' => $this->addInnerUlWrapper]);
                    if ($this->addInnerUlWrapper) {
                        $add = Html::tag('div', $add, ['class' => 'submenu']);
                    }
                    $content .= $add;
                }                
                print Html::tag('li', $content, $options);
            }
        }
        print '</ul>';
    }
}

