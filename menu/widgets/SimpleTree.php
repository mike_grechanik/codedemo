<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use mgrechanik\menu\helpers\MenuHelper;

/**
 * Widget to print menus. Obsolete one. For example.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class SimpleTree extends Widget
{
    /**
     * @var array of top nodes which buil a tree
     * @see \mgrechanik\cmscore\models\traits\MaterializedPath::getArrayTree() 
     */    
    public $tree = [];
    
    /**
     * @inheritdoc
     */    
    public function run()
    {
        print '<ul class="menu clearfix">';
        foreach ($this->tree as $node) {
            if ($node->value['enabled'] == 0) {
                $content = '';
            } else {
                $content = MenuHelper::getAtagFromItem($node->value);
            }
            if ($content) {
                $options = [];
                if (!empty($node->value['licssclass'])) {
                    $options['class'] = $node->value['licssclass'];
                }                        
                if (!empty($node->value['listyle'])) {
                    $options['style'] = $node->value['listyle'];
                }   
                if (!$node->isLeaf()//empty($node->children) 
                     && ($node->value['expanded'] == 1)
                    ) {
                    $content .= self::widget(['tree' => $node->children]);
                }                
                
                print Html::tag('li', $content, $options);
            }
        }
        print '</ul>';
    }
}

