<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\menu\components;

use yii\base\BootstrapInterface;

/**
 * Bootstrap class for the menu module.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        // setting message sourse
        $i18n = $app->i18n;
        $i18n->translations['menu'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@app/data/messages',
            'fileMap' => [
                'menu' => 'menu.php',
            ],
        ];  
    }
}
