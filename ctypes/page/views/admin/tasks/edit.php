<?php
$hint = '';
$pane = '';
if (!Yii::$app->urlManager->isOneLanguage()) {
    $langcode = $model->lang;
    $um = \Yii::$app->get('urlManager');
    $linfo = $um->language->getLangFromCode($langcode);
    if ($linfo) {
        $hint = '<div class=infoitem>' . Yii::t('cmscore', 'Language') . ' : <strong>' . $linfo['label'] . '</strong></div>';
    }
    $mid = $module->id;
    $res = \mgrechanik\ctypes\page\service\ContentTypeHelper::getLanguagePane(
        $model,
        "/$mid/admin/tasks/edit",
        "/$mid/admin/tasks/create"
    );
    $pane = '<div class="tab2">';
    foreach ($res as $val) {
        $pane .= $val;
    } 
    $pane .= '</div>';
}
?>

<div>
    <?php if ($pane) print $pane;  ?>
    <h1><?= \Yii::t('cmscore', 'Editing the Page') ?></h1>
    <?php if ($hint) { ?>
        <?= $hint ?>
    <?php } ?>
    
    <?= $this->render($module->formTemplate, [
        'model' => $model,
        'module' => $module,
    ]) ?>  
</div>
