<?php

use yii\helpers\Html;
?>
<h1><?= \Yii::t('cmscore', 'Deleting the Page') ?></h1>
<?= Html::beginForm(); ?>
Вы действительно хотите удалить страницу "<?= Html::encode($node->title) ?>" ?
<br><br>
<?= Html::submitButton(Yii::t('cmscore', 'Delete'), ['name' => 'Deletenode', 'class' => 'btn btn-primary']);?>
<?= Html::endForm(); ?>


