<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mgrechanik\ctypes\page\models\Pagenode;
use mgrechanik\cmscore\models\Tformats;
use yii\bootstrap\Modal;

\yii\gii\GiiAsset::register($this);
\mgrechanik\cmscore\assets\ShortCodes\ScodesAsset::register($this);
\mgrechanik\cmscore\assets\ShortCodes\ImageButtonAsset::register($this);
$this->registerJs('imagebuttonformats = ' . Tformats::getJsonFormatsWithImageButton() . ";", \yii\web\View::POS_BEGIN);
Modal::begin([
    'header' => '<h4>' . Yii::t('cmscore', 'Inserting the shortcode') . '</h4>',
    'options' => [
        'id' => 'shortcode-modal-window',
    ],
]);
echo '';
Modal::end();
Modal::begin([
    'header' => '<h4>' . Yii::t('cmscore', 'Inserting the image') . '</h4>',
    'options' => [
        'id' => 'imagebutton-modal-window',
    ],
]);
print $this->renderFile('@mgrechanik/cmscore/admin/views/admin/page/getelfinder.php');
Modal::end();
?>
<div class="default-view">
<?php $form = ActiveForm::begin(['enableClientValidation' => true]) ?>
<?php 
if (!empty($langchoice)) {
    print $langchoice;
}
?>
<?= $form->field($model, 'title')->textInput() ?>
<?php
    $formatokey = $model->isAttributeSafe('tformat');
    if ($formatokey) {
        print Html::button(Yii::t('cmscore', 'Insert the image'), ['class' => 'btn btn-success imageinsertbutton', 'name' => 'pagenode-tformat', 'value' => 'pagenode-body']);
        print '&nbsp;' . Html::button(Yii::t('cmscore', 'Insert the shortcode'), ['class' => 'btn btn-success scodesbutton', 'name' => 'pagenode-tformat', 'value' => 'pagenode-body']);        
    }
    print $form->field($model, 'body')->textarea(['disabled' => !$formatokey]);
    if ($formatokey) {
        print $form->field($model, 'tformat', ['labelOptions' => ['class' => 'control-label tformat-help']])
                   ->dropDownList(Yii::$app->user->getFormats())->hint('&nbsp;');
    } else {
        if ($format = Tformats::findCachedFormat($model->tformat)) {
            print '<div>' . Yii::t('cmscore', 'Body format') . ' : '. Html::encode($format->fullname) .
                  ' - ' .  Yii::t('cmscore', 'You do not have permissions to edit this text format') . '</div>';
        }
    }
?>    
<?= $form->field($model, 'status')->radioList([
    Pagenode::STATUS_PUBLICHED => \Yii::t('cmscore', 'Published'),
    Pagenode::STATUS_NOTPUBLICHED => \Yii::t('cmscore', 'Not published'),
]) ?>
<?= $model->renderFields($form, $this); ?>
<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('cmscore', 'Create') : Yii::t('cmscore', 'Save'), ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
</div>