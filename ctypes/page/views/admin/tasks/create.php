<?php
$hint = '';
if (!Yii::$app->urlManager->isOneLanguage() && (isset($_GET['item']))) {
    $langcode = $model->lang;
    $um = \Yii::$app->get('urlManager');
    $linfo = $um->language->getLangFromCode($langcode);
    if ($linfo) {
        $hint = '<div class=infoitem>' . Yii::t('cmscore', 'Language') . ' : <strong>' . $linfo['label'] . '</strong></div>';
    }
}
$langchoice = '';
$items = Yii::$app->urlManager->getOptionsForAllLanguages();
if (!isset($_GET['item']) && (count($items) > 1)) {
    $langchoice = '<div class="langcode-choice">' . Yii::t('cmscore', 'Language') . ':' . \yii\helpers\Html::radioList('langcode', [$model->lang], $items, ['style' => 'display:inline-block;']) . '</div>';
}
?>
<div>
    <h1><?= \Yii::t('cmscore', 'Creating a new Page'); ?></h1>
    <?php if ($hint) { ?>
        <?= $hint ?>
    <?php } ?>
    
    <?= $this->render($module->formTemplate, [
        'model' => $model,
        'langchoice' => $langchoice,
        'module' => $module,
    ]) ?>    
</div>
