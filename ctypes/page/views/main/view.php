<?php
use yii\helpers\Html;
?>
<article class="node node-page page-default-index clearfix">
	<header><h1><?= Html::encode($node->title) ?></h1></header>
    <div class="content clearfix">
    <?= $node->cache ?>
    </div>
</article>
