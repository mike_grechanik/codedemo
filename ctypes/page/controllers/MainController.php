<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\ctypes\page\controllers;

use Yii;
use mgrechanik\cmscore\base\Controller;
use yii\base\InvalidRouteException;
use mgrechanik\cmscore\helpers\Common;
use mgrechanik\ctypes\page\service\ContentTypeHelper;

/**
 * Controller to display "basic page" nodes
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class MainController extends Controller
{
    /**
     * @var integer The unique identifier of the content type (nodes of which are being managed).
     */
    public $contentType = 1;
    
    /**
     * Action to display a node page.
     * 
     * @param integer $id
     * @return mixed
     * @throws InvalidRouteException
     */
    public function actionView($id)
    {
        $pmanager = Yii::$app->get('pageManager');
        $meta = $this->getThisContentTypeMetaData();
        $class = $meta['contentModelClass'];                
        $node = $class::findOne($id);
        $okey = true;
        if (!$node || ($node->lang != $pmanager->langCode)) {
            $okey = false;
        } else {
            if ($node->status != 1) {
                if (!Yii::$app->user->can('core_see_basic_admin_list_pages')) {
                    $okey = false;
                } else {
                    $lang = null;
                    $identity = Yii::$app->user->getIdentity();
                    if (isset($identity->adminlang)) {
                        $lang = $identity->adminlang;
                    }   
                    Yii::$app->session->setFlash('form-successful-message', Yii::t('cmscore', 'Attention!<br> This post is not published and thereby it is not available by site users exept those with admin access.', [], $lang));
                }
            }
        }
        if (!$okey) {
            throw new InvalidRouteException('Page not found');
        }
        $pmanager->pageType = $node->contentType;
        $pmanager->pageTypedata['nid'] = $id;
        $pmanager->currentData['node']['nid'] = $id;
        $pmanager->currentData['node']['object'] = $node;
        ContentTypeHelper::setLanguageSwitcher($node);
        // --- CACHING
        $process = Common::whetherToRebuiltCache($node->tformat, $node->cache, $node->cache_data);
        if ($process) {
            // creating the cache
            $res = Common::format($node->tformat, $node->body, true);
            $node->cache = $res['text'];
            // saving the cache
            $data = serialize($res['data']);
            $class::updateAll(['cache' => $res['text'], 'cache_data' => $data],
                    'nid=:nid', ['nid' => $node->nid]);
            // end saving the cache
        } else {
            $data = [];
            if ($node->cache_data) {
                $data = unserialize($node->cache_data);
            }
            Common::processFormatData($data);
        }
        // --- END CACHING
        
        return $this->render($this->module->mainViewTemplate, ['node' => $node]);
    }
    
    /**
     * @inheritdoc
     */    
    public function getMetaData()
    {
        return [
            'view' => [
                'pageargs' => ['id'],
             ],            
        ];
    }
    
    /**
     * Returning a meta data for current content type.
     * 
     * @return array
     */
    protected function getThisContentTypeMetaData()
    {
        $types = Yii::$app->get('pageManager')->getContentTypes();
        return $types[$this->contentType];
    }         
}
