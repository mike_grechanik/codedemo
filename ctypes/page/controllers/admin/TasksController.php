<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\ctypes\page\controllers\admin;

use Yii;
use mgrechanik\cmscore\base\AdminController;
use yii\base\InvalidRouteException;
use mgrechanik\user\models\basic\PermissionInterface;
use yii\filters\AccessControl;
use yii\helpers\Html;
use mgrechanik\cmscore\models\FilesManagableInterface;

/**
 * Basic controller to manage content types.
 * 
 * It could be reused by other content types.
 * Use [[contentType]] to configure with what content type we are working.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class TasksController extends AdminController implements PermissionInterface
{
    /**
     * @var integer The unique identifier of the content type (nodes of which are being managed).
     */
    public $contentType = 1;

    /**
     * @var string The view page of the nodes of the [[contentType]] content type.
     * Used for a redirect.
     */
    public $contentViewPage = '/page/main/view';

    /**
     * @inheritdoc
     */        
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['core_all_access_to_content_types_nodes'],
                    ],                    
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['basicpages_create'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['edit'],
                        'roles' => ['basicpages_edit_all', 'basicpages_edit_own'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['basicpages_delete_all', 'basicpages_delete_own'],
                    ],
                    
                ],
            ],
        ];
    }  
    
    /**
     * Creating a new node.
     * 
     * @param string $langcode
     * @param integer $item The nid of the node which is main language node for the created one.
     * @return mixed
     * @throws InvalidRouteException
     */
    public function actionCreate($langcode = '', $item = null)
    {
        $this->setAdminPath('create');
        $meta = $this->getThisContentTypeMetaData();
        $class = $meta['contentModelClass'];
        $model = new $class(['title' => '', 'status' => 1]);
        // checking language
        $lang = (!empty($langcode)) ? $langcode : Yii::$app->get('urlManager')->getLanguageCode();
        if (isset($_POST['langcode'])) {
            $lang = $_POST['langcode'];
        }
        if (!Yii::$app->get('urlManager')->isLangCodeExists($lang)) {
            throw new InvalidRouteException('Wrong language code');
        }
        $model->lang = $lang;
        $model->initFieldsModel();
        $issaveok = false;
        if ($model->load(Yii::$app->request->post()) ) {
            if ($model instanceof FilesManagableInterface) {
                $model->loadFiles();
            }            
            $model->loadFields();
            $issaveok = $model->validate() & $model->validateFields();
            if ($issaveok) {
                // saving
                //first save
                $model->save(false);
                // preparation for second save
                $model->uid = (int) Yii::$app->user->getId();
                $model->item = $model->nid;
                if (!empty($item)) {
                    if ($class::findOne($item)) {
                        $model->item = $item;
                    }
                }
                $model->created = time();
                if ($model instanceof FilesManagableInterface) {
                    $model->saveFiles();
                }                                
                // second save
                $model->save(false);
                $model->adjustFieldsModel();
                // saving fields
                $model->saveFields();
                //redirect
                $flashText = Yii::t('cmscore', 'The new page has been created') . '<br>';
                $where = [
                    $this->contentViewPage,
                    'id' => $model->nid,
                    'lang' => $model->lang,
                    'cross' => 'front',
                ];
                $flashText .= Yii::t('cmscore', 'To visit the changed page follow the {link}', [
                    'link' => \yii\helpers\Html::a(Yii::t('cmscore', 'link'), \yii\helpers\Url::to($where)),
                ]);                                        
                Yii::$app->session->setFlash('form-successful-message', $flashText);
                return $this->redirect(['/admin/admin/page/list', 'type' => $model->contentType]);                
                // end redirect
            } else {
                $errorText = '';
                if ($model->hasErrors()) {
                    $errorText .= Html::errorSummary($model);
                }
                if ($errors = $model->getFieldsErrors()) {
                    $errorText .= Html::ul($errors);
                }
                Yii::$app->session->setFlash('form-error-message', $errorText);
            }
        }

        if (!$issaveok) {
            return $this->render('create', [
                'model' => $model,
                'module' => $this->module,
            ]);
        }        
    }    
    
    /**
     * Editing a node.
     * 
     * @param integer $id
     * @return mixed
     * @throws InvalidRouteException
     */
    public function actionEdit($id)
    {
        $meta = $this->getThisContentTypeMetaData();
        $class = $meta['contentModelClass'];
        $model = $class::findOne($id);
        if (!$model) {
           throw new InvalidRouteException('This Page is not found'); 
        }
        $this->setAdminPath('edit', $model);
        $model->initFieldsModel();
        $issaveok = false;
        if ($model->load(Yii::$app->request->post()) ) {
            if ($model instanceof FilesManagableInterface) {
                $model->loadFiles();
            }
            $model->loadFields();
            $issaveok = $model->validate() & $model->validateFields();
            if ($issaveok) {
                // saving
                if ($model instanceof FilesManagableInterface) {
                    $model->saveFiles();
                }                
                $model->save(false);
                $model->adjustFieldsModel();
                // saving fields
                $model->saveFields();
                //redirect
                $flashText = Yii::t('cmscore', 'The changes are saved successfully') . '<br>';
                $where = [
                    $this->contentViewPage,
                    'id' => $model->nid,
                    'lang' => $model->lang,
                    'cross' => 'front',
                ];
                $flashText .= Yii::t('cmscore', 'To visit the changed page follow the {link}', [
                    'link' => \yii\helpers\Html::a(Yii::t('cmscore', 'link'), \yii\helpers\Url::to($where)),
                ]);                                        
                Yii::$app->session->setFlash('form-successful-message', $flashText);
                return $this->redirect(['/admin/admin/page/list', 'type' => $model->contentType]);                
                // end redirect
            } else {
                $errorText = '';
                if ($model->hasErrors()) {
                    $errorText .= Html::errorSummary($model);
                }
                if ($errors = $model->getFieldsErrors()) {
                    $errorText .= Html::ul($errors);
                }
                Yii::$app->session->setFlash('form-error-message', $errorText);                
            }
        }

        if (!$issaveok) {
            return $this->render('edit', [
                'model' => $model,
                'module' => $this->module,
            ]);
        }            
        
    }    
    
    /**
     * Deleting a node.
     * 
     * @param integer $id
     * @return mixed
     * @throws InvalidRouteException
     */
    public function actionDelete($id)
    {
        $meta = $this->getThisContentTypeMetaData();
        $class = $meta['contentModelClass'];
        $node = $class::findOne($id);
        if (!$node) {
           throw new InvalidRouteException('This Page is not found'); 
        }
        $this->setAdminPath('delete', $node);
        // whether the node is not for deleting
        $permanent = false;
        if ($this->module instanceof \mgrechanik\ctypes\page\CtypeModule
             && in_array(intval($id), $this->module->permanentNodes)) {
            $permanent = true;
            Yii::$app->session->setFlash('form-successful-message', Yii::t('cmscore', 'You cannot delete this post because it serves as the one for main page'));
        }
        if (isset($_POST['Deletenode']) && !$permanent) {
            $type = $node->contentType;
            $node->delete();
            Yii::$app->session->setFlash('form-successful-message', Yii::t('cmscore', 'The Page has been deleted'));
            return $this->redirect(['/admin/admin/page/list', 'type' => $type]);
        }
        return $this->render('delete', ['node' => $node, 'module' => $this->module,]);
    }
    
    /**
     * Setting breadcrumbs and current admin path to the choosen action.
     * 
     * @param string $action One of the: create, edit, delete
     * @param object $node
     */
    protected function setAdminPath($action, $node = null)
    {
        $meta = $this->getThisContentTypeMetaData();
        $innername = $meta['innername']; 
        $this->getView()->params['breadcrumbs'][] = ['label' => Yii::t('adminnames', 'All pages'), 'url' => ['/admin/admin/page/list']];
        $this->getView()->params['breadcrumbs'][] = ['label' => Yii::t('adminnames', $meta['name']), 'url' => ['/admin/admin/page/list', 'type' => $this->contentType]];
        $title = '';
        if ($action == 'create') {
            $this->initializeAdminPage('pages/create/' . $innername);
            $title = Yii::t('cmscore', 'Creating a new Page');
        } else {
            $this->initializeAdminPage('pages/all/' . $innername . '/{id}/' . $action,
                [
                    'nodetype' => $this->contentType,
                    'nodeid' => $node->nid,
                    'nodename' => $node->title,
                    'nodelang' => $node->lang,
                ],
                [
                    'nodeobject' => $node,
                ]
            );
            if ($action == 'edit') {
                $title = Yii::t('cmscore', 'Editing the Page');
            } else {
                $title = Yii::t('cmscore', 'Deleting the Page');
            }
            //$title .= ' : ' . Html::encode($node->title);
            $title .= ' : ' . $node->title;
        }
        $this->getView()->params['breadcrumbs'][] = $this->getView()->title = $title;
    }
    
    /**
     * Returning a meta data for current content type.
     * 
     * @return array
     */
    protected function getThisContentTypeMetaData()
    {
        $types = Yii::$app->get('pageManager')->getContentTypes();
        return $types[$this->contentType];
    }    

    /**
     * @inheritdoc
     */       
    public function getPermissions()
    {
        $meta = $this->getThisContentTypeMetaData();
        $prefix = $meta['permissionPrefix'];
        return [
            $prefix .'create' => [
                'description' => 'Create a new basic page',
            ],
            $prefix . 'edit_all' => [
                'description' => 'Edit any basic pages',
            ],            
            $prefix . 'delete_all' => [
                'description' => 'Delete any basic pages',
            ],  
            $prefix . 'edit_own' => [
                'description' => 'Edit own basic page',
                'rule' => 'mgrechanik\ctypes\page\models\rbac\ManageOwnBasicPage',
            ],            
            $prefix . 'delete_own' => [
                'description' => 'Delete own basic page',
                'rule' => 'mgrechanik\ctypes\page\models\rbac\ManageOwnBasicPage',
            ],  
            'category' => 'Basic pages',
        ];
    } 
    
    /**
     * @inheritdoc
     */       
    public function getAdminPages()
    {
        $meta = $this->getThisContentTypeMetaData();
        $prefix = $meta['permissionPrefix'];
        $innername = $meta['innername'];
        return [
            'pages/create/' . $innername => [
                'text' => $meta['name'],
                'url' => ['/' . $innername . '/admin/tasks/create'] ,
                'permissions' => ['core_all_access_to_content_types_nodes', $prefix . 'create'],
            ],
            'pages/all/' . $innername . '/{id}' =>[
                'text' => '{nodename}',
                'isCategory' => 1,
                'is_show_dynamic_link_callback' => 'mgrechanik\cmscore\components\PageManager::isNodePage',
            ],
            'pages/all/' . $innername . '/{id}/edit' =>[
                'text' => 'Edit',
                'url' => ['/' . $innername . '/admin/tasks/edit', 'id' => '{nodeid}'] ,
                'isTab' => 1,
                'is_show_dynamic_link_callback' => 'mgrechanik\cmscore\components\PageManager::isNodePage',
                'permissions' => ['core_all_access_to_content_types_nodes', $prefix . 'edit_all', $prefix . 'edit_own'],
                'paramsforcan' => ['nodeid' => '{nodeid}'],
            ], 
            'pages/all/' . $innername . '/{id}/delete' =>[
                'text' => 'Delete',
                'url' => ['/' . $innername . '/admin/tasks/delete', 'id' => '{nodeid}'] ,
                'isTab' => 1,
                'is_show_dynamic_link_callback' => 'mgrechanik\cmscore\components\PageManager::isNodePage',
                'permissions' => ['core_all_access_to_content_types_nodes', $prefix . 'delete_all', $prefix . 'delete_own'],
                'paramsforcan' => ['nodeid' => '{nodeid}'],
            ],
            'pages/all/' . $innername . '/{id}/view' =>[
                'text' => 'View',
                'url' => ['/' . $innername . '/main/view', 'id' => '{nodeid}', 'lang' => '{nodelang}', 'cross' => 'front'] ,
                'isTab' => 1,
                'linkoptions' => ['class' => 'externallink'],
                'is_show_dynamic_link_callback' => 'mgrechanik\cmscore\components\PageManager::isNodePage',
            ],              
        ];
    }
    
}
