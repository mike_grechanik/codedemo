<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\ctypes\page\models\rbac;

use yii\rbac\Rule;
use mgrechanik\ctypes\page\models\Pagenode;

/**
 * Rule to check whether the page is created by user in request.
 * 
 * Checks if authorID matches user passed via params
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class ManageOwnBasicPage extends Rule
{
    /**
     * @inheritdoc
     */     
    public $name = 'manage_own_basic_page';
    
    /**
     * @inheritdoc
     */     
    public function execute($user, $item, $params)
    {
        $user = \Yii::$app->user;
        if ($id = (int) $user->getId()) {
            $nid = (isset($params['nodeid'])) ? (int) $params['nodeid'] : null;
            if (!$nid && isset($_GET['id'])) {
                $nid = (int) $_GET['id'];
            }
            if (($nid) && ($node = Pagenode::findOne($nid))) {
                if ($node->uid == $id) {
                    return true;
                }
            }
        } 
        return false;
    }    
}