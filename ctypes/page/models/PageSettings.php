<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\ctypes\page\models;

use mgrechanik\cmscore\models\traits\Fields;
use mgrechanik\cmscore\models\FieldsInterface;
use Yii;

/**
 * Model to manage "basic page" content type settings
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class PageSettings extends \yii\base\Model implements FieldsInterface
{
    /**
     * Mandatory for Fields
     */
    use Fields;
    
    /**
     * @var integer We put it to -1 because it is content type settings
     * @see \mgrechanik\cmscore\models\ContentSettings
     */
    public $nid = -1;
    
    /**
     * @var integer The unique number of this content type.
     * @see \mgrechanik\cmscore\models\ContentSettings
     */
    public $contentType = 1;
    
    /**
     * @var boolean We set it for fields behaviors 
     */
    public $isNewRecord = false;

    /**
     * @inheritdoc
     */      
    public function behaviors()
    {
        return [
            'pathautotemplate' => [
                'class' => 'mgrechanik\cmscore\behaviors\pathauto\PathAutoContentTypeSettingsBehavior',
                'tokengroup' => 'forpathauto'
            ],    
            'whichmenus' => [
                'class' => 'mgrechanik\menu\behaviors\WhichMenuBehavior',
            ],    
            'comments' => [
                'class' => 'mgrechanik\comments\behaviors\CommentContentTypeSettingsBehavior',
            ],
            'sitemaps' => [
                'class' => 'mgrechanik\cmscore\behaviors\sitemap\SitemapContentTypeSettingsBehavior',
            ],            
        ];
    }
    
    /**
     * @inheritdoc
     */     
    public function getFieldNames()
    {
        return [
            'pathautotemplate',
            'whichmenus',
            'comments',
            'sitemaps',
        ];
    }  
    
    /**
     * @inheritdoc
     */     
    public function getFieldsForRender()
    {
        return [
            'pathautotemplate' => [
                'title' => Yii::t('cmscore', 'Url addresses settings'),
                'fieldnames' => [
                    'pathautotemplate' => '', 
                ],
            ],                        
            'whichmenus' => [
                'title' => Yii::t('cmscore', 'Menu'),
                'fieldnames' => [
                    'whichmenus' => '', 
                ],
            ],            
            'comments' => [
                'title' => Yii::t('cmscore', 'Comments'),
                'fieldnames' => [
                    'comments' => '', 
                ],
            ], 
            'sitemaps' => [
                'title' => Yii::t('cmscore', 'Site maps'),
                'fieldnames' => [
                    'sitemaps' => '',
                ],
            ],            
        ];        
    }        
    
}
