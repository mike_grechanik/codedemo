<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\ctypes\page\models;

use Yii;

use mgrechanik\cmscore\models\traits\Fields;
use mgrechanik\cmscore\models\FieldsInterface;
use mgrechanik\cmscore\models\traits\ContentType;

/**
 * This is the model class for table "{{%pagenode}}".
 * 
 * This model is the main model for "basic page" content type.
 *
 * @property integer $nid
 * @property integer $item
 * @property string $lang
 * @property string $title
 * @property string $body
 * @property integer $pageid
 * @property integer $uid
 * @property integer $status
 * @property integer $created
 * @property integer $changed
 * @property string $tformat
 * @property string $cache
 * @property string $cache_data
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Pagenode extends PublishedNode implements FieldsInterface
{
    /**
     * Madatory for fields functionality
     */
    use Fields;
    
    /**
     * Mandatory for a content type functionality
     */
    use ContentType;
    
    /**
     * @var string The inner url of the view page of this content type node.
     * @see \mgrechanik\ctypes\page\service\ContentTypeHelper::setLanguageSwitcher()
     * @see \mgrechanik\cmscore\behaviors\AliasFieldBehavior::afterInsert()
     */
    public $innerUrlTemplate = 'page/main/view?id=<nid>';
    
    /**
     * @var integer Unique number of this content type 
     */
    public $contentType = 1;

    /**
     * @inheritdoc
     */  
    public static function tableName()
    {
        return '{{%pagenode}}';
    }

    /**
     * @inheritdoc
     */      
    public function behaviors()
    {
        return [
            'aliasfield' => [
                'class' => 'mgrechanik\cmscore\behaviors\AliasFieldBehavior',
                'fieldsAdjust' => [
                    'pageid' => 'pageid',
                ],
                'tokengroup' => 'forpathauto',
            ],
            'pagetracker' => [
                'class' => 'mgrechanik\cmscore\behaviors\PageTrackerBehavior',
            ],            
            'seofield' => [
                'class' => 'mgrechanik\cmscore\behaviors\SeoFieldBehavior',
                'fieldsAdjust' => [
                    'pageid' => 'pageid',
                ],
            ],
            'menuchoice' => [
                'class' => 'mgrechanik\menu\behaviors\ContentMenuFieldBehavior',
            ],                        
            'comments' => [
                'class' => 'mgrechanik\comments\behaviors\CommentNodeSettingsBehavior',
            ],                                    
            'sitemaps' => [
                'class' => 'mgrechanik\cmscore\behaviors\sitemap\SitemapNodeSettingsBehavior',
            ],            
        ];
    }
    
    /**
     * @inheritdoc
     */      
    public function getFieldNames()
    {
        return [
            'aliasfield', 
            'pagetracker', 
            'seofield',
            'menuchoice',
            'comments',
            'sitemaps' // it should be after 'aliasfield'
        ];
    }  
    
    /**
     * @inheritdoc
     */      
    public function getFieldsForRender()
    {
        return [
            'seo' => [
                'title' => Yii::t('cmscore', 'Seo'),
                'fieldnames' => [
                    'aliasfield' => '', 
                    'seofield' => '',
                    'sitemaps' => Yii::t('cmscore', 'Site maps'),
                ],
            ],
            'menuchoice' => [
                'title' => Yii::t('cmscore', 'Menu'),
                'fieldnames' => [
                    'menuchoice' => '', 
                ],
            ],            
            'comments' => [
                'title' => Yii::t('cmscore', 'Comments'),
                'fieldnames' => [
                    'comments' => '', 
                ],
            ],                        
        ];        
    }
    
    /**
     * @inheritdoc
     */    
    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'handlerAfterDelete']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'handlerBeforeUpdate']);
    }        

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $res = [
            [['lang', 'title', 'status'], 'required'],
            [['status'], 'in', 'range' => [self::STATUS_NOTPUBLICHED, self::STATUS_PUBLICHED]],
            [['lang'], 'string', 'max' => 8],
            [['title'], 'string', 'max' => 256]
        ];
        $formatOkey = false;
        $formats = Yii::$app->user->getFormats();
        if ($this->isNewRecord) {
            $formatOkey = true;
        } else {
            if (isset($formats[$this->tformat])) {
                $formatOkey = true;
            }
        }
        if ($formatOkey) {
            $res[] = [['body', 'tformat'], 'required'];
            $res[] = [['body'], 'string'];
            $res[] = [['tformat'], 'in', 'range' => array_keys($formats)];
        }
        return $res;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nid' => Yii::t('cmscore', 'Nid'),
            'item' => Yii::t('cmscore', 'Item'),
            'lang' => Yii::t('cmscore', 'Lang'),
            'title' => Yii::t('cmscore', 'Title'),
            'body' => Yii::t('cmscore', 'Body'),
            'pageid' => Yii::t('cmscore', 'Pageid'),
            'uid' => Yii::t('cmscore', 'Uid'),
            'status' => Yii::t('cmscore', 'Status'),
            'created' => Yii::t('cmscore', 'Created'),
            'changed' => Yii::t('cmscore', 'Changed'),
            'tformat' => Yii::t('cmscore', 'Body format'),
        ];
    }
    
    /**
     * Event handler.
     * 
     * @param \yii\base\Event $event
     */
    public function handlerBeforeUpdate($event)
    {
        $this->changed = time();
        $this->cache = '';
    }

    /**
     * Event handler.
     * 
     * @param \yii\base\Event $event
     */    
    public function handlerAfterDelete($event)
    {
        $item = $this->item;
        if (($this->nid == $item) && ($newMainNode = self::findOne(['item' => $item]))) {
            $newMainNode->makeMeAsMain();
        }
    }
    
}
