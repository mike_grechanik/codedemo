<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\ctypes\page\models;

/**
 * Parent class for nodes who might have a status of being (not)published
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
abstract class PublishedNode extends \yii\db\ActiveRecord
{
    /**
     * Constants of publishing state
     */
    const STATUS_NOTPUBLICHED = 0;
    const STATUS_PUBLICHED = 1;      
}
