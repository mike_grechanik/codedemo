<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\ctypes\page\service;

use yii\db\ActiveRecord;
use Yii;
use yii\helpers\Html;
use mgrechanik\ctypes\page\models\PublishedNode;
use mgrechanik\cmscore\helpers\Common;

/**
 * Helper to do all the service work for content types functionality
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class ContentTypeHelper
{
    /**
     * Returns the page to language connected nodes. Is shown at node edit page.
     * 
     * @param ActiveRecord $model
     * @param string $editurl Url to editing model's content ype nodes
     * @param string $createurl Url to creating model's content ype nodes
     * @return array
     * @throws \Exeption
     * @see \mgrechanik\ctypes\page\views\admin\tasks\edit.php
     */
    public static function getLanguagePane(ActiveRecord $model, $editurl, $createurl)
    {
        if (!isset($model->nid) || !isset($model->item)) {
            throw new \Exeption('the model is incorrent content type model');
        }
        $createperm = 'unknown_create';
        $type = $model->contentType;
        $types = Yii::$app->get('pageManager')->getContentTypes();
        if (isset($types[$type])) {
            $createperm = $types[$type]['permissionPrefix'] . 'create';
        }
        $res = [];
        $codes = Yii::$app->urlManager->language->langCodes;
        $exist = [];
        if (!$model->isNewRecord) {
            $class = get_class($model);
            $pages = $class::find()->asArray()->where(['item' => $model->item])->all();
            foreach ($pages as $val) {
              $exist[$val['lang']] = $val;
            }        
        }
        foreach ($codes as $key => $val) {
            $what = '';
            $url = '';
            if (isset($exist[$key])) {
                if ($exist[$key]['nid'] != $model->nid) {
                  $what = '-(edit)';
                }
                $url = [$editurl, 'id' => $exist[$key]['nid']];
            } else {
                $user = Yii::$app->user;
                if ($user->can('core_all_access_to_content_types_nodes') || $user->can($createperm)) {
                    $what = '-(create)';
                    $url = [$createurl, 'langcode' => $key, 'item' => $model->item];
                } else {
                    continue;
                }
            }
            $res[$key] = ($what) ? 
                         Html::a($codes[$key]['label'] . $what, $url) :
                         Html::tag('span', $codes[$key]['label'], ['class' => 'active']);
        }
        return $res;        
    }
    
    /**
     * Sets the data for a language swither based on the node model.
     * 
     * @param ActiveRecord $model Node model being displayed at the page
     * @throws \Exeption
     */
    public static function setLanguageSwitcher(ActiveRecord $model)
    {
        if (!isset($model->nid) || !isset($model->item) || !isset($model->innerUrlTemplate)) {
            throw new \Exeption('the model is incorrent content type model');
        }        
        $pm = Yii::$app->get('pageManager');
        $um = Yii::$app->urlManager;
        if ($um->isOneLanguage()) {
            return null;
        }
        $codes = $um->language->langCodes;
        $item = $model->item;
        $class = get_class($model);
        $status = [PublishedNode::STATUS_PUBLICHED];
        $user = Yii::$app->user;
        if (!$user->isGuest && $user->can('core_see_basic_admin_list_pages')) {
            $status[] = PublishedNode::STATUS_NOTPUBLICHED; 
        }
        $nodes = $class::find()->asArray()->where(['item' => $item, 'status' => $status])->all();
        $res = [];
        foreach ($nodes as $val) {
            $nid = $val['nid'];
            $lang = $val['lang'];
            if ($nid == $model->nid) {
                $res[$lang]['url'] = '';
            } else {
                $innerUrl = str_replace('<nid>', $nid, $model->innerUrlTemplate);
                if ($um->isFrontPage($lang, $innerUrl)) {
                    $innerUrl = '/<front>';
                }                
                $res[$lang]['url'] = Common::urlTo($innerUrl, ['lang' => $lang]); 
            }
            $res[$lang]['label'] = $codes[$lang]['label'];
            $res[$lang]['shortLabel'] = $codes[$lang]['shortLabel'];
        }
        $pm->setLangSwitcher($res);         
    }
}

