<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\ctypes\page;

/**
 * Parent class for a cmf module which implements a content type.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
abstract class CtypeModule extends \mgrechanik\cmscore\base\Module
{
    /**
     * @var string The name of the view with which we display the page with this content type node.
     * Type of parameter is what is passed into $view->render()
     * Example:'//override/cpageview' (how to override it in the final site)
     */
    public $mainViewTemplate = 'view';  
    
    /**
     * @var string The name of the view with which we display the editing form for this content type node.
     * Type of parameter is what is passed into $view->render()
     * Example:'//override/cpageform' (how to override it in the final site)
     */
    public $formTemplate = '_form';  
    
    /**
     * @var integer[] The list of node nid's we no not want for admin to be able to delete.
     * For example if the node is maid as the one for front page we do not want
     * for admin to be able to delete it by mistake.
     */
    public $permanentNodes = [];
}
