<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\ctypes\page;

/**
 * Module to produce basic page content type.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Module extends CtypeModule
{
    /**
     * @inheritdoc
     */       
    public $controllerNamespace = 'mgrechanik\ctypes\page\controllers';

    /**
     * @inheritdoc
     */       
    public function getMetaData()
    {
        return [
            'contentTypes' => [
                1 => [
                    'name' => 'Page',
                    'innername' => 'page',
                    'permissionPrefix' => 'basicpages_',
                    'adminLinks' => [
                        'Edit' => '/page/admin/tasks/edit?id=<nid>',
                        'Delete' => '/page/admin/tasks/delete?id=<nid>',
                    ], 
                    'adminLinksPermissionCallbacks' => [
                        'Edit' => 'mgrechanik\cmscore\components\PageManager::isShowNodeEditLinkInList',
                        'Delete' => 'mgrechanik\cmscore\components\PageManager::isShowNodeDeleteLinkInList',
                    ],                      
                    'contentSettingsModelClass' => 'mgrechanik\ctypes\page\models\PageSettings',
                    'contentModelClass' => 'mgrechanik\ctypes\page\models\Pagenode',
                ]
            ],
           'tokens' => [
               'nodetitle' => [
                   'callback' => 'mgrechanik\cmscore\service\BasicFilters::nodeTokens',
                   'title' => 'The title of the current post',
                   'template' => '[node:title]'
               ],
           ], 
           'tokengroups' => [
               'forpathauto' => [
                   'group1' => [
                       'title' => 'Dates',
                       'tokens' => ['shortdate1', 'shortdate2'],
                   ],
                   'group2' => [
                       'title' => 'Current post',
                       'tokens' => ['nodetitle'],
                   ],                   
               ],
           ], 
        ];
    }     
}
