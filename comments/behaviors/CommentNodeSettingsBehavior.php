<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\behaviors;

use mgrechanik\comments\models\fields\CommentContentTypeSettingsModel;
use yii\db\ActiveRecord;
use mgrechanik\cmscore\behaviors\FieldsBehavior;
use mgrechanik\cmscore\models\ContentSettings;
use mgrechanik\comments\models\CommentStatistic;
use Yii;

/**
 * This field is for choosing comments settings for a node
 * 
 * @see \mgrechanik\ctypes\page\models\Pagenode
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class CommentNodeSettingsBehavior extends FieldsBehavior
{
    /**
     * @inheritdoc
     */      
    public $template = '@mgrechanik/comments/models/fields/fieldviews/commentsettingsnodeedit.php';

    /**
     * @var ContentSettings The model to save our settings
     */      
    protected $saveModel = null;

    /**
     * @inheritdoc
     */      
    public function initModel()
    {
        $mainModel = $this->owner;
        $conf0 = [
            'pagetype' => $mainModel->contentType,
            'nid' => $mainModel->nid,
            'what' => 'comments',
        ];
        $this->saveModel = ContentSettings::findOne($conf0);
        $conf = $this->getCtypeConf();
        if ($this->saveModel) {
            if ($this->saveModel->configarray) {
                $conf = $this->saveModel->configarray; 
            }
        } else {
           $this->saveModel = new ContentSettings($conf0);
        }
        $this->model = new CommentContentTypeSettingsModel($conf);
    }  
    
    /**
     * @inheritdoc
     */     
    public function adjustModels()
    {
        $this->saveModel->nid = $this->owner->nid;
    }
    
    /**
     * @inheritdoc
     */ 
    public function saveModel()
    {
        if ($this->model) {  
            $this->saveModel->configarray = [
                'howmany' => $this->model->howmany,
                'type' => $this->model->type,
                'opened' => $this->model->opened,
            ];
            $this->saveModel->save(false);
        }
    }            

    /**
     * @inheritdoc
     */     
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'handlerBeforeDelete',
        ];
    } 
    
    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */
    public function handlerBeforeDelete($event)
    {
        if (!$this->model) {
            $this->initModel();
        }
        if ($this->model) {
            $this->saveModel->delete();
        } 
        // deleting comments from comments table
        $def = Yii::$container->getDefinitions();
        if (isset($def['commentsModelClass'])) {
            $mclass = $def['commentsModelClass']['class'];
            $mclass::deleteAll([
                'pagetype' => $this->owner->contentType,
                'nid' => $this->owner->nid,
            ]);
            CommentStatistic::deleteAll([
                'pagetype' => $this->owner->contentType,
                'nid' => $this->owner->nid,
            ]);            
        }        
        // end deleting comments from comments table
    }
    
    /**
     * Returns a configuration for comments for a content type
     * 
     * @return array
     */
    public function getCtypeConf()
    {
        $id = $this->owner->contentType;
        $pmanager = Yii::$app->get('pageManager');
        $ctypes = $pmanager->getContentTypes(); 
        $ctype = $ctypes[$id];
        $class = $ctype['contentSettingsModelClass'];
        $model = new $class();
        $model->initFieldsModel();  
        return $model->getCommentSettings();
    }
    
    /**
     * Returns comment settings for this node without fields initialization
     * 
     * @api
     * @return array
     * @see \mgrechanik\ctypes\article\views\main\view.php
     */
    public function getCommentSettings()
    {
        $mainModel = $this->owner;
        $conf0 = [
            'pagetype' => $mainModel->contentType,
            'nid' => $mainModel->nid,
            'what' => 'comments',
        ];
        if ($sett = ContentSettings::findOne($conf0)) {
            $res = $sett->configarray;
        } else {
            $res = ['howmany' => 10, 'type' => 1, 'opened' => 3];
        }
        $res['showExistedComm'] = (isset($res['opened'])) && ($res['opened'] != 3);
        $res['showFormComm'] = (isset($res['opened'])) && ($res['opened'] == 1);        
        return $res;
    }
}

