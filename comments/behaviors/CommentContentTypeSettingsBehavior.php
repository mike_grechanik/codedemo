<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\behaviors;

use mgrechanik\comments\models\fields\CommentContentTypeSettingsModel;
use yii\db\ActiveRecord;
use mgrechanik\cmscore\behaviors\FieldsBehavior;
use mgrechanik\cmscore\models\ContentSettings;

/**
 * Content type field behavior with settings for comments
 * 
 * @see \mgrechanik\ctypes\page\models\PageSettings
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class CommentContentTypeSettingsBehavior extends FieldsBehavior
{
    /**
     * @inheritdoc
     */     
    public $template = '@mgrechanik/comments/models/fields/fieldviews/commentsettingsctypeedit.php';
    
    /**
     * @var ContentSettings The model to save our settings
     */  
    protected $saveModel = null;

    /**
     * @inheritdoc
     */         
    public function initModel()
    {
        $mainModel = $this->owner;
        $conf0 = [
            'pagetype' => $mainModel->contentType,
            'nid' => $mainModel->nid,
            'what' => 'comments',
        ];
        $this->saveModel = ContentSettings::findOne($conf0);
        $conf = [];
        if ($this->saveModel) {
            if ($this->saveModel->configarray) {
                $conf = $this->saveModel->configarray; 
            }
        } else {
           $this->saveModel = new ContentSettings($conf0);
        }
        $this->model = new CommentContentTypeSettingsModel($conf);
    }  
    
    /**
     * @inheritdoc
     */       
    public function saveModel()
    {
        if ($this->model) {  
            $this->saveModel->configarray = [
                'howmany' => $this->model->howmany,
                'type' => $this->model->type,
                'opened' => $this->model->opened,
            ];
            $this->saveModel->save(false);
        }
    }            

    /**
     * Returns the settings for current content type.
     * 
     * Result resides in the values of this array
     *   $ps = new \mgrechanik\ctypes\page\models\PageSettings();
     *   $ps->initFieldsModel();
     *   var_dump($ps->getCommentSettings());
     *   array (size=2)
     *     'howmany' => 
     *     'type' => 
     *     'opened' =>
     * 
     * @api
     * @return array
     * @see \mgrechanik\comments\behaviors\CommentNodeSettingsBehavior::getCtypeConf()
     */
    public function getCommentSettings()
    {
        if (!$this->saveModel->isNewRecord) {
            $conf = $this->saveModel->configarray;
            return $conf;
        }
        return ['howmany' => 10, 'type' => 1, 'opened' => 1];
    }
    
}

