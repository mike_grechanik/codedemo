<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\behaviors;

use yii\db\ActiveRecord;
use mgrechanik\comments\models\CommentStatistic;
use mgrechanik\cmscore\service\SettingsManager;

/**
 * This behavior is for use together with the comment table
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class CommentTableBehavior extends \yii\base\Behavior
{
    /**
     * @var integer Default status for a new comment created
     * It is not used now. Have a look at [[beforeInsert]]
     * 0 - not published
     * 1 - published
     * 2 - deleted
     */
    public $defaultStatus = 1;

    /**
     * @var string Permission with which user's comment won't be needed pre moderation 
     */
    public $permissionForInitialPublished = 'core_see_basic_admin_list_pages';
    
    /**
     * @var integer|null The initial status before current update. 
     */
    protected $formerStatus = null;
    
    /**
     * @inheritdoc
     */  
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
        ];
    }     
    
    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */
    public function beforeInsert($event)
    {
        $ow = $this->owner;
        $ow->created = time();
        //$ow->status = $this->defaultStatus;
        $user = \Yii::$app->user;
        if (!$user->isGuest && $user->can($this->permissionForInitialPublished)) {
            $ow->status = 1;
        } else {
            $ow->status = SettingsManager::getSettingValue('comments_initial_status', 'val', $this->defaultStatus);
        }
        $this->setNewThread();
        if ($ow->status == 1) {
            $this->addToStatistic($ow->pagetype, $ow->nid);
        }
    }

    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */    
    public function afterFind($event)
    {
        $this->formerStatus = $this->owner->status;
    }  
    
    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */    
    public function beforeUpdate($event)
    {
        $this->owner->updated = time();
        if (!is_null($this->formerStatus)) {
            if ( ($this->formerStatus != 1) && ($this->owner->status == 1) ) {
                $this->addToStatistic($this->owner->pagetype, $this->owner->nid);
            }
            if ( ($this->formerStatus == 1) && ($this->owner->status != 1) ) {
                $this->removeFromStatistic($this->owner->pagetype, $this->owner->nid);
            }            
        }
    }   
    
    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */    
    public function beforeDelete($event)
    {
        $ow = $this->owner;  
        $this->removeFromStatistic($ow->pagetype, $ow->nid);
    }       
    
    /**
     * Setting the thread and level for the comments
     */
    protected function setNewThread()
    {
        $ow = $this->owner;
        $class = get_class($ow);
        $query = $class::find();
        $ato = (int) $ow->answerto;
        $prefix = '';
        $level = 0;
        if ($ato) {
            if ($parent = $class::findOne([
                'cid' => $ato,
                'pagetype' => $ow->pagetype,
                'nid' => $ow->nid,
                ])){
                $thread = $parent->thread;
                $prefix = $thread . '.';
                $level = $parent->level + 1;
            } else {
                $ato = 0;
            }
        }
        $objLast = $query
                ->where([
                    'answerto' => $ato, 
                    'pagetype' => $ow->pagetype,
                    'nid' => $ow->nid,                    
                    ])
                ->orderBy('thread desc')
                ->limit(1)
                ->one();
        if ($objLast) {
            $path = $objLast->thread;
            $marker = substr($path, strlen($prefix));
            $ow->thread = $prefix . $this->getIncreasedMarker($marker);
        } else {
            $ow->thread = $prefix . '000';
        }
        $ow->level = $level;
    }
    
    /**
     * Increases the marker
     * 
     * Example:
     * in  - 'a1y'
     * out - 'a1z'
     * 
     * @param string $marker the string which holds a number in 36 number notation
     * @return string
     */
    protected function getIncreasedMarker($marker)
    {
        $number10 = base_convert($marker, 36, 10);
        $number10 = intval($number10) + 1;
        $numberZ = base_convert($number10, 10, 36);
        $numberZ = str_pad($numberZ, 3, '0', STR_PAD_LEFT); 
        return $numberZ;
    }
    
    /**
     * Adds to statistic
     * 
     * @param integer $pagetype
     * @param integer $nid
     */
    protected function addToStatistic($pagetype, $nid)
    {
        $conf = [
            'pagetype' => $pagetype,
            'nid' => $nid,
        ];
        if ($stat = CommentStatistic::findOne($conf)) {
            $stat->comment_count = $stat->comment_count + 1;
        } else {
            $conf['comment_count'] = 1;
            $stat = new CommentStatistic($conf);
        }
        $stat->save(false);        
    }
    
    /**
     * Removes from statistic
     * 
     * @param integer $pagetype
     * @param integer $nid
     */
    protected function removeFromStatistic($pagetype, $nid)
    {
        $conf = [
            'pagetype' => $pagetype,
            'nid' => $nid,
        ];
        if ($stat = CommentStatistic::findOne($conf)) {
            if ($comment_count = ($stat->comment_count - 1)) {
                $stat->comment_count = $comment_count;
                $stat->save(false);
            } else {
                $stat->delete();
            }
        }         
    }
}

