$(document).ready(function(){
    var $formBlock = $('div.comment-form');
    var $answerToInput = $('#comment-answerto');
    var $acomment0 = $('a.comment0');
    $acomment0.hide();
    $('a.answerto').click(function(){
        var $this = $(this);
        $answerToInput.val($this.attr('name'));
        $parent = $this.closest('article.comment-item');
        $formBlock.insertAfter($parent);
        $formBlock.css('margin-left', (parseInt($parent.css('margin-left')) + 20) + 'px');
        $acomment0.show();
        $('html, body').animate({ scrollTop: $parent.offset().top }, 500);
        return false;
    });
    $acomment0.click(function(){
        $answerToInput.val(0);
        $formBlock.insertAfter($('section.comment-wrapper'));
        $formBlock.css('margin-left', '0px');
        $acomment0.hide();
        $('html, body').animate({ scrollTop: $formBlock.offset().top }, 500);
        return false;
    });
});	

