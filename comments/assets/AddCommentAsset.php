<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\assets;

use yii\web\AssetBundle;

/**
 * Asset to add comment form, when the comments is hierarchical, we need it to move comment form.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class AddCommentAsset extends AssetBundle
{

    public $sourcePath = '@mgrechanik/comments/assets/res';

	public $js = [
        'js/addcomment.js',
	];
    
	public $css = [
	    'css/addcomment.css',
	];    

	public $depends = [
		'yii\web\JqueryAsset',
	];

}
