<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\controllers;

use Yii;
use mgrechanik\cmscore\base\AdminController;
use yii\base\InvalidRouteException;
use mgrechanik\user\models\basic\PermissionInterface;
use mgrechanik\cmscore\helpers\Common;
use yii\filters\AccessControl;
use yii\helpers\Html;
use mgrechanik\comments\models\StatusFilter;

/**
 * Comments admin tasks.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class TaskController extends AdminController implements PermissionInterface
{
    /**
     * @var string Class  of the comment model
     */
    protected $mclass = '';
    
    /**
     * @inheritdoc
     */  
    public function init()
    {
        parent::init();
        $def = Yii::$container->getDefinitions();
        if (isset($def['commentsModelClass'])) {
            $this->mclass = $def['commentsModelClass']['class'];
        }
    }
    
    /**
     * @inheritdoc
     */      
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view-comments'],
                        'roles' => ['core_see_basic_admin_list_pages'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['edit-comment', 'delete-comment', 'new-comments', 'redirect-to-comment'],
                        'roles' => ['comments_manage_update_delete_comments'],
                    ],
                ],
            ],
        ];
    }        

    /**
     * Admin page of viewing the comments.
     * 
     * @param integer $pagetype
     * @param integer $nid
     * @return mixed
     */
    public function actionViewComments($pagetype, $nid)
    {
        $this->setAdminPath($pagetype, $nid, '');
        Yii::$app->session->remove('redirect-to-new-comments');
        return $this->render('viewcomments', [
            'pagetype' => $pagetype,
            'nid' => $nid,
        ]);
    }
    
    /**
     * Admin page of editing the comment.
     * 
     * @param integer $cid
     * @return mixed
     * @throws InvalidRouteException
     */
    public function actionEditComment($cid)
    {
        $class = $this->mclass;
        $model = $class::findOne($cid);
        $model->scenario = 'edit';
        if (!$model) {
           throw new InvalidRouteException('This Page is not found'); 
        } 
        $this->setAdminPath($model->pagetype, $model->nid, '-edit', $cid);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //redirect
            Yii::$app->session->setFlash('form-successful-message', Yii::t('cmscore', 'The comment has been changed'));
            if (!($url = $this->getIsUrlToNewCommentsPage($cid))) {
                $url = ['view-comments', 'pagetype' => $model->pagetype, 'nid' => $model->nid, '#' => 'comment-' . $model->cid];
                $page = $this->getRedirectPage($model, $this->module->amountOnEditingPage, 1, 1);
                if ($page > 1) { 
                    $url['page'] = $page;
                }                           
            }
            return $this->redirect($url);
        }
        return $this->render('editcomment', [
            'model' => $model,
        ]);
    }
    
    /**
     * Admin page of deleting the comment.
     * 
     * @param integer $cid
     * @return mixed
     * @throws InvalidRouteException
     */    
    public function actionDeleteComment($cid)
    {
        $class = $this->mclass;
        $model = $class::findOne($cid);
        if (!$model) {
           throw new InvalidRouteException('This Page is not found'); 
        }
        $this->setAdminPath($model->pagetype, $model->nid, '-delete', $cid);
        if (isset($_POST['Deletenode'])) {
            $url = ['view-comments', 'pagetype' => $model->pagetype, 'nid' => $model->nid];
            if ($atocount = $class::find()->where(['answerto' => $cid])->count()) {
                $model->status = 2;
                $model->save(false);
                $url['#'] = 'comment-' . $model->cid;
                $page = $this->getRedirectPage($model, $this->module->amountOnEditingPage, 1, 1);
                if ($page > 1) { 
                    $url['page'] = $page;
                }                                           
            } else {
               $model->delete(); 
            }
            Yii::$app->session->setFlash('form-successful-message', Yii::t('cmscore', 'The comment has been deleted'));
            if ($url2 = $this->getIsUrlToNewCommentsPage($cid)) {
                $url = $url2;
            }
            return $this->redirect($url);
        }
        $name = $model->name;
        if ((int)$model->uid) {
            $class = Yii::$app->getModule('users')->userClass;
            if ($user = $class::findOne($model->uid)) {
                $name = $user->usertitle;
            }
        }        
        return $this->render('delete', ['comment' => $model, 'name' => $name]);
    }
    
    /**
     * Display all last comments.
     * 
     * @param integer $status
     * @return mixed
     */
    public function actionNewComments($status = -1)
    {
        $this->initializeAdminPage('comments/newones');
        if (!in_array($status, [-1, 0])) {
            $status = -1;
        }
        $plist = new StatusFilter();
        if (isset($_POST['filter_button'])) {
            $plist->load(Yii::$app->request->post());
            if ($plist->validate()) {
                return $this->redirect(['', 'status' => $plist->status]);
            }
        }
        $plist->status = (int) $status;
        Yii::$app->session->set('redirect-to-new-comments', [
            'new-comments',
            'status' => $status,
            'page' => (isset($_GET['page'])) ? intval($_GET['page']) : 1,
        ]);

        return $this->render('allcomments', ['status' => $status, 'filter' => $plist]);
    }
    
    /**
     * If we are managing comments from 'new comments' page it has to return
     * return url to it.
     * 
     * @param integer $cid
     */
    protected function getIsUrlToNewCommentsPage($cid)
    {
        $val = Yii::$app->session->get('redirect-to-new-comments');
        if (!empty($val) && is_array($val)) {
            $val['#'] = 'comment-' . $cid;
        }
        return $val;
    }

    /**
     * Redirects user to a page of editing or viewing some certain comment
     * 
     * @param integer $cid
     * @param string $where Where to redirect. editing ('back') or viewing('front')
     * @return mixed
     */
    public function actionRedirectToComment($cid = -1, $where = 'front')
    {
        $res = null;
        $page = 1;
        $class = $this->mclass;
        $model = $class::findOne($cid);
        if ($model) {
           $pmanager = Yii::$app->get('pageManager');
           $ctypes = $pmanager->getContentTypes();
           if (isset($ctypes[intval($model->pagetype)]['contentModelClass'])) {
               $nodeclass = $ctypes[intval($model->pagetype)]['contentModelClass'];
               $node = $nodeclass::findOne($model->nid);
           }
           if ($where == 'front') {
               if ($node->hasMethod('getCommentSettings')) {
                   $csett = $node->getCommentSettings();
                   $page = $this->getRedirectPage($model, $csett['howmany'], ($csett['type'] == 2), 0);
                   $params = ['#' => 'comment-' . $model->cid, 'cross' => 'front', 'lang' => $node->lang];
                   if ($page > 1) { 
                       $params['page'] = $page;
                   }
                   $innerUrl = str_replace('<nid>', $node->nid, $node->innerUrlTemplate);
                   $res = Common::urlTo($innerUrl, $params);                    
               }
           } elseif ($where == 'back') {
                $res = [
                    'view-comments', 
                    'pagetype' => $model->pagetype,
                    'nid' => $model->nid,
                    '#' => 'comment-' . $model->cid];
                $page = $this->getRedirectPage($model, $this->module->amountOnEditingPage, 1, 1);
                if ($page > 1) { 
                    $res['page'] = $page;
                }               
           } 
        }  
        if (!$res) {
            $res = ['/'];
        }
        return $this->redirect($res);
    }  
    
    /**
     * Returns number of the page we need to redirect to.
     * 
     * @param object $comment
     * @param integer $countPerPage
     * @param integer $isHierarchical
     * @param integer $isForAdminEdit
     * @return integer Number of the page
     */
    protected function getRedirectPage($comment, $countPerPage, $isHierarchical = 0, $isForAdminEdit = 0)
    {
        $status = [1];
        if ($isHierarchical) {
            $status[] = 2;
        }
        if ($isForAdminEdit) {
            $status[] = 0;
        }
        $class = $this->mclass;
        $query = $class::find();
        $query->where([
            'pagetype' => $comment->pagetype, 
            'nid' => $comment->nid,
            'status' => $status,
        ]);  
        $query->andWhere(['<', 'thread', $comment->thread]);
        // amount of comments before this one
        $count = $query->count();
        return floor($count/$countPerPage) + 1;
    }    

    /**
     * Helper to set a current admin page.
     * 
     * @param integer $nodetype
     * @param integer $nodeid
     * @param string $what
     */
    protected function setAdminPath($nodetype, $nodeid, $what = '/???', $commentid = null)
    {
        $types = Yii::$app->get('pageManager')->getContentTypes();
        if (isset($types[$nodetype])) {
            $type = $types[$nodetype];
            $innername = $type['innername'];
            $nodeClass = $type['contentModelClass'];
            $this->getView()->params['breadcrumbs'][] = ['label' => Yii::t('adminnames', 'All pages'), 'url' => ['/admin/admin/page/list']];
            $this->getView()->params['breadcrumbs'][] = ['label' => Yii::t('adminnames', $type['name']), 'url' => ['/admin/admin/page/list', 'type' => $nodetype]];
            if ($node = $nodeClass::findOne($nodeid)) {
                $tokens =
                    [
                        'nodetype' => $nodetype,
                        'nodeid' => $nodeid,
                        'nodename' => $node->title,
                        'nodelang' => $node->lang,
                    ];
                if ($commentid) {
                    $tokens['commentid'] = $commentid;
                }
                $this->initializeAdminPage('pages/all/' . $innername . '/{id}/editcomments' . $what,
                    $tokens,
                    [
                        'nodeobject' => $node,
                    ]
                );
                $prefix = '';
                if ($what == '-edit') {
                    $prefix = Yii::t('cmscore', 'Edit the comment for:');
                } elseif ($what == '-delete') {
                    $prefix = Yii::t('cmscore', 'Delete the comment for:');
                } elseif ($what == '') {
                    $prefix = Yii::t('cmscore', 'Edit the comments for:');
                }
                $this->getView()->params['breadcrumbs'][] = $title = $prefix . ' ' . $node->title;
                $this->getView()->title = Html::encode($title);
            }
        }
    }
    
    /**
     * @inheritdoc
     */         
    public function getAdminPages()
    {
        $res = [
            'comments' => [
                'text' => 'Comments',
                'isCategory' => 1,
            ],
            'comments/newones' => [
                'text' => 'New comments',
                'url' => ['/comments/task/new-comments'],
                'title' => 'New comments',
                'permissions' => ['comments_manage_update_delete_comments'],
            ],            
        ]; 
        $pmanager = Yii::$app->get('pageManager');
        $types = $pmanager->getContentTypes();
        unset($types[0]);
        foreach ($types as $k => $v) {
            $res['pages/all/' . $v['innername'] . '/{id}/editcomments'] = [
                'text' => 'Edit comments',
                'url' => ['/comments/task/view-comments', 'pagetype' => '{nodetype}', 'nid' => '{nodeid}'] ,
                'isTab' => 1,
                'dynamic_change_callback' => 'mgrechanik\comments\helpers\Service::checkEditCommentsActive',
                'is_show_dynamic_link_callback' => 'mgrechanik\comments\helpers\Service::isShowEditComments',
                'permissions' => ['comments_manage_update_delete_comments'],                
            ];
            $res['pages/all/' . $v['innername'] . '/{id}/editcomments-edit'] = [
                'text' => 'Edit comment',
                'url' => ['/comments/task/edit-comment', 'cid' => '{commentid}'] ,
                'isTab' => 1,
                'tabLevel' => 2,
                'is_show_dynamic_link_callback' => 'mgrechanik\comments\helpers\Service::isShowEditDeleteCommentTab',
                'permissions' => ['comments_manage_update_delete_comments'],                
            ];            
            $res['pages/all/' . $v['innername'] . '/{id}/editcomments-delete'] = [
                'text' => 'Delete comment',
                'url' => ['/comments/task/delete-comment', 'cid' => '{commentid}'] ,
                'isTab' => 1,
                'tabLevel' => 2,
                'is_show_dynamic_link_callback' => 'mgrechanik\comments\helpers\Service::isShowEditDeleteCommentTab',
                'permissions' => ['comments_manage_update_delete_comments'],                
            ];                        
        }
        return $res;
    }  
    
    /**
     * @inheritdoc
     */         
    public function getPermissions()
    {
        return [
            'comments_add_new_comment' => [
                'description' => 'Allow to add a new comment',
            ],
            'comments_manage_update_delete_comments' => [
                'description' => 'Manage comments',
            ],            
            'category' => 'Comments',
        ];
    }      
    
}
