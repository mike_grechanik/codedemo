<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\models\fields;

/**
 * This is the model to be used by node and content type fields behaviors
 * 
 * @see \mgrechanik\comments\behaviors\CommentNodeSettingsBehavior
 * @see \mgrechanik\comments\behaviors\CommentContentTypeSettingsBehavior
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class CommentContentTypeSettingsModel extends \yii\base\Model 
{
    /**
     * Constants for values of statuses of the comments settings
     */
    const FLAT_COMMENT_TYPE = 1;
    const HIERARCHICAL_COMMENT_TYPE = 2;
    
    const COMMENT_OPENED = 1;
    const COMMENT_CLOSED = 2;
    const COMMENT_HIDDEN = 3;

    /**
     * @var integer Amount of comments at one page
     */
    public $howmany = 10;
    
    /**
     * @var integer The type of the comments 
     */
    public $type = 1;

    /**
     * @var integer Status ob being opened/closed...
     */
    public $opened = 1;

    /**
     * @inheritdoc
     */     
    public function rules()
    {
        return [
            ['howmany', 'integer', 'min' => 1],
            ['type', 'in', 'range' => [self::FLAT_COMMENT_TYPE, self::HIERARCHICAL_COMMENT_TYPE]],
            ['opened', 'in', 'range' => [self::COMMENT_CLOSED, self::COMMENT_OPENED, self::COMMENT_HIDDEN]],
        ];
    }

    /**
     * @inheritdoc
     */     
    public function attributeLabels()
    {
        return [
            'howmany' => \Yii::t('cmscore', 'Amount items per page'),
            'type' => \Yii::t('cmscore', 'Type of comments'),
            'opened' => \Yii::t('cmscore', 'Comments state'),
        ];
    }    
    
    /**
     * Gets the list of comment types. Helper to display input.
     * 
     * @return array
     */
    public static function getOptionsType()
    {
        return [
            self::FLAT_COMMENT_TYPE => \Yii::t('cmscore', 'Flat'),
            self::HIERARCHICAL_COMMENT_TYPE => \Yii::t('cmscore', 'Hierarchical'),
        ];
    }
    
    /**
     * Gets the list of comment statuses. Helper to display input.
     * 
     * @return array
     */    
    public static function getOptionsOpened()
    {
        return [
            self::COMMENT_OPENED => \Yii::t('cmscore', 'Opened'),
            self::COMMENT_CLOSED => \Yii::t('cmscore', 'Closed'),
            self::COMMENT_HIDDEN => \Yii::t('cmscore', 'Hidden'),
        ];
    }
}


