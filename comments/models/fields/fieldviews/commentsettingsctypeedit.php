<?php
use mgrechanik\comments\models\fields\CommentContentTypeSettingsModel;
?>
<?= $form->field($model, 'opened')->dropDownList(CommentContentTypeSettingsModel::getOptionsOpened()); ?>
<?= $form->field($model, 'howmany')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'type')->dropDownList(CommentContentTypeSettingsModel::getOptionsType()); ?>
<?= '<p>' . Yii::t('cmscore', 'Pay attention that these default comment settings will be applyed only for new created posts') . '</p>' ?>

