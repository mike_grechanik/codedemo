<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\models;

use Yii;
use yii\base\Model;

/**
 * This model works as a filter for admin list of pages
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class StatusFilter extends Model
{
    
    /**
     * @var integer The publishing status of the page. -1 == "all" types
     */    
    public $status = -1;
    
    
    /**
     * Getting the list of publishing statuses for a filter.
     * 
     * @return array
     */    
    public function getStatuses()
    {
        return [
            -1 => Yii::t('cmscore', 'All'),
            0 => Yii::t('cmscore', 'Not published (waits for moderation)'),
        ];
    }

    /**
     * @inheritdoc
     */     
    public function rules()
    {
        $statuses = array_keys($this->getStatuses());
        return [
            [['status'], 'in', 'range' => $statuses],
        ];
    }
    
    /**
     * @inheritdoc
     */      
    public function attributeLabels()
    {
        return [
            'status' => Yii::t('cmscore', 'Status'),
        ];
    }
    
}

