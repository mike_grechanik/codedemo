<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\models;

use Yii;

/**
 * This is the model class for table "{{%comment_statistic}}".
 * 
 * This table keeps information about number of comments per node.
 * It is managed by [[CommentTableBehavior]]
 * @see \mgrechanik\comments\behaviors\CommentTableBehavior
 * @property integer $pagetype
 * @property integer $nid
 * @property integer $comment_count
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class CommentStatistic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment_statistic}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pagetype', 'nid', 'comment_count'], 'required'],
            [['pagetype', 'nid', 'comment_count'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pagetype' => 'Pagetype',
            'nid' => 'Nid',
            'comment_count' => 'Comment Count',
        ];
    }
}
