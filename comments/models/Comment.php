<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\models;

use Yii;

/**
 * This is the model class for table "{{%comment}}".
 *
 * @property integer $cid
 * @property integer $pagetype
 * @property integer $nid
 * @property integer $answerto
 * @property integer $uid
 * @property string $subject
 * @property string $body
 * @property string $cache
 * @property string $name
 * @property string $mail
 * @property string $homepage
 * @property integer $status Look at CommentTableBehavior
 * @property string $thread
 * @property string $level
 * @property integer $created
 * @property integer $updated
 * @property string $hostname
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @var boolean Whether the comments are hierarchical 
     */
    protected $isHierarchical = false;

    /**
     * Setter for [[isHierarchical]]
     * 
     * @param boolean $val
     */
    public function setIsHierarchical($val)
    {
        $this->isHierarchical = $val;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }
    
    /**
     * @inheritdoc
     */    
    public function behaviors()
    {
        return [
            'comment' => [
                'class' => 'mgrechanik\comments\behaviors\CommentTableBehavior',
            ]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $res = [
            [['body'], 'required'],
            [['body'], 'string'],
            [['status'], 'in', 'range' => [0, 1, 2], 'on' => 'edit'],
        ];
        if ($this->isHierarchical) {
            $res[] = [['answerto'], 'safe', 'on' => 'default'];
        }
        if (!$this->uid) {
            $res[] = [['name'], 'required', 'on' => ['default', 'edit']];
            $res[] = [['name'], 'string', 'max' => 60, 'on' => ['default', 'edit']];
        }
        return $res;
    }
    
    /**
     * @inheritdoc
     */    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->cache = '';
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cid' => 'Cid',
            'pagetype' => 'Pagetype',
            'nid' => 'Nid',
            'answerto' => 'Answerto',
            'uid' => 'Uid',
            'subject' => 'Subject',
            'body' => Yii::t('cmscore', 'Comment'),
            'name' => Yii::t('cmscore', 'Name'),
            'mail' => 'Mail',
            'homepage' => 'Homepage',
            'status' => Yii::t('cmscore', 'Status'),
            'thread' => 'Thread',
            'created' => 'Created',
            'updated' => 'Updated',
            'hostname' => 'Hostname',
        ];
    }
}
