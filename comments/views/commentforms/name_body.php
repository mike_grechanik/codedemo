<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="comment-form">
    <?php $form = ActiveForm::begin(); ?>
<?php if ($model->isAttributeSafe('name')) { ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
<?php } ?>
    <?= $form->field($model, 'body')->textarea() ?>
    
    <?php if ($isHierarchical) {
        print $form->field($model, 'answerto')->hiddenInput();
    }  ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('cmscore', 'Add new comment') , ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

