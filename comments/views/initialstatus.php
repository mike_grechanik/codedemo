<?= $form->field($model, 'val')
        ->dropDownList([
            1 => Yii::t('cmscore', 'Published'),
            0 => Yii::t('cmscore', 'Not published (waits for moderation)'),
        ], ['disabled' => !$model->isAttributeSafe('val')])
        ->hint($model->hint . (($model->isAttributeSafe('val')) ? '' : (($model->hint) ? '<br>' : '') . $safeMessage)) 
?>