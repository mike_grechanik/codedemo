<?php

use yii\helpers\Html;
?>
<h1><?= $this->title ?></h1>
<?= Html::beginForm(); ?>
Do you really want to delete comment from "<?= Html::encode($name) ?>" ?
<br><br>
<?= Html::submitButton(Yii::t('cmscore', 'Delete'), ['name' => 'Deletenode', 'class' => 'btn btn-primary']);?>
<?= Html::endForm(); ?>


