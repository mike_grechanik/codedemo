<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mgrechanik\cmscore\models\Tformats;

\yii\gii\GiiAsset::register($this);

$format = Yii::$app->getModule('comments')->commentTextFormat;
$formatHint = $this->renderFile('@mgrechanik/cmscore/admin/views/admin/page/getformathelp.php', [
    'fobj' => Tformats::findOne($format),
]);
?>
<h1><?= $this->title ?></h1>
<div class="comment-form default-view">

    <?php $form = ActiveForm::begin(); ?>
<?php if ($model->isAttributeSafe('name')) { ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
<?php } else {
    $class = Yii::$app->getModule('users')->userClass;
    if ($user = $class::findOne($model->uid)) {
        print Html::tag('div', Yii::t('cmscore', 'User') . ' - ' . Html::encode($user->usertitle));
    }
} ?>
    <?= $form->field($model, 'body')->textarea()->hint($formatHint) ?>
    
    <?= $form->field($model, 'status')->dropDownList([
            0 => Yii::t('cmscore', 'not published'), 
            1 => Yii::t('cmscore', 'published'), 
            2 => Yii::t('cmscore', 'deleted'), 
         ]) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('cmscore', 'Edit comment') , ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


