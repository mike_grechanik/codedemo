<?php

use yii\helpers\Html;
?>
<h1><?= $this->title ?></h1>
<?= Html::beginForm(); ?>
Вы точно хотите удалить комментарий от "<?= Html::encode($name) ?>" ?
<br><br>
<?= Html::submitButton(Yii::t('cmscore', 'Delete'), ['name' => 'Deletenode', 'class' => 'btn btn-primary']);?>
<?= Html::endForm(); ?>


