<?php
use mgrechanik\comments\widgets\ShowNewComments;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$statuses = $filter->getStatuses();
$this->title = Yii::t('cmscore', 'All last comments');
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?=$this->title?></h1>
    <div>
        <?php $form = ActiveForm::begin(['enableClientValidation' => false]) ?>
        <?= $form->field($filter, 'status')->dropDownList($statuses) ?>
        <?= Html::submitButton(Yii::t('user', 'Filter'), ['class' =>  'btn btn-success', 'name' => 'filter_button']) ?>
        <?php ActiveForm::end(); ?> 
    </div>
    <br>
<div class="new-comments">    
    <?php 
        print ShowNewComments::widget([
            'status' => $status,
            'indentCoefficient' => 0,
            'isShowComments' => true,
            'countPerPage' => $this->context->module->amountOnEditingPage,
            'isShowForm' => false,
        ]);
 ?>  
</div>
