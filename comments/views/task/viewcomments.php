<?php 
use mgrechanik\comments\widgets\ShowCommentsList;
?>
<h1><?= $this->title ?></h1>
<?= ShowCommentsList::widget([
    'contentType' => $pagetype,
    'nid' => $nid,
    //'node' => $node,
    'isShowComments' => true,
    'isHierarchical' => true,
    'countPerPage' => $this->context->module->amountOnEditingPage,
    'isForAdminEdit' => Yii::$app->user->can('comments_manage_update_delete_comments'), //true,
    'isShowForm' => false,
]) ?>