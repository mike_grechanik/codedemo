<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments;

/**
 * The module to add comments to a site.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Module extends \mgrechanik\cmscore\base\Module
{
    /**
     * @inheritdoc
     */        
    public $controllerNamespace = 'mgrechanik\comments\controllers';
    
    /**
     * @var string  Inner name of the text format which is associated with comments
     * Attention:
     * If you are changing this format or you are changing the format itself
     * you have to clean 'cache' column of the comments table for rebuilding comment's caches
     */
    public $commentTextFormat = 'plain_text';
    
    /**
     * @var integer Amount of comments on comments editing page
     */
    public $amountOnEditingPage = 100;
    
    /**
     * @var string Message of comment being submitted and waiting for approval
     * Translate group is 'endsite' 
     */
    public $messageCommentWaitsApproval = 'Your comment has been added and will be published after verification by moderator';

    /**
     * @inheritdoc
     */        
    public function getMetaData()
    {
        return [
            'diffContentTypes' => [
               'mgrechanik\comments\helpers\Service::contentTypesMetaChange',
            ],
           'filters' => [
               'jbbcode' => [
                   'name' => 'bb codes',
                   'callback' => 'mgrechanik\comments\helpers\Service::filterJbbCode',
                   'isPositional' => 1,
                   'formatHelpText' => '<ul><li><b>[b]</b>bold text<b>[/b]</b></li><li><b>[i]</b>italik text<b>[/i]</b></li><li><b>[u]</b>underline text<b>[/u]</b></li><li><b>[img]</b>src address<b>[/img]</b></li><li><b>[url=</b>address<b>]</b>link text<b>[/url]</b></li><li><b>[code]</b>code text<b>[/code]</b></li><li><b>[quote]</b>quote text<b>[/quote]</b></li></ul>',
               ],  
           ],
           'blockTypes' => [
                'core_commentblock' => [
                    'name' => 'Comments Block',
                    'core_callback' => 'mgrechanik\comments\btypes\commentblock\Callback::run',
                    'settingsmodel' => [
                        'class' => 'mgrechanik\comments\btypes\commentblock\Settings',
                    ],
                    'blockmodel' => [
                        'admintitle' => 'Comments Block',
                        'iscacheable' => 0,
                    ]                    
                ],               
           ], 
           'variables' => [
               'comments_initial_status' => [
                   'ignoreLang' => 1,
                   'editPermission' => 'comments_manage_update_delete_comments',
                   'modelClass' => 'mgrechanik\cmscore\models\variables\DynamicModel',
                   'editTemplate' => '@mgrechanik/comments/views/initialstatus.php',
                   'helpText' => 'Initial status of new comments posted',
                   'classConfig' => [
                       'label' => 'Initial status of new comments posted',
                       'rules' => [
                           ['val', 'in', 'range' => [0,1]],
                           ['val', 'required'],
                       ],  
                   ],    
               ],                               
           ],    
        ];
    }     
}
