<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\helpers;

use Yii;
use mgrechanik\cmscore\service\AdminPagesManager;

/**
 * Helper to work together with moderating comments functionality. 
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Service
{
    /**
     * @var array Associative array of amount of comments per nodes
     * Format: [nodetype_nid => count] 
     *   array (size=3)
     *     '2_5' => string '6' (length=1)
     *     '2_10' => string '1' (length=1)
     *     '2_19' => string '1' (length=1)
     */
    protected static $commentscount = null;

    /**
     * Fills [[commentscount]] with the data about amount of comments per nodes.
     * 
     * @param array $pagetypenids It's format: [[pagetype, nid],... ]
     * @return array Returns [[commentscount]]
     */
    protected static function getCommentCounts($pagetypenids)
    {
        if (self::$commentscount === null) {
            $def = Yii::$container->getDefinitions();
            if (isset($def['commentsModelClass'])) {
                $mclass = $def['commentsModelClass']['class'];
                $expr1 = new \yii\db\Expression('COUNT(cid) AS count');
                $all = $mclass::find()
                        ->select(['pagetype', 'nid', $expr1])
                        ->where(['in', ['pagetype', 'nid'], $pagetypenids])
                        ->groupBy('pagetype,nid')
                        ->asArray()
                        ->all();
                $allend = [];
                foreach ($all as $val) {
                    $allend[$val['pagetype'] . '_' . $val['nid']] = $val['count'];
                }
                self::$commentscount = $allend;
            }    
        }
        return self::$commentscount;
    }
    
    /**
     * We are checking if there are comments for nodes.
     * 
     * Callback to 'adminLinksPermissionCallbacks' for node types metadata
     * 
     * @param integer $nodetype
     * @param integer $nodeid
     * @param array $pagetypenids
     * @return boolean
     */
    public static function isShowCommentEditLinkInList($nodetype, $nodeid, $pagetypenids = [])
    {
        $user = Yii::$app->user;
        if ($user->can('comments_manage_update_delete_comments')) {
            $counts = self::getCommentCounts($pagetypenids);
            if (isset($counts[$nodetype . '_' . $nodeid])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Changing the existed content types by adding context manage comment link.
     * 
     * Implementation of 'diffContentTypes' for a content type metadata.
     * 
     * @param array $ctypes
     * @return array
     */
    public static function contentTypesMetaChange($ctypes)
    {
        foreach ($ctypes as $k => $v) {
            if ($k === 0) {
                continue;
            }
            $ctypes[$k]['adminLinks']['Edit comments'] = '/comments/task/view-comments?pagetype=<pagetype>&nid=<nid>';
            $ctypes[$k]['adminLinksPermissionCallbacks']['Edit comments'] = 'mgrechanik\comments\helpers\Service::isShowCommentEditLinkInList';
        }
        return $ctypes;
    }

    /**
     * Implementation of 'is_show_dynamic_link_callback' for AdminPagesInterface.
     * 
     * @return boolean
     */
    public static function isShowEditComments()
    {
        if (isset(AdminPagesManager::$tokens['nodetype'])){
            $def = Yii::$container->getDefinitions();
            if (isset($def['commentsModelClass'])) {
                $mclass = $def['commentsModelClass']['class'];
                if ($count = $mclass::find()->where([
                    'pagetype' => AdminPagesManager::$tokens['nodetype'],
                    'nid' => AdminPagesManager::$tokens['nodeid'],
                ])->count()) {
                    return true;
                }
            }
            return false; 
        } else {
            return false;
        }
    }   
    
    /**
     * Implementation of 'is_show_dynamic_link_callback' for AdminPagesInterface.
     * 
     * @return boolean
     */
    public static function isShowEditDeleteCommentTab()
    {
        if (isset(AdminPagesManager::$tokens['commentid'])) {
            return true;
        }
        return false;
    }      
    
    /**
     * Checking whether we are at one of the manage comment pages.
     * 
     * Implementation of 'dynamic_change_callback' callback for
     * AdminPagesInterface.
     * 
     * @param array $node
     * @return array
     */
    public static function checkEditCommentsActive($node)
    {
        if (isset(AdminPagesManager::$tokens['commentid'])) {
            $node['isActive'] = true;
        }
        return $node;
    } 
    
    /**
     * Implemantation of the callback function for filters (from text formats)
     * 
     * @param string $text
     * @param array $config
     * @param array $flags
     * @return string
     */
    public static function filterJbbCode($text, $config, &$flags)
    {
        $parser = new \JBBCode\Parser();
        $parser->addCodeDefinitionSet(new \JBBCode\DefaultCodeDefinitionSet());
        $builder = new \JBBCode\CodeDefinitionBuilder('code', '<code>{param}</code>');
        $parser->addCodeDefinition($builder->build());        
        $builder = new \JBBCode\CodeDefinitionBuilder('quote', '<blockquote>{param}</blockquote>');
        $parser->addCodeDefinition($builder->build());                
        $parser->parse($text);
        return $parser->getAsHtml();
        return $text;
    }    
    
    /**
     * Returns whether the current page is the node one and the node has comments associated
     * 
     * @return boolean 
     */
    public static function isNodeWithCommentsPage()
    {
        $pmanager = Yii::$app->get('pageManager');
        $cdata = $pmanager->currentData;
        if (isset($cdata['node']['object'])) {
            $node = $cdata['node']['object'];
            if ($node->hasMethod('getCommentSettings')) {
                return true;
            }
        }
        return false;
    }
}

