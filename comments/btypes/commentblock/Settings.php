<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\btypes\commentblock;

use Yii;

/**
 * Settings block model for comments block
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Settings extends \mgrechanik\blocks\base\SettingsModel
{
    /**
     * @inheritdoc
     */      
    public $viewTemplate = '@mgrechanik/comments/btypes/commentblock/view/block.php';
    
    /**
     * @var template with comment form 
     */
    public $commentModelTemplate  = '@mgrechanik/comments/views/commentforms/name_body.php';
    
    /**
     * @inheritdoc
     */      
    protected $editTemplate = '@mgrechanik/comments/btypes/commentblock/view/settingsedit.php';
    
    /**
     * @var integer Ident between levels. Model property.
     */      
    public $indent = 30;
    
    /**
     * @var string The format of the date of the comment created. Model property.
     * For php function date()
     */
    public $createdformat = 'd-m-Y H:i';

    /**
     * @inheritdoc
     */      
    public function rules()
    {
        $res = [
            [['indent', 'createdformat'], 'required'],
            [['indent'], 'integer', 'min' => 10, 'max' => 100],
            [['createdformat'], 'string'],
            [['createdformat'], 'match', 'pattern' => '/[<>]/i', 'not' => true],
        ];
        if (Yii::$app->user->can('programmingwork')) {
            $res[] = [['viewTemplate'], 'string'];
            $res[] = [['commentModelTemplate'], 'string'];
        }
        return $res;
    }
    
    /**
     * @inheritdoc
     */      
    public function attributeLabels()
    {
        return [
            'indent' => Yii::t('cmscore', 'Indent in pixels between comment levels (for hierarchical comments)'),
            'createdformat' => Yii::t('cmscore', 'Date-time format of comment created label. For php date() function.'),
            'viewTemplate' => Yii::t('cmsblock', 'ViewTemplate'),
            'commentModelTemplate' => Yii::t('cmscore', 'commentModelTemplate'),
        ];
    }    
}
