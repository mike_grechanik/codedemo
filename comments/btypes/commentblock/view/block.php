<?php
use mgrechanik\comments\widgets\ShowCommentsList;
$blockdata = null; 
$pmanager = Yii::$app->get('pageManager');
$cdata = $pmanager->currentData;
if (isset($cdata['node']['object'])) {
    $node = $cdata['node']['object'];
    if ($node->hasMethod('getCommentSettings')) {
        $csett = $node->getCommentSettings();
        $csett['showFormComm'] = $csett['showFormComm'] && Yii::$app->user->can('comments_add_new_comment');
        $blockdata = ShowCommentsList::widget([
            'commentModelTemplate' => $settings->commentModelTemplate,
            'indentCoefficient' => $settings->indent,
            'createdFormat' => $settings->createdformat,
            'contentType' => $node->contentType,
            'nid' => $node->nid,
            'node' => $node,
            'isShowComments' => $csett['showExistedComm'],
            'isHierarchical' => ($csett['type'] == 2),
            'countPerPage' => $csett['howmany'],
            'isShowForm' => $csett['showFormComm'],
        ]);
    } 
}
if ($blockdata) {
?>
<div class="block block-<?=$id . $cssclass ?>">
  <?php if ($title) { ?>  
    <h3><?=$title?></h3>
  <?php } ?>
    <?php 
        print $blockdata;
    ?>  
</div>
<?php } ?>