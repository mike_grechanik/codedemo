<?= $form->field($settingsmodel, 'indent')->textInput() ?>
<?= $form->field($settingsmodel, 'createdformat')->textInput() ?>
<?php if ($settingsmodel->isAttributeSafe('viewTemplate')) { ?>        
    <?= $form->field($settingsmodel, 'viewTemplate')->textInput() ?>
<?php } ?>
<?php if ($settingsmodel->isAttributeSafe('commentModelTemplate')) { ?>        
    <?= $form->field($settingsmodel, 'commentModelTemplate')->textInput() ?>
<?php } ?>
