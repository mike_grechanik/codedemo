<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\btypes\commentblock;

/**
 * Storage of callbacks for some core blocks
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Callback
{
    /**
     * Callback to form the comment block.
     * 
     * @param \mgrechanik\blocks\base\SettingsModel $settings Settings object for this block
     * @param \yii\web\View $view View of a layout
     * @param integer $id Block's bid
     * @param string $title The title of the block. '' if not exist or 'none'
     * @param string $cssclass
     * @return string The html of the block
     */
    public static function run($settings, $view, $id, $title, $cssclass = '')
    {
        return $view->renderFile($settings->viewTemplate, [
            'title' => $title,
            'id' => $id,
            'cssclass' => $cssclass,
            'settings' => $settings,
        ]);
    }
    
}

