<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\comments\widgets;

use yii\helpers\Html;
use mgrechanik\cmscore\helpers\Common;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use mgrechanik\comments\assets\AddCommentAsset;
use mgrechanik\cmscore\uploads\models\Files;
use yii\db\Query;
use Yii;

/**
 * Widget to display comments together with the comments form
 * 
 * Look at example how to add this widget:
 * \mgrechanik\ctypes\page\views\main\view.php
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class ShowCommentsList extends \yii\base\Widget
{
    /**
     * @var array Comments. Commonly filled automatically by contentType,nid
     */
    public $comments = [];

    /**
     * @var boolean
     * @see \mgrechanik\comments\behaviors\CommentNodeSettingsBehavior 
     */
    public $isShowComments = true;
    
    /**
     * @var boolean Whether to show add comment form 
     */
    public $isShowForm = true;

    /**
     * @var boolean Whether the comments are hierarchical 
     */
    public $isHierarchical = false;
    
    /**
     * @var The template of the comment. You can use the next tokens:@indent,@name,@body,@answerto,@created
     *  
     */
    public $templateComment =
'<article id="comment-@cid" class="comment-item" style="margin-left:@indentpx;">
	<header>
      <h3>@picture @name</h3>
      <div class="data"><time datetime="@created">@created</time></div>
    </header>
	<div class="content">@body</div>
	@answerto
</article>
';	    
    
    /**
     * @var integer The height of the avatar 
     */
    public $avatarHeight = 75;

    /**
     * @var integer Coefficient of indent between parent-child comments
     */
    public $indentCoefficient = 30;
    
    /**
     * @var string Format of the date created 
     */
    public $createdFormat = 'd-m-Y H:i';

    /**
     * @var string The name of the di alias for a comment model.
     * It is set in the main application config(like web.php) 
     */
    public $commentModelDiAlias = 'commentsModelClass';

    /**
     * @var string Aliased path to a template with the form for comment model 
     */
    public $commentModelTemplate = '@mgrechanik/comments/views/commentforms/name_body.php';

    /**
     * @var integer Content type of the page for which we show comments 
     */
    public $contentType = -1;
    
    /**
     * @var integer Number of the page for which  we show comments
     */
    public $nid = -1;
    
    /**
     * @var object Node object 
     */
    public $node = null;
    
    /**
     * @var integer Amount of comments to display per one page 
     */
    public $countPerPage = 10;
    
    /**
     * @var boolean Whether we are displaying comments for admin edit and user has the rights to moderate comments.
     * @see \mgrechanik\comments\views\task\viewcomments.php
     */
    public $isForAdminEdit = false;
    
    /**
     * @var \yii\base\Model Model of the comment
     */
    protected $formmodel = null;
    
    /**
     * @var object Paginator object 
     */
    protected $paginator = null;
    
    /**
     * @var array Preserving usernames 
     * Format: [uid => usertitle,...]
     */
    protected $usernames = [];

    /**
     * @inheritdoc
     */     
    public function init()
    {
        parent::init();
        if ($this->isShowForm) {
            $this->initModel();
        }
        if ($this->isShowComments && empty($this->comments)) {
            $this->initComments();
        } 
        if ($this->isShowForm && $this->isHierarchical) {
            AddCommentAsset::register($this->view);
        }
    }
    
    /**
     * @inheritdoc
     */ 
    public function run()
    {
        // show comments
        if ($this->isShowComments && !empty($this->comments)) {
            $textFormat = Yii::$app->getModule('comments')->commentTextFormat;
            $def = Yii::$container->getDefinitions();
            $modelClass = null;
            if (isset($def[$this->commentModelDiAlias])) {
                $modelClass = $def[$this->commentModelDiAlias]['class'];  
            }
            $usermodule = Yii::$app->getModule('users');
            $withAvatars = $usermodule->flagManageAvatars;
            $picture = '';
            print '<section id="comments" class="comment-wrapper">';
            foreach ($this->comments as $comment) {
                if ($this->isForAdminEdit){
                    $status = (int) $comment['status'];
                    $statuses = [Yii::t('cmscore', 'not published'),
                        Yii::t('cmscore', 'published'),
                        Yii::t('cmscore', 'deleted')];
                    $status = Yii::t('cmscore', 'State') . ' : ' . $statuses[$status];
                }
                $name = ($comment['status'] != 2) ? Html::encode($comment['name']) : '';
                if ($withAvatars) {
                    $picture = Common::getFileUrlByPath($usermodule->pathToDefaultAvatar);
                }
                if ($uid = (int) $comment['uid']) {
                    $name = Yii::t('cmscore', 'John Smith');
                    if (isset($this->usernames[$uid])) {
                        $name = Html::encode($this->usernames[$uid]['usertitle']);
                        if (Yii::$app->user->can('user_view_user_profiles')
                                &&
                                ($this->usernames[$uid]['status'] == 1)
                            ) {
                            $name = Html::a($name, ['/users/main/view', 'username' => $this->usernames[$uid]['innerusername']]);
                        }
                        if ($withAvatars && !empty($this->usernames[$uid]['fullpath'])) {
                            $picture = Common::getFileUrlByPath($this->usernames[$uid]['fullpath']);
                        }
                    }
                }
                //cache
                if ($comment['status'] != 2) {
                    if (empty($comment['cache']) && $modelClass) {
                        $data = Common::format($textFormat, $comment['body']);
                        $comment['cache'] = $text = $data['text'];
                        if (!$text) {
                            // protection from empty comments being format processed per every view request
                            $text = ' ';
                        }
                        $modelClass::updateAll(['cache' => $text], 'cid=:cid', ['cid' => $comment['cid']]);
                    }
                }
                //end cache
                print strtr($this->templateComment, [
                    '@indent' => ($this->isHierarchical) ? $this->indentCoefficient * $comment['level']: 0 ,
                    '@name' => $name,
                    '@picture' => $picture ? Html::img($picture, ['height' => $this->avatarHeight]) : '',
                    '@created' => Html::encode(date($this->createdFormat, (int) $comment['created'])),
                    '@cid' => $comment['cid'],
                    '@body' => ($comment['status'] != 2) ? $comment['cache'] : '<i>' . Yii::t('cmscore', 'deleted') . '</i>',
                    '@answerto' => $this->isForAdminEdit ?
                        '<div class="operations"><span class="status">' . $status . '</span><a class="edit-comment" href="/comments/task/edit-comment?cid=' . $comment['cid'] . '">' . Yii::t('cmscore', 'Edit') . '</a>' .
                        '<a class="delete-comment"  href="/comments/task/delete-comment?cid=' . $comment['cid'] . '">' . Yii::t('cmscore', 'Delete') . '</a></div>'
                        :    (($this->isHierarchical && $this->isShowForm) ?
                                   '<footer><a style="display:block;text-align:right;outline:none;" href="#" class="answerto" name="' . $comment['cid'] . '">' . Yii::t('cmscore', 'Answer') . '</a></footer>' : ''),
                ]);
            }
            print LinkPager::widget([
                  'pagination' => $this->paginator,
            ]);            
            print '</section>';
        }
        // show comments form
        if ($this->isShowForm) {
            if (empty($this->comments)) {
                print '<section id="comments" class="comment-wrapper"></section>';
            }
            print $this->renderFile($this->commentModelTemplate, [
                'model' => $this->formmodel,
                'isHierarchical' => $this->isHierarchical,
            ]); 
            if ($this->isHierarchical) {
                print '<a class="comment0" style="display:block;text-align:left;" href="#" class="answerto" name="0">' . Yii::t('cmscore', 'Answer') . '</a>';                
            }
        }
    }
    
    /**
     * From information about the node we will fill $this->comments,
     * $this->usernames and $this->paginator
     */
    protected function initComments()
    {
        $status = [1];
        if ($this->isHierarchical) {
            $status[] = 2;
        }
        if ($this->isForAdminEdit) {
            $status[] = 0;
        }
        $query = $this->getCommentQuery();
        $query->where([
            'pagetype' => $this->contentType, 
            'nid' => $this->nid,
            'status' => $status,
        ]);
        $countQuery = clone $query;
        $this->paginator = $paginator = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize' => $this->countPerPage,
            'forcePageParam' => false,
        ]);
        if ($this->isHierarchical) {
            $query->orderBy('thread asc');
        } else {
            $query->orderBy('created asc');
        }
        $this->comments = 
            $query->offset($paginator->offset)
                    ->limit($paginator->limit)
                    ->asArray()
                    ->all();
        $class = Yii::$app->getModule('users')->userClass;
        $uids = [];
        foreach ($this->comments as $comment) {
            if ($uid = (int) $comment['uid']) {
                $uids[] = $uid;
            }
        }
        if ($uids) {
            /*
            $this->usernames = $class::find()
                    ->select(['id', 'usertitle', 'innerusername', 'status'])        
                    ->where(['id' => $uids])
                    ->asArray()
                    ->indexBy('id')
                    ->all();
            */
            $this->usernames = (new Query)
                    ->select(['id', 'usertitle', 'innerusername', 'uu.status', 'ff.fullpath'])        
                    ->from($class::tableName() . ' uu')
                    ->leftJoin(Files::tableName() . ' ff', 'uu.fid=ff.fid')
                    ->where(['id' => $uids])
                    ->indexBy('id')
                    ->all();            
            //var_dump($usernames);
        }
    }
    
    /**
     * Returning a query object for comments table.
     * 
     * Use this to adjust comment model with some data from another table
     * joined by left join, etc.
     * 
     * @return \yii\db\Query
     */
    public function getCommentQuery()
    {
        $def = Yii::$container->getDefinitions();
        if (isset($def[$this->commentModelDiAlias])) {
            $class = $def[$this->commentModelDiAlias]['class'];
            return $class::find();            
        }
    }

    /**
     * Initializes comment model for the add comment form.
     * It also handles the form submitting.
     */
    protected function initModel()
    {
        if (null == $this->formmodel) {
            $this->formmodel = Yii::$container->get($this->commentModelDiAlias);
            $this->formmodel->pagetype = $this->contentType;
            $this->formmodel->nid = $this->nid;
            $this->formmodel->uid = (int) Yii::$app->user->getId();
            $this->formmodel->answerto = 0;
            $this->formmodel->setIsHierarchical($this->isHierarchical);
            if ($this->formmodel->load(Yii::$app->request->post()) && $this->formmodel->save()) {
                $this->isShowComments = false;
                $this->isShowForm = false;
                // redirect
                $url = '/<front>';
                if ($this->node) {
                    if ($this->formmodel->status == 0) {
                        Yii::$app->session->setFlash('form-successful-message', Yii::t('endsite', Yii::$app->getModule('comments')->messageCommentWaitsApproval));
                        $params = [];
                        if (isset($_GET['page'])) {
                            $params['page'] = $_GET['page'];
                        }
                    } else {
                        $params = ['#' => 'comment-' . $this->formmodel->cid];
                        $page = $this->getRedirectPage($this->formmodel->thread);
                        if ($page > 1) {
                            $params['page'] = $page;
                        }
                    }
                    $innerUrl = str_replace('<nid>', $this->node->nid, $this->node->innerUrlTemplate);
                    $url = Common::urlTo($innerUrl, $params); 
                }
                $this->view->context->redirect($url); 
                Yii::$app->end();
            }
        }
    }
    
    /**
     * Getting the number of page the new comment is currently in.
     * 
     * @param string $thread The value of the thread of the comment added.
     * @return integer
     */
    protected function getRedirectPage($thread)
    {
        $status = [1];
        if ($this->isHierarchical) {
            $status[] = 2;
        }
        if ($this->isForAdminEdit) {
            $status[] = 0;
        }
        $query = $this->getCommentQuery();
        $query->where([
            'pagetype' => $this->contentType, 
            'nid' => $this->nid,
            'status' => $status,
        ]);  
        $query->andWhere(['<', 'thread', $thread]);
        // amount of comments before this one
        $count = $query->count();
        return floor($count/$this->countPerPage) + 1;
    }

}
