<?php
namespace mgrechanik\comments\widgets;

use yii\helpers\Html;
use mgrechanik\cmscore\helpers\Common;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use Yii;

/**
 * Widget to display new comments posted
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class ShowNewComments extends ShowCommentsList
{
    /**
     * @inheritdoc
     */           
    public $isForAdminEdit = true;
    
    /**
     * @var integer  -1 for all and 0 for waited for moderation
     */
    public $status = -1;

    /**
     * @inheritdoc
     */       
    protected function initComments()
    {
        $query = $this->getCommentQuery();
        if (!$this->status) {
            $query->where(['status' => 0]);
        }
        $countQuery = clone $query;        
        $this->paginator = $paginator = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize' => $this->countPerPage,
            'forcePageParam' => false,
        ]);
        $query->orderBy('created desc');

        $this->comments = 
            $query->offset($paginator->offset)
                    ->limit($paginator->limit)
                    ->asArray()
                    ->all();
        $class = Yii::$app->getModule('users')->userClass;
        $uids = [];
        foreach ($this->comments as $comment) {
            if ($uid = (int) $comment['uid']) {
                $uids[] = $uid;
            }
        }
        if ($uids) {
            $this->usernames = $class::find()
                    ->select(['id', 'usertitle', 'innerusername', 'status'])        
                    ->where(['id' => $uids])
                    ->asArray()
                    ->indexBy('id')
                    ->all();
        }
    }
    
    /**
     * @inheritdoc
     */ 
    public function run()
    {
        // show comments
        if ($this->isShowComments && !empty($this->comments)) {
            $textFormat = Yii::$app->getModule('comments')->commentTextFormat;
            $def = Yii::$container->getDefinitions();
            $modelClass = null;
            if (isset($def[$this->commentModelDiAlias])) {
                $modelClass = $def[$this->commentModelDiAlias]['class'];  
            }
            print '<table id="newcomments" class="table table-striped table-bordered table-hover table-condensed">';
            foreach ($this->comments as $comment) {
                if ($this->isForAdminEdit){
                    $status = (int) $comment['status'];
                    $statuses = [Yii::t('cmscore', 'not published'),
                        Yii::t('cmscore', 'published'),
                        Yii::t('cmscore', 'deleted')];
                    $status = Yii::t('cmscore', 'State') . ' : ' . $statuses[$status];
                }
                $name = ($comment['status'] != 2) ? Html::encode($comment['name']) : '';
                if ($uid = (int) $comment['uid']) {
                    $name = Yii::t('cmscore', 'John Smith');
                    if (isset($this->usernames[$uid])) {
                        $name = Html::encode($this->usernames[$uid]['usertitle']);
                        if (Yii::$app->user->can('user_view_user_profiles')
                                &&
                                ($this->usernames[$uid]['status'] == 1)
                            ) {
                            $name = Html::a($name, ['/users/main/view', 'username' => $this->usernames[$uid]['innerusername']]);
                        }
                    }
                }
                //cache
                if ($comment['status'] != 2) {
                    if (empty($comment['cache']) && $modelClass) {
                        $data = Common::format($textFormat, $comment['body']);
                        $comment['cache'] = $text = $data['text'];
                        if (!$text) {
                            // protection from empty comments being format processed per every view request
                            $text = ' ';
                        }
                        $modelClass::updateAll(['cache' => $text], 'cid=:cid', ['cid' => $comment['cid']]);
                    }
                }
                //end cache
                $links = '';
                if ($comment['status'] != 0) {
                    $links .= Html::a(Yii::t('cmscore', 'View'), ['/comments/task/redirect-to-comment', 'cid' => intval($comment['cid'])]) . '<br>';
                }
                //$links .= Html::a(Yii::t('cmscore', 'Edit'), ['/comments/task/redirect-to-comment', 'cid' => intval($comment['cid']), 'where' => 'back']);
                $links .= Html::a(Yii::t('cmscore', 'Edit'), ['/comments/task/edit-comment', 'cid' => intval($comment['cid'])]) . '&nbsp;&nbsp;';
                $links .= Html::a(Yii::t('cmscore', 'Delete'), ['/comments/task/delete-comment', 'cid' => intval($comment['cid'])]);
                
                //$add= '<img src="http://trash.datasvit.ks.ua/themes/dragon/images/dragon.jpg">';
                $add= '';
                $addtime = '';
                $seconds = time() - intval($comment['created']);
                $hours = floor($seconds/3600);
                if ($hours > 0) {
                    if ($hours < 25) {
                        $addtime = Yii::t('cmscore', '{n} hours back', ['n' => $hours]);
                    } else {
                        $days = floor($hours/24);
                        $hours = $hours - $days*24;
                        $addtime = Yii::t('cmscore', '{g} days, {n} hours back', ['n' => $hours, 'g' => $days]);
                    }
                    $addtime = '&nbsp;&nbsp;(' . $addtime . ')';
                }
                print '<tr><td><div  style="max-height:200px;overflow-y:auto;">' . strtr($this->templateComment, [
                    '@indent' => 0 ,
                    '@picture' => '',
                    '@name' => $name,
                    '@created' => Html::encode(date($this->createdFormat, (int) $comment['created'])) . $addtime,
                    '@cid' => $comment['cid'],
                    '@body' => ($comment['status'] != 2) ? $comment['cache'] : '<i>' . Yii::t('cmscore', 'deleted') . '</i>',
                    '@answerto' => '<kbd class="status"><i>' . $status . '</i></kbd>',
                ]) . $add . '</div></td><td>' . $links .'</td></tr>';
                
            }
            print '</table>';
            print LinkPager::widget([
                  'pagination' => $this->paginator,
            ]);            
            
        }

    }    
    
}

