<?php
use yii\helpers\Html;

print $form->field($mainmodel, 'status')->radioList([
    1 => Yii::t('cmsblock', 'Active'), 
    2 => Yii::t('cmsblock', 'Closed'),
]);
print $form->field($mainmodel, 'typeresult')->radioList([
    1 => Yii::t('cmsblock', 'Display the amount of votes'), 
    2 => Yii::t('cmsblock', 'Display percents'),
    3 => Yii::t('cmsblock', 'Display both'),    
]);
print $form->field($mainmodel, 'isguestsvote')->dropDownList([
    1 => Yii::t('cmsblock', 'Yes'), 
    0 => Yii::t('cmsblock', 'No'),
]);
print $form->field($mainmodel, 'isguestsshow')->dropDownList([
    1 => Yii::t('cmsblock', 'Yes'), 
    0 => Yii::t('cmsblock', 'No'),
]);
$final = $mainmodel->getIsQuestionsFinal();
$mess = '';
$questions = null;
$helpText = Yii::t('cmsblock', 'The list of questions of this poll:');
$existed = $mainmodel->getCurrentQuestions();
if ($final) {
    $mess = Yii::t('cmsblock', 'Voting for this poll has already begun');    
    $items = [];
    foreach ($existed as $q) {
        $items[] = $q['text'] . ' ( ' . $q['votes'] . ' ' . Yii::t('cmsblock', 'votes') . ' )';
    }
    $questions = Html::ul($items);
} else {
    \mgrechanik\cmfmodules\blocks\types\pollblock\assets\QuestionsAsset\QAsset::register(Yii::$app->view);    
    $mess = Yii::t('cmsblock', 'You can edit the set of questions until the voting starts');    
    $countEmpty = 0;
    $res = [];
    if (empty($existed)) {
        $countEmpty = 2;
    } else {
        foreach ($existed as $k => $v) {
            $arr = ['qid' => intval($v['qid'])];
            $arr['content'] = '<input size="50" type="text" name="some" value="' . Html::encode($v['text']) . '">';
            $res[] = $arr;
        }
    }
    while ($countEmpty) {
        $countEmpty--;
        $res[] = ['qid' => 0, 'content' => '<input size="50" type="text" name="some" value="">'];
    }
}
?>
<div id="poll-area" class="form-group">
    <?php if ($mess) { ?>
    <div class="poll-title-help">
        <?= $mess ?>
    </div>  
    <?php } ?>
    <h4><?= $helpText ?></h4>
    <?php if ($questions) { ?>
    <div class="poll-body">
        <?= $questions ?>
    </div>  
    <?php } ?>  
    <?php if (!$final) { ?>
        <ul id="poll-ul"  class="connectedSortable connected">
            <?php
            $del = '<a href="#" class="quest-delete">' . Yii::t('cmsblock', 'delete') . '</a>';
            foreach ($res as $val) {
                print '<li class="pollwanted" qid="' . $val['qid'] . '">' . $val['content'] . $del . '</li>';
            }
            ?>
        </ul> 
        <a href="#" id="add-question"><?= Yii::t('cmsblock', 'add')?></a>
        <div class="container-add" style="display:none;">
            <li class="pollwanted" qid="0"><input size="50" type="text" name="some" value=""><?=$del ?></li>
        </div>
    <?php } ?>  
</div>
