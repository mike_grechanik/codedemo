<?php
use yii\helpers\Html;
?>
<section class="block block-<?=$id  . $cssclass ?> clearfix">
  <?php if ($title) { ?>  
    <h3><?=$title?></h3>
  <?php } ?>  
    <div class="pollcontent">
       <?php 
           if ($mainmodel->isVotingActive()) {
               \mgrechanik\cmfmodules\blocks\types\pollblock\assets\QuestionsAsset\VotingAsset::register(yii::$app->view);
               $items = $mainmodel->getQuestionTexts();
               print Html::hiddenInput('poll-number', $mainmodel->pid);
               print '<ul class="poll-ul-container">';
               $count = count($items);
               $i = 0;
               foreach ($items as $key => $val) {
                   $i++;
                   $class = '';
                   if ($i == $count) {
                       $class = ' class="last"';
                   }
                   print "<li$class>"
                   .'<label><input type="radio"  class="poll-element" name="pollinput" value="' . $key . '">' . $val . '</label>'
                   . '</li>';
               }
               print '</ul>';
               print '<div class="help-done" style="display:none">' . Yii::t('cmsblock', 'You have voted. Wait for result..') . '</div>';
               print '<button class="poll-button" disabled>' . Yii::t('cmsblock', 'Vote') . '</button>';
           } else {
               print $mainmodel->getActualCache();
           }
       ?>
    </div>
</section>
