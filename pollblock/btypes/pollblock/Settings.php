<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmfmodules\blocks\types\pollblock\btypes\pollblock;

use Yii;

/**
 * Settings block model for blocks of 'bodyblock' type
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Settings extends \mgrechanik\blocks\base\SettingsModel
{
    /**
     * @inheritdoc
     */     
    public $viewTemplate = '@mgrechanik/cmfmodules/blocks/types/pollblock/btypes/pollblock/view/block.php';
    
    /**
     * @inheritdoc
     */     
    protected $editTemplate = '@mgrechanik/cmfmodules/blocks/types/pollblock/btypes/pollblock/view/settingsedit.php';
    
    /**
     * @inheritdoc
     */     
    public function rules()
    {
        if (Yii::$app->user->can('programmingwork')) {
            return [
                [['viewTemplate'], 'string'],
            ];
        }
        return [];
    }

    /**
     * @inheritdoc
     */     
    public function attributeLabels()
    {
        return [
            'viewTemplate' => Yii::t('cmsblock', 'ViewTemplate'),
        ];
    }    
}
