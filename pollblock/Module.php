<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmfmodules\blocks\types\pollblock;

use Yii;

/**
 * Module to add next - previouus node navigation block
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Module extends \mgrechanik\cmscore\base\Module
{
    /**
     * @inheritdoc
     */        
    public $controllerNamespace = 'mgrechanik\cmfmodules\blocks\types\pollblock\controllers';
    
    /**
     * @var string Aliased path to a template for result 
     */
    public $resultTemplate = '@mgrechanik/cmfmodules/blocks/types/pollblock/views/result.php';

    /**
     * @inheritdoc
     */       
    public function getMetaData()
    {
        return [
            'blockTypes' => [
                'pollblock' => [
                    'name' => 'Poll block',
                    'mainmodel' => [
                        'class' => 'mgrechanik\cmfmodules\blocks\types\pollblock\models\Poll',
                        'status' => 1,
                        'isguestsvote' => 1,
                        'isguestsshow' => 1,
                        'typeresult' => 2,
                    ],                    
                    'settingsmodel' => [
                        'class' => 'mgrechanik\cmfmodules\blocks\types\pollblock\btypes\pollblock\Settings',
                    ],
                    'blockmodel' => [
                        'iscacheable' => 0,
                        'cssclass' => 'poll',
                        'vis3' => 'mgrechanik\cmfmodules\blocks\types\pollblock\service\Callback::isShowPolltoGuests|Check whether to display this block to guests',
                    ],                                        
                ], 
            ],
           'translatesources' => [
               'pollblock' => [
                   'for' => 'cmsblock',
                   'basePath' => '@mgrechanik/cmfmodules/blocks/types/pollblock/messages',
               ],               
           ],                
        ];
    }       
}
