<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmfmodules\blocks\types\pollblock\controllers;

use Yii;
use mgrechanik\cmscore\base\Controller;
use yii\web\Response;
use mgrechanik\cmfmodules\blocks\types\pollblock\models\Poll;
use mgrechanik\cmfmodules\blocks\types\pollblock\models\Question;
use mgrechanik\cmfmodules\blocks\types\pollblock\models\Pollusers;

/**
 * Controller to serve voting
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class MainController extends Controller
{
    /**
     * Process voting and return result after that 
     * 
     * @param integer $pid
     * @param integer $choice
     * @return mixed
     */
    public function actionVote($pid, $choice)
    {
        $res = ['res' => 0];
        if (($poll = Poll::findOne($pid)) && ($poll->status == 1)) {
            $force = false;
            $q = null;
            $proceed = true;
            if ($choice != -1) {
                $q = Question::find()->where(['pid' => $pid, 'qid' => $choice])->one();
                if (!$q) {
                    $proceed = false;
                }
            } 
            if ($proceed) {
                if (Pollusers::canCurrentUserVote($pid)) {
                    if ($q) {
                        $q->votes++;
                        $q->save(false);
                        $force = true;
                    }
                    Pollusers::setCurrentUserVoted($pid);
                }
            }
            $force = true;
            $res = ['res' => $poll->getActualCache($force)];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $res;
    }
    
}