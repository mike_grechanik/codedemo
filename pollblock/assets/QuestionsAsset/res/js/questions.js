$(document).ready(function(){
    var htmlEscape = function(str) {
        return String(str)
                .replace(/&/g, '&amp;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&#39;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;');
    };
    
  $( ".connected" ).sortable({
			connectWith: ".connectedSortable"
		});//.disableSelection(); 
  $ul = $('#poll-ul');             
  $('#add-question').click(function(){
      var length = $('#poll-ul li.pollwanted').length;
      if (length == 1) {
          $('#poll-ul a.quest-delete').show();
      }      
      $ul.append($('div.container-add').html());
      return false;
  });
  $('#poll-ul').on('click', 'a.quest-delete', function(){
      var length = $('#poll-ul li.pollwanted').length;
      if (length > 1) {
        $(this).parent().remove();
      }
      if (length == 2) {
          $('#poll-ul a.quest-delete').hide();
      }
      return false;
  });
                
  $('#blockSaveButton').click(function(){
    $f = $(this).closest('form');
    $('#poll-ul li.pollwanted').each(function(){
      var $this = $(this);
      var $par = $this.closest('ul').children();
      var ind = $par.index($this[0]) + 1;
      var qid = $this.attr('qid');
      var text = $this.find('input').val();
      if (text != '') {
        var add = '<input type="hidden" name="polldata[' + ind + ']" value="' + qid + '#' + htmlEscape(text) +'">';
        $f.append(add);
      }
    });
    var add = '<input type="hidden" name="polldata[-1]" value="">';
    $f.append(add);    
  });   
  
  });	

