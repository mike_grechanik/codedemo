$(document).ready(function(){
   
  $('button.poll-button').click(function(){
    var $parent = $(this).closest('div.pollcontent');
    var pid = $parent.find('input[name=poll-number]').val();
    var choice = $parent.find('input:checked').val();
    //alert(pid + '_' + choise);
    $parent.find('button.poll-button').hide();
    $parent.find('div.help-done').show();
    $.ajax({
        url: '/pollblock/main/vote',
        type: "GET",
        data: {pid: pid, choice:choice},
        success: function (data) {
            if (data) {
                if (data.res != 0) {
                    $parent.html(data.res);
                }
            }
        }
    }); 
    
  });
  
  $('ul.poll-ul-container input.poll-element').change(function(){
      var $parent = $(this).closest('div.pollcontent');
      $parent.find('button.poll-button').attr('disabled', false);
  });
  
});	

