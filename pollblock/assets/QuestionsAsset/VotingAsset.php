<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmfmodules\blocks\types\pollblock\assets\QuestionsAsset;

use yii\web\AssetBundle;

/**
 * Asset to manage pool questions.
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class VotingAsset extends AssetBundle
{
    public $sourcePath = '@mgrechanik/cmfmodules/blocks/types/pollblock/assets/QuestionsAsset/res';

	public $css = [
	    'css/voting.css',
	];
    
	public $js = [
	    'js/voting.js',
	];

	public $depends = [
        'yii\web\JqueryAsset',
	];


}
