<?php
use yii\helpers\Html;
use mgrechanik\cmfmodules\blocks\types\pollblock\models\Question;
use mgrechanik\cmfmodules\blocks\types\pollblock\service\Callback;
/* @var $poll \mgrechanik\cmfmodules\blocks\types\pollblock\models\Poll  */
if ($all = Question::find()->where(['pid' => $poll->pid])->asArray()->all()) {
    $sum = 0;
    foreach ($all as $q) {
        $sum += $q['votes'];
    }
    Callback::roundingPercents($all, $sum);
    print '<div class="poll-result">';
    foreach ($all as $q) {
        $add = '';
        switch ($poll->typeresult)
        {
            case 1:
                $add = $q['votes'] . ' ' . Yii::t('cmsblock', 'votes');
                break;
            case 2:
                $add = $q['percent'] . '%';
                break;
            case 3:
                $add = '(' . $q['percent'] . '% , ' . $q['votes'] . ' ' . Yii::t('cmsblock', 'votes') . ')';
                break;            
        }
        print '<div class="result-title-line"><span class="votes">' . $add . '</span><span class="title">' . Html::encode($q['text']) . '</span></div>';
        print '<div class="result-container"><div class="result-percent-line" style="width:' . $q['percent'] . '%"></div></div>';        
        //print '<div class="result-title-line"><span class="title">' . $q['text'] . '</span> <span class="votes">' . $add . '</span></div>';
        //print '<div class="result-percent-line" style="height:10px;background-color:brown;width:' . $q['percent'] . '%"></div>';
    }    
    print '</div>';
}


