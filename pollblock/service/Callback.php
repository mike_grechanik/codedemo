<?php
/**
 * @link http://mikhailgrechanik.name
 * @copyright Copyright © 2016 and onwards, Mikhail Grechanik. All rights reserved.
 * @license Proprietary/Closed Source
 */

namespace mgrechanik\cmfmodules\blocks\types\pollblock\service;

use Yii;
use mgrechanik\cmfmodules\blocks\types\pollblock\models\Poll;

/**
 * Some callbacks for pollblock
 * 
 * @author Mikhail Grechanik <mike.grechanik@gmail.com>
 * @since 1.0
 */
class Callback
{
    /**
     * Whether to show poll to guests current is the node page.
     * 
     * @return boolean
     */
    public static function isShowPolltoGuests($block)
    {
        if (Yii::$app->user->isGuest){
            if ($poll = Poll::findOne(intval($block['bname']))) {
                if (!$poll->isguestsshow) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Rounding percents of every vote
     * 
     * @param array $rows
     * @param integer $sum
     */
    public static function roundingPercents(&$rows, $sum)
    {
        $bigKey = null;
        $bigPerc = 0;
        $percentSum = 0;
        foreach ($rows as $key => $val) {
            if ($sum == 0) {
                $rows[$key]['percent'] = 0;
                continue;
            } 
            $rows[$key]['percent'] = $percent = round(($val['votes']/$sum)*100);
            $percentSum += $percent;
            if ($percent > $bigPerc) {
                $bigPerc = $percent;
                $bigKey = $key;
            }
        }
        if (($percentSum != 100) && $bigKey) {
            $diff = $percentSum - 100;
            $rows[$bigKey]['percent'] -= $diff;
        }
    }
}
