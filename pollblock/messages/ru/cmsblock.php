<?php
return [
    'Poll block' => 'Блок голосования',
    'Active' => 'Активное',
    'Closed' => 'Закрытое',
    'Status of the poll' => 'Статус голосования',
    'Can unregistered users vote?' => 'Могут ли незарегистрированные пользователи голосовать?',
    'Do we show the block with this poll to unregistered users?' => 'Показывать блок с данным голосованием незарегистрированным пользователям?',
    'Yes' => 'Да',
    'No' => 'Нет',
    'delete' => 'удалить',
    'add' => 'добавить',
    'The list of questions of this poll:' => 'Список вопросов данного голосования:',
    'You can edit the set of questions until the voting starts' => 'Вы можете редактировать набор вопросов пока не началось голосование',
    'Voting for this poll has already begun' => 'Голосование уже началось',
    'votes' => 'голосов',
    'Format of the result of voting' => 'Формат результата голосования',
    'Display the amount of votes' => 'Показывать количество голосов',
    'Display percents' => 'Показывать проценты',
    'Display both' => 'Показывать оба',
    'Empty vote (see result)' => 'Пустой голос (смотреть результат)',
    'Vote' => 'Голосовать',
    'You have voted. Wait for result..' => 'Вы проголосовали. Ждем результат..',
];
