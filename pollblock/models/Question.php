<?php

namespace mgrechanik\cmfmodules\blocks\types\pollblock\models;

use Yii;

/**
 * This is the model class for table "{{%pollquestions}}".
 *
 * @property integer $qid
 * @property integer $pid
 * @property string $text
 * @property string $imgurl
 * @property integer $votes
 * @property integer $weight
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pollquestions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid'], 'required'],
            [['pid', 'votes', 'weight'], 'integer'],
            [['text', 'imgurl'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'qid' => Yii::t('cmsblock', 'Qid'),
            'pid' => Yii::t('cmsblock', 'Pid'),
            'text' => Yii::t('cmsblock', 'Text'),
            'imgurl' => Yii::t('cmsblock', 'Imgurl'),
            'votes' => Yii::t('cmsblock', 'Votes'),
            'weight' => Yii::t('cmsblock', 'Weight'),
        ];
    }
}
