<?php

namespace mgrechanik\cmfmodules\blocks\types\pollblock\models;

use Yii;

/**
 * This is the model class for table "{{%pollusers}}".
 *
 * @property integer $pid
 * @property integer $uid
 */
class Pollusers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pollusers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid', 'uid'], 'required'],
            [['pid', 'uid'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pid' => Yii::t('cmsblock', 'Pid'),
            'uid' => Yii::t('cmsblock', 'Uid'),
        ];
    }
    

    /**
     * Whether the current user has the right to vote.
     * 
     * @param integer $pid Number of the poll
     * @return boolean
     */
    public static function canCurrentUserVote($pid)
    {
        $ip = Yii::$app->request->userIP;
        $db = Yii::$app->db;
        $user = Yii::$app->user;
        $params = [];
        $sql = 'SELECT COUNT(*) FROM {{%pollusers}} WHERE [[pid]]=' . intval($pid) . ' AND ';
        if (!$user->isGuest) {
            $sql .= '[[uid]]=' . intval($user->getId());
        } else {
            $sql .= '[[uid]] = IFNULL(INET_ATON(:ip),0)';
            $params[':ip'] = $ip;
        }
        if ($count = (int) $db->createCommand($sql, $params)->queryScalar()) {
            return false;
        }
        return true;
    }
    
    /**
     * Write info about current user's voting event in this poll.
     * 
     * @param integer $pid
     */
    public static function setCurrentUserVoted($pid)
    {
        $ip = Yii::$app->request->userIP;
        $db = Yii::$app->db;
        $user = Yii::$app->user;
        $params = [];
        $sql = 'REPLACE INTO {{%pollusers}} VALUES(' . intval($pid) . ' , ';
        if (!$user->isGuest) {
            $sql .= intval($user->getId());
        } else {
            $sql .= 'IFNULL(INET_ATON(:ip),0)';
            $params[':ip'] = $ip;
        }
        $sql .= ')';
        $db->createCommand($sql, $params)->execute();        
    }
}
