<?php

namespace mgrechanik\cmfmodules\blocks\types\pollblock\models;

use Yii;
use yii\helpers\Html;
use mgrechanik\blocks\base\MainModelInterface;
use mgrechanik\blocks\base\BlockModelTrait;

/**
 * This is the model class for table "{{%poll}}".
 *
 * @property integer $pid
 * @property integer $status 1 - active, 2 - closed
 * @property integer $isguestsvote Whether guests can vote 1/0
 * @property integer $isguestsshow Whether to show to guests 1/0
 * @property integer $typeresult Format of result label 1 - votes, 2 - percent, 3 - both
 * @property string $cache
 */
class Poll extends \yii\db\ActiveRecord implements MainModelInterface
{
    /**
     * Mandatory for a block main model
     */
    use BlockModelTrait;
    
    /**
     * @var string  Aliased path to a template of editing this model functionality
     * Mandatory for a block main model
     */
    protected $editTemplate = '@mgrechanik/cmfmodules/blocks/types/pollblock/btypes/pollblock/view/mainmodeledit.php';
    
    /**
     * @var array|null Loaded questions
     */
    protected $_loadquestions = null;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%poll}}';
    }

    /**
     * @inheritdoc
     */        
    public function getBName()
    {
        return $this->pid;
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'isguestsvote', 'isguestsshow', 'typeresult'], 'integer'],
            [['isguestsvote', 'isguestsshow'], 'in', 'range' => [1,0]],
            [['typeresult'], 'in', 'range' => [1,2,3]],
            [['cache'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pid' => Yii::t('cmsblock', 'Pid'),
            'status' => Yii::t('cmsblock', 'Status of the poll'),
            'isguestsvote' => Yii::t('cmsblock', 'Can unregistered users vote?'),
            'isguestsshow' => Yii::t('cmsblock', 'Do we show the block with this poll to unregistered users?'),
            'typeresult' => Yii::t('cmsblock', 'Format of the result of voting'),
            'cache' => Yii::t('cmsblock', 'Cache'),
        ];
    }
    
    /**
     * @inheritdoc
     */    
    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'handlerAfterDelete']);

    }
    
    /**
     * Event handler
     * 
     * @param \yii\base\Event $event
     */    
    public function handlerAfterDelete($event)
    {
        Question::deleteAll(['pid' => $this->pid]);
        Pollusers::deleteAll(['pid' => $this->pid]);
    }    
    
    /**
     * @inheritdoc
     */           
    public function load($data, $formName = null)
    {
        parent::load($data, $formName);
        if (isset($_POST['polldata']) && is_array($_POST['polldata'])) {
            unset($_POST['polldata'][-1]);
            $this->_loadquestions = [];
            foreach ($_POST['polldata'] as $key => $val) {
                if (!is_string($val)) {
                    continue;
                }
                $data = explode('#', $val);
                if (count($data) != 2) {
                    continue;
                }
                $this->_loadquestions[$key] = [
                    'weight' => $key,
                    'text' => $data[1],
                    'qid' => intval($data[0]),
                ];
            }
        }
    }

    /**
     * @inheritdoc
     */           
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->cache = '';
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */       
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (!$this->getIsQuestionsFinal()) {
            // adjusting associated questions
            if ($this->_loadquestions !== null) {
                $existed = $this->getExistedQuestions();
                foreach ($this->_loadquestions as $q) {
                    $qid = intval($q['qid']);
                    if ($qid) {
                        // some protection from wrong input
                        if (isset($existed[$qid])) {
                            $existed[$qid]['okay'] = true;
                            if ($obj = Question::findOne($qid) ) {
                                $obj->text = $q['text'];
                                $obj->weight = $q['weight'];
                                $obj->save(false);
                            }
                        }
                    } else {
                        $obj = new Question([
                            'text' => $q['text'],
                            'weight' => $q['weight'],
                            'pid' => $this->pid,
                        ]);
                        $obj->save(false);
                    }
                }
                foreach ($existed as $val) {
                    if (!isset($val['okay'])) {
                        if ($obj = Question::findOne($val['qid'])) {
                            $obj->delete();
                        }
                    }
                }
                
            }
        }
    }

    /**
     * Whether the questiona has been formed finally
     * 
     * @return boolean
     */
    public function getIsQuestionsFinal()
    {
        if ($this->isNewRecord) {
            return false;
        }
        return (Question::find()->where([
            'pid' => $this->pid,
        ])->andWhere(['>', 'votes', 0])->count() > 0) ? true : false;
    }
    
    /**
     * Returns a list of questions for this poll
     * 
     * @return array
     */
    public function getExistedQuestions()
    {
        $query = Question::find()
                ->select('qid,text,votes')
                ->where(['pid' => $this->pid])
                ->orderBy('weight asc')
                ->indexBy('qid');
        return $query->asArray()->all();
    }
    
    /**
     * Returns current, including loaded but not yet saved, questions
     * 
     * @return array
     */
    public function getCurrentQuestions()
    {
        if (null === $this->_loadquestions) {
            return $this->getExistedQuestions();
        } else {
            return $this->_loadquestions;
        }
    }
    
    /**
     * Checking whether the voting for this poll is active for the current user.
     * 
     * @return boolean
     */
    public function isVotingActive()
    {
        if ($this->status == 2) {
            return false;
        } else {
            $user = Yii::$app->user;
            if (($this->isguestsvote == 0) && $user->isGuest) {
                return false;
            }
            return Pollusers::canCurrentUserVote($this->pid);
        }
    }
    
    /**
     * Returns the texts of questions for this poll. Including empty one.
     * @return array
     */
    public function getQuestionTexts()
    {
        $res = [];
        $qs = Question::find()->select('qid,text')->where(['pid' => $this->pid])->orderBy('weight asc')->asArray()->all();
        foreach ($qs as $q) {
            $res[intval($q['qid'])] = Html::encode($q['text']);
        }
        $res[-1] = Yii::t('cmsblock', 'Empty vote (see result)');
        return $res;
    }
    
    /**
     * Rebuild and save cache.
     * 
     * @param boolean $force Whether to force cache rebuilding
     * @return string Cache text
     */
    public function getActualCache($force = false)
    {
        if ($force || empty($this->cache)) {
            $template = Yii::$app->getModule('pollblock')->resultTemplate;
            $cache = Yii::$app->view->renderFile($template, [
                'poll' => $this,
            ]);   
            if (!$cache) {
                $cache = ' ';
            }
            $this->cache = $cache;
            self::updateAll(['cache' => $cache], 'pid=:pid', ['pid' => $this->pid]);
        }
        return $this->cache;
    }
}
